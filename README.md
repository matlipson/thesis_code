This is a repository of code associated with the doctoral thesis by M.Lipson (m.lipson@unsw.edu.au) entitled:

> Model development for urban climates.

Key source code produced during the thesis includes an Urban CLimate and building Energy Model (UCLEM) [(Lipson et al., 2018)](https://rmets.onlinelibrary.wiley.com/doi/abs/10.1002/qj.3317) and an interface conduction scheme [(Lipson et al., 2017)](http://www.geosci-model-dev.net/10/991/2017/).

Directories contain the following:

CCAM_SCM
--------

Source code for the Conformal-Cubic Atmospheric Model (CCAM) atmospheric model revision 3975, (latest version [here](https://bitbucket.csiro.au/projects/CCAM/repos/ccam/browse)), with:

* **scm.f90:** the single column framework developed by M. Thatcher and M. Lipson,
* **ateb.f90:** the UCLEM urban/energy model developed by M. Lipson and M. Thatcher,
* **run_scm.sh:** an example of a runfile located within the SCM_example directory.

UCLEM_standalone
----------------

Source code for the standalone version of UCLEM which has components of the urban climate model (aTEB - [Thatcher and Hurley, 2012](https://link.springer.com/article/10.1007/s10546-011-9663-8)) removed to allow idealised evaluation of the new internal scheme. The structure of subroutine matches with the integrated version. Files include:

* **uclem.f90:** the primary UCLEM source code equivalent to ateb.f90,
* **uclemwrap.f90:** a wrapper code which includes synthetic forcing, equivalent to atebwrap.f90,
* **uclem.nml:** a namelist controlling key switches and parameters, equivalent to ateb.nml
* **makefile:** a makefile which compiles the source code

This code was used to produce Figure 4.4. 

To run the code:
1. type ```make``` to compile
2. type ```./run``` to run

Timesteps (dt) are adjusted in ```uclemwrap.f90``` and internal iterations (ncyits) within ```uclem.f90```.

aTEB_UCLEM
----------

Source code for UCLEM integrated with aTEB (together called UCLEM). Files as above (with uclem -> ateb). Forcing data not included (contact the author if necessary). To run the code:

1. type ```make``` to compile
2. create the following directories: out, energy, variables, nml, src.
2. type ```./run.sh``` to run

conduction
----------

Source code for the interface and half layer conduction schemes used to create Figure 3.4. Python code used for idealised studies and for the exact solution is available [here](https://bitbucket.org/matlipson/ics).

To run the code:

1. type ```gfortran conduction.f90 -o run``` (for the gfortran compiler),
2. type ```run```, then you will be prompted to enter which conduction scheme to use.

energyplus
----------

Contains the [EnergyPlus](https://energyplus.net/) configuration file (ideal_unit_ig4_infc1_ac.idf) used to compare UCLEM output in Figure 4.4.

figurescripts
-------------

Python code used to produce some of the figures of the thesis. 





