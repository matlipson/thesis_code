import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sys

pd.set_option('display.width', 150)

data = pd.read_csv('output_1.dat',delim_whitespace=True,index_col=['Time'])
data.index = (data.index/3600)-(data.index[0]/3600)
# data = data-273.15

plt.close('all')

fig, ax = plt.subplots(1,1,figsize=(8,4))
data.iloc[:-1].plot(ax=ax)
ax.hlines(y=20,xmin=data.index[0],xmax=data.index[-1])
ax.xaxis.set_ticks(range(0,25,3))
ax.set_xlim([0,24])

plt.legend(fontsize='small')

fig.savefig('plot_%s.png' %sys.argv[1], dpi=300,bbox_inches='tight',pad_inches=0.05)