!!=============================================================================
!! wrapper program for running UCLEM for unit testing
!! UCLEM is a simple model for building heat transfer processes and human behaviour
!! UCLEM makes up part of the aTEB urban canopy model (CSIRO)
!! UCLEM developer: Mathew Lipson <m.lipson@unsw.edu.au>
!! revision: May 2018

!!  UCLEM is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!! 
!!  UCLEM is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!  GNU General Public License for more details.
!! 
!!  You should have received a copy of the GNU General Public License
!!  along with UCLEM.  If not, see <http://www.gnu.org/licenses/>.
!!=============================================================================

program uclemwrap

use uclem
implicit none

!!!!!!!!!!!!!!!! constants !!!!!!!!!!!!!!!!
integer, parameter :: dp = SELECTED_REAL_KIND(p=8)
real, parameter :: period = 86400.              ! forcing period [s] (default is one day)
real, parameter :: spinup = 89.                 ! number of days spin up for discrete models
integer :: out_interval = 180                  ! output interval [s]
real, parameter :: pi = 4 * atan(1.0)  ! pi double precision
!!!!!!!!!!!!!!!! variables !!!!!!!!!!!!!!!!
integer, parameter :: ufull = 1   ! sets number of urban tiles (vectorised calculation)
character(len=15)  :: filename
integer :: f=11                   ! file unit bypass (to avoid stdout=6)
integer :: timesteps
integer :: t,i
integer :: diag
real    :: dt
real    :: start_time, stop_time
real    :: temp_ext
real, save  :: time=0.  ! time of day in [s]
real, dimension(ufull)   :: sigu
real, dimension(ufull,7) :: varout
!!!!!!!!!! main program !!!!!!!!!!!
call cpu_time(start_time)

diag    = 0       ! diag = diagnostic message mode (0 = off, 1 = basic, 2 = detailed, etc)
sigu    = 1.      ! fraction of urban in tile
dt      = 60.     ! timestep (sec)

timesteps = int((spinup+1)*(period)/dt)  ! calculate number of timesteps including spinup
if ( dt > out_interval ) then
  write (6,*) "Timestep smaller than out_interval, updating to match"
  out_interval = int(dt)
end if

call init_arrays(ufull,sigu,diag)

do i=1,ufull
    write(filename, '(A7,I0,A)') 'output_', i,'.dat'
    open(i+f,file=filename)
    ! write(i+f,'(A10,7A15)') 'Time', 'ext_air_T', 'int_air_T', 'walle_T',    & 
    !     'wallw_T', 'slab_T', 'roof_T', 'null'
    write(i+f,'(A10,8A15)') 'Time', 'ext_air_T', 'int_air_T', 'convect',    & 
        'acflux', 'intgains', 'infil', 'nodetemp', 'elecdemand'
    ! write(i+f,'(A10,7A15)') 'Time', 'ext_air_T', 'int_air_T',    & 
    !     'ggext', 'ggint', 'rgint', 'storage', 'nodetemp'
end do

do t = 1, timesteps
    time = time + dt
    if (time>=period) time=0.
    ! forcing
    ! call sinusoid_temp(temp_ext, dt, period, t)
    call step_temp(temp_ext, time)

    call internal_main(dt,temp_ext,varout,diag)
    do i=1,ufull
      ! write if timesteps are past spinup and match output interval
      if ((t>=spinup*period/dt) .and. MOD(int(t*dt),out_interval)==0) then
          write(i+f,'(I10,8F15.8)') int(time), temp_ext-273.16, varout(i,6),        &
                varout(i,1), varout(i,2), varout(i,3), varout(i,4), varout(i,5), varout(i,7)
      end if
  end do
end do

do i=1,ufull
    close(i+f)
end do

call finish(diag)

call cpu_time(stop_time)
write (6,*) 'Computation time:', stop_time - start_time, '[s] with,',int(dt),'[s] timestep'


contains 

subroutine sinusoid_temp(temp_ext, dt, period, t)
  ! Sets external temperature forcing into temp_ext array
  implicit none
  integer, intent(in)    :: t
  real, intent(in)       :: period, dt
  real, intent(out)      :: temp_ext

  temp_ext = 290.16 + 10*sin(2.*pi*t*dt/period)

end subroutine sinusoid_temp

subroutine step_temp(temp_ext, time)
  ! Sets external temperature forcing into temp_ext array
  implicit none
  real, intent(out)   :: temp_ext
  real, intent(in)    :: time      ! decimal time
  real                :: temp_dt   ! temperature change per second
  real :: temp_upper, temp_lower

  temp_lower = 10.+273.16
  temp_upper = 30.+273.16
  temp_dt   = (temp_upper-temp_lower)/3600.

  if      ( (time >= 0.*3600.)  .and. (time < 1.*3600.) )  then ! gradient between 00 and 01 hr
    temp_ext = temp_upper - time*temp_dt
  else if ( (time >= 1.*3600.)  .and. (time < 12.*3600.) ) then ! constant between 01 and 12 hr
    temp_ext = temp_lower
  else if ( (time >= 12.*3600.) .and. (time < 13.*3600.) ) then ! gradient between 12 and 13 hr
    temp_ext = temp_lower + (time - 12.*3600)*temp_dt
  else if ( (time >= 13.*3600.) .and. (time < 24.*3600.) ) then ! constant between 13 and 24 hr
    temp_ext = temp_upper
  else
    print *, 'error in setting step time', time
    stop
  end if 

end subroutine step_temp

end program uclemwrap
