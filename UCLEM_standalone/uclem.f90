!!=============================================================================
!! UCLEM: Urban Climate and Energy Model
!! a simple model for building heat transfer processes and human behaviour
!! UCLEM makes up part of the aTEB urban canopy model (CSIRO)
!! this standalone version is for unit testing
!! developer: Mathew Lipson <m.lipson@unsw.edu.au>
!! revision: October 2018
!!
!!  UCLEM is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!! 
!!  UCLEM is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!  GNU General Public License for more details.
!! 
!!  You should have received a copy of the GNU General Public License
!!  along with UCLEM.  If not, see <http://www.gnu.org/licenses/>.
!!=============================================================================
!! uclem.nml namelist defines timestep, layer numbers and other options
!!=============================================================================
!!uclemwrap.f90                     : wrapper (defines forcing, timesteps and tiles)
!!  call init_arrays(uclem.f90)     : allocate arrays as per ateb (for multi-processor)
!!    call atebtype_thread()        : defines uclem variables
!!      call init_internal()        : defines internal specific variables
!!      call init_lwcoeff()         : defines internal lw coefficients
!!  return to uclemwrap.f90
!!  do t=1, timsteps                : loop timesteps
!!    call sinusoid_temp()          : initialise dynamic arrays and initial properties
!!    call internal_main(uclem.f90) : pre-calculate longwave radiation coefficients
!!      call external_forcing()     : get external temperature temp_ext (sinusoidal)
!!      call internal_main()        : splits tiles per processor and passes to calc
!!        call internal_calc()      : main loop for calculations (per processor)
!!          call getdiurnal()       : calculate time of day fluxes
!!          call solvecanyon()      : boundary conditions for facets (wall/roof/slab etc)
!!          call solvetridiag()     : calculate conduction through materials
!!          call energyclosure()    : check energy conservation
!!          pass outputs back up    : write outputs to file
!!  return to uclem.f90
!!  call finish()                   : unallocate arrays
!!=============================================================================

module uclem

implicit none
private
public :: init_arrays,finish,internal_main
public ncyits
public upack_g,ufull_g,nl
public f_roof,f_wall,f_road,f_slab,f_intm
public intm_g
public road_g,roof_g,room_g,slab_g,walle_g,wallw_g,intl_g
public f_g,p_g
public facetparams,facetdata,intldata
public fparmdata,pdiagdata

!!! STATE ARRAY DECLARATIONS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

integer, save :: ufull          ! number of urban grid points (i.e., the vector length) on this processor.
integer, save :: ifull          ! total number of input grid points for a processor
integer, save :: imax           ! number of grid points on the thread (subset of the ifull points)
integer, save :: ntiles = 1     ! number of sub-sections assigned to threads (=1 for no threading)
integer, dimension(:), allocatable, save :: ufull_g   ! total number of urban grid points
logical, dimension(:,:), allocatable, save :: upack_g ! to pack or unpack ifull arrays into ufull arrays.

type facetdata
  real, dimension(:,:), allocatable :: nodetemp        ! Temperature of node (prognostic)       [K]
  real(kind=8), dimension(:,:), allocatable :: storage ! Facet energy storage (diagnostic)
end type facetdata

type facetparams
  real, dimension(:,:), allocatable :: depth         ! Layer depth                              [m]
  real, dimension(:,:), allocatable :: volcp         ! Layer volumetric heat capacity           [J /m3 /K]
  real, dimension(:,:), allocatable :: lambda        ! Layer conductivity                       [W /m /K]
  real, dimension(:),   allocatable :: alpha         ! Facet albedo (internal & external)
  real, dimension(:),   allocatable :: emiss         ! Facet emissivity (internal & external)
end type facetparams

type intldata
  real, dimension(:,:,:), allocatable :: viewf       ! internal radiation view factors
  real, dimension(:,:,:), allocatable :: psi         ! internal radiation matrix
end type intldata

type fparmdata
  real, dimension(:), allocatable :: hwratio         ! height width ratio of canyon           [-]
  real, dimension(:), allocatable :: coeffbldheight  ! building height coefficient            [-]
  real, dimension(:), allocatable :: effhwratio      ! effective building height              [-]
  real, dimension(:), allocatable :: sigmabld        ! building fraction                      [-]
  real, dimension(:), allocatable :: industryfg      ! industry waste heat                    [W /m2]
  real, dimension(:), allocatable :: trafficfg       ! time-averaged traffic waste heat flux  [W /m2]
  real, dimension(:), allocatable :: intgains_flr    ! internal gains per floor area          [W /m2]
  real, dimension(:), allocatable :: bldheight       ! building height (parameter)            [m]
  real, dimension(:), allocatable :: bldwidth        ! building width (prognostic)            [m]
  real, dimension(:), allocatable :: ctime           ! hour of day                            [h]
  real, dimension(:), allocatable :: bldairtemp      ! building air temperature               [K]
  real, dimension(:), allocatable :: infilach        ! nominal infiltration air change rate   [m3 /m3 /hour]
  real, dimension(:), allocatable :: ventilach       ! nominal ventilation air change rate    [m3 /m3 /hour]
  real, dimension(:), allocatable :: tempheat        ! heating comfort temperature            [K]
  real, dimension(:), allocatable :: tempcool        ! cooling comfort temperature            [K]
  integer, dimension(:), allocatable :: intmassn     ! number of internal mass volumes        [-]
end type fparmdata

type pdiagdata
  real, dimension(:), allocatable :: bldheat         ! sensible flux from heating buildings   [W /m2]
  real, dimension(:), allocatable :: bldcool         ! building heating flux                  [W /m2]
  real, dimension(:), allocatable :: traf            ! time-varying traffic waste heat flux   [W /m2]
  real, dimension(:), allocatable :: intgains_full   ! internal gains per building area       [W /m2]
  real(kind=8), dimension(:),   allocatable :: surferr             ! facet energy error       [W /m2]
  real(kind=8), dimension(:),   allocatable :: atmoserr            ! atmosphere energy error  [W /m2]
  real(kind=8), dimension(:),   allocatable :: surferr_bias        ! facet cumulative error   [W /m2]
  real(kind=8), dimension(:),   allocatable :: atmoserr_bias       ! atmos cumulative error   [W /m2]
  real(kind=8), dimension(:),   allocatable :: storagetot_net      ! NOT USED                 [W /m2]
  real(kind=8), dimension(:,:), allocatable :: storagetot_road     ! NOT USED                 [W /m2]
  real(kind=8), dimension(:,:), allocatable :: storagetot_roof     ! NOT USED                 [W /m2]
  real(kind=8), dimension(:,:), allocatable :: storagetot_walle    ! NOT USED                 [W /m2]
  real(kind=8), dimension(:,:), allocatable :: storagetot_wallw    ! NOT USED                 [W /m2]
end type pdiagdata

type(facetdata),   dimension(:), allocatable, save :: roof_g,road_g      ! global state data types
type(facetdata),   dimension(:), allocatable, save :: walle_g,wallw_g    ! global state data types
type(facetdata),   dimension(:), allocatable, save :: slab_g,intm_g      ! global state data types
type(facetdata),   dimension(:), allocatable, save :: room_g             ! global state data types
type(facetparams), dimension(:), allocatable, save :: f_roof,f_road      ! global facet parameters
type(facetparams), dimension(:), allocatable, save :: f_wall             ! global facet parameters
type(facetparams), dimension(:), allocatable, save :: f_slab,f_intm      ! global facet parameters
type(intldata),    dimension(:), allocatable, save :: intl_g             ! global internal data
type(fparmdata),   dimension(:), allocatable, save :: f_g                ! global tile parameters
type(pdiagdata),   dimension(:), allocatable, save :: p_g                ! global tile prognostics

!!! PARAMETER DECLARATIONS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! parameterisation methods and model parameters
integer, parameter :: dp = SELECTED_REAL_KIND(p=8)   ! precision to 8 decimal points
integer, parameter :: nl          = 4        ! Number of layers (default 4, must be factors of 4)
integer, save      :: atebnmlfile = 11       ! Read configuration from nml file (0=off, >0 unit number (default=11))
integer, save      :: acmeth      = 1        ! AC heat pump into canyon (0=Off, 1=On, 2=Reversible, COP of 1.0)
integer, save      :: intairtmeth = 1        ! Internal air temperature (0=fixed, 1=implicit varying)
integer, save      :: intmassmeth = 2        ! Internal thermal mass (0=none, 1=one floor, 2=dynamic floor number)
integer, save      :: cvcoeffmeth = 1        ! Internal surface convection heat transfer coefficient (0=DOE,1=ISO6946,2=fixed)
integer, save      :: statsmeth   = 1        ! Use statistically based diurnal QF amendments (0=off, 1=on) from Thatcher 2007
integer, save      :: behavmeth   = 1        ! Use smooth behavioural functions for AC and windows (0=off,1=on) from Rijal 2007
integer, save      :: infilmeth   = 1        ! Method to calculate infiltration rate (0=constant,1=EnergyPlus/BLAST,2=ISO)
integer, save      :: conductmeth = 1        ! Conduction method (0=half-layer, 1=interface)
integer, save      :: ncyits      = 18        ! Number of iterations for balancing canyon sensible and latent heat fluxes (default=6)
real(kind=8), save :: energytol = 0.005_8    ! Tolerance for acceptable energy closure in each timestep
real, save         :: urbtemp   = 290.       ! reference temperature to improve precision
! physical parameters
real, parameter    :: aircp   = 1004.64      ! Heat capacity of dry air                [J /kg /K]
real, parameter    :: air_pr = 100000        ! standard sea level pressure @ 15 degrees [kg m^-1 s^-2]
real, parameter    :: air_rd = 287.04        ! Gas constant for dry air [J kg^-1 K^-1]
real, parameter    :: pi      = 3.14159265   ! pi (must be rounded down for shortwave) [-]
real, parameter    :: sbconst = 5.67e-8      ! Stefan-Boltzmann constant               [W /m2 /K4]
! generic urban parameters
real, save         :: acfactor    = 10.      ! Air conditioning inefficiency factor
real, save         :: ac_copmax   = 10.      ! Maximum COP for air-conditioner
real, save         :: ac_heatcap  = 3.       ! Maximum heating/cooling capacity        [W /m3]
real, save         :: ac_coolcap  = 3.       ! Maximum heating/cooling capacity        [W /m3]
real, save         :: ac_heatprop = 1.       ! Proportion of heated spaces             [W /m3]
real, save         :: ac_coolprop = 1.       ! Proportion of cooled spaces             [W /m3]
real, save         :: ac_smooth   = 0.5      ! Synchronous heating/cooling smoothing parameter
real, save         :: ac_deltat   = 1.       ! Comfort range for temperatures          [+-K]

real, dimension(:,:), allocatable, save :: varout

contains

!!! SUBROUTINES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This subroutine prepares the arrays used by the scheme
! It is based on the atebinit subroutine from ateb.f90 which allows dynamic
!   array dimensions based on number of tiles for the processor (here = 1)

subroutine init_arrays(ifin,sigu,diag)

implicit none

integer, intent(in) :: ifin                          ! input grid length used to define ifull
integer, intent(in) :: diag                          ! tile_number, diagnostic
integer, dimension(:), allocatable, save :: utype    ! urban type
integer tile, is, ie                                 ! processor threading indexes
real, dimension(ifin), intent(in) :: sigu            ! urban fraction
integer, parameter :: maxtype = 8                    ! number of tile types

if (diag>=1) write(6,*) "Initialising arrays"

ufull = ifin
ifull = ifin
imax  = ifull/ntiles

allocate( roof_g(ntiles), road_g(ntiles), walle_g(ntiles), wallw_g(ntiles))
allocate( slab_g(ntiles), intm_g(ntiles), room_g(ntiles) )
allocate( intl_g(ntiles) )
allocate( f_roof(ntiles), f_road(ntiles), f_wall(ntiles), f_slab(ntiles), f_intm(ntiles) )
allocate( f_g(ntiles) )
allocate( p_g(ntiles) )
allocate( ufull_g(ntiles) )
allocate( upack_g(imax,ntiles) )

allocate( varout(ufull,7))

allocate( utype(imax) )

do tile = 1,ntiles
  if (diag>=1) write(6,*) "allocating to tile", tile

  is = (tile-1)*imax + 1                          ! =1 for single thread
  ie = tile*imax                                  ! =1 for single thread

  upack_g(1:imax,tile) = sigu(is:ie)>0.           ! logical array to set whether urban tile
  ufull_g(tile) = count( upack_g(1:imax,tile) )   ! counts full number of urban tiles

  allocate(f_roof(tile)%depth(ufull_g(tile),nl),f_roof(tile)%lambda(ufull_g(tile),nl))
  allocate(f_roof(tile)%volcp(ufull_g(tile),nl))
  allocate(f_wall(tile)%depth(ufull_g(tile),nl),f_wall(tile)%lambda(ufull_g(tile),nl))
  allocate(f_wall(tile)%volcp(ufull_g(tile),nl))
  allocate(f_road(tile)%depth(ufull_g(tile),nl),f_road(tile)%lambda(ufull_g(tile),nl))
  allocate(f_road(tile)%volcp(ufull_g(tile),nl))
  allocate(f_slab(tile)%depth(ufull_g(tile),nl),f_slab(tile)%lambda(ufull_g(tile),nl))
  allocate(f_slab(tile)%volcp(ufull_g(tile),nl))
  allocate(f_intm(tile)%depth(ufull_g(tile),nl),f_intm(tile)%lambda(ufull_g(tile),nl))
  allocate(f_intm(tile)%volcp(ufull_g(tile),nl))
  allocate(f_roof(tile)%emiss(ufull_g(tile)),f_roof(tile)%alpha(ufull_g(tile)))
  allocate(f_wall(tile)%emiss(ufull_g(tile)),f_wall(tile)%alpha(ufull_g(tile)))
  allocate(f_road(tile)%emiss(ufull_g(tile)),f_road(tile)%alpha(ufull_g(tile)))
  allocate(f_slab(tile)%emiss(ufull_g(tile)))
  allocate(roof_g(tile)%nodetemp(ufull_g(tile),0:nl),road_g(tile)%nodetemp(ufull_g(tile),0:nl))
  allocate(walle_g(tile)%nodetemp(ufull_g(tile),0:nl),wallw_g(tile)%nodetemp(ufull_g(tile),0:nl))
  allocate(slab_g(tile)%nodetemp(ufull_g(tile),0:nl),intm_g(tile)%nodetemp(ufull_g(tile),0:nl))
  allocate(room_g(tile)%nodetemp(ufull_g(tile),1))
  allocate(road_g(tile)%storage(ufull_g(tile),nl),roof_g(tile)%storage(ufull_g(tile),nl))
  allocate(walle_g(tile)%storage(ufull_g(tile),nl),wallw_g(tile)%storage(ufull_g(tile),nl))
  allocate(slab_g(tile)%storage(ufull_g(tile),nl),intm_g(tile)%storage(ufull_g(tile),nl))
  allocate(room_g(tile)%storage(ufull_g(tile),1))
  allocate(intl_g(tile)%viewf(ufull_g(tile),4,4),intl_g(tile)%psi(ufull_g(tile),4,4))
  allocate(f_g(tile)%ctime(ufull_g(tile)),f_g(tile)%bldairtemp(ufull_g(tile)))
  allocate(f_g(tile)%hwratio(ufull_g(tile)),f_g(tile)%coeffbldheight(ufull_g(tile)))
  allocate(f_g(tile)%effhwratio(ufull_g(tile)),f_g(tile)%bldheight(ufull_g(tile)))
  allocate(f_g(tile)%sigmabld(ufull_g(tile)),f_g(tile)%industryfg(ufull_g(tile)))
  allocate(f_g(tile)%intgains_flr(ufull_g(tile)),f_g(tile)%trafficfg(ufull_g(tile)))
  allocate(f_g(tile)%tempheat(ufull_g(tile)),f_g(tile)%tempcool(ufull_g(tile)))
  allocate(f_g(tile)%intmassn(ufull_g(tile)),f_g(tile)%bldwidth(ufull_g(tile)))
  allocate(f_g(tile)%infilach(ufull_g(tile)),f_g(tile)%ventilach(ufull_g(tile)))
  allocate(p_g(tile)%bldheat(ufull_g(tile)),p_g(tile)%bldcool(ufull_g(tile)),p_g(tile)%traf(ufull_g(tile)))
  allocate(p_g(tile)%intgains_full(ufull_g(tile)))
  allocate(p_g(tile)%surferr(ufull_g(tile)),p_g(tile)%atmoserr(ufull_g(tile)))
  allocate(p_g(tile)%surferr_bias(ufull_g(tile)),p_g(tile)%atmoserr_bias(ufull_g(tile)))

  ! if urban tiles exist, initialise variables
  if ( ufull_g(tile)>0 ) then
    ! Initialise state variables
    roof_g(tile)%nodetemp  = 1.  ! + urbtemp
    roof_g(tile)%storage   = 0._8
    road_g(tile)%nodetemp  = 1.  ! + urbtemp
    road_g(tile)%storage   = 0._8
    walle_g(tile)%nodetemp = 1. ! + urbtemp
    walle_g(tile)%storage  = 0._8
    wallw_g(tile)%nodetemp = 1. ! + urbtemp
    wallw_g(tile)%storage  = 0._8
    slab_g(tile)%nodetemp  = 1. ! + urbtemp
    slab_g(tile)%storage   = 0._8
    intm_g(tile)%nodetemp  = 1. ! + urbtemp
    intm_g(tile)%storage   = 0._8
    room_g(tile)%nodetemp  = 1.  ! + urbtemp
    room_g(tile)%storage   = 0._8

    ! Initialise parameters
    f_roof(tile)%depth     = 0.1
    f_roof(tile)%volcp     = 2.E6
    f_roof(tile)%lambda    = 2.
    f_roof(tile)%alpha     = 0.2
    f_roof(tile)%emiss     = 0.97
    f_wall(tile)%depth     = 0.1
    f_wall(tile)%volcp     = 2.E6
    f_wall(tile)%lambda    = 2.
    f_wall(tile)%alpha     = 0.2
    f_wall(tile)%emiss     = 0.97
    f_road(tile)%depth     = 0.1
    f_road(tile)%volcp     = 2.E6
    f_road(tile)%lambda    = 2.
    f_road(tile)%alpha     = 0.2
    f_road(tile)%emiss     = 0.97
    f_slab(tile)%depth     = 0.1
    f_slab(tile)%volcp     = 2.E6
    f_slab(tile)%lambda    = 2.
    f_slab(tile)%emiss     = 0.97
    f_intm(tile)%depth     = 0.1
    f_intm(tile)%lambda    = 2.
    f_intm(tile)%volcp     = 2.E6
    f_g(tile)%hwratio      = 1.
    f_g(tile)%sigmabld     = 0.5
    f_g(tile)%industryfg   = 0.
    f_g(tile)%intgains_flr = 0.
    f_g(tile)%trafficfg    = 0.
    f_g(tile)%bldheight    = 10.
    f_g(tile)%bldairtemp   = 1. ! + urbtemp
    f_g(tile)%ctime        = 0.
    f_g(tile)%infilach     = 0.5
    f_g(tile)%ventilach    = 2.

    utype= 1 ! default urban
    call atebtype_thread(utype,diag,f_g(tile), &
                         f_roof(tile),f_road(tile),f_wall(tile),f_slab(tile),     &
                         f_intm(tile),intl_g(tile),upack_g(:,tile),               &
                         ufull_g(tile))

    room_g(tile)%nodetemp(:,1)=f_g(tile)%bldairtemp

    ! Initialise prognostic variables
    p_g(tile)%bldheat       = 0._8
    p_g(tile)%bldcool       = 0._8
    p_g(tile)%traf          = 0._8
    p_g(tile)%intgains_full = 0._8
    p_g(tile)%surferr       = 0._8
    p_g(tile)%atmoserr      = 0._8
    p_g(tile)%surferr_bias  = 0._8
    p_g(tile)%atmoserr_bias = 0._8
  end if

  ! emulate ateb ctime calculation (fraction of day between 0 and 1)
  f_g(tile)%ctime = 0.
end do


deallocate( utype )

return
end subroutine init_arrays

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This subroutine defines parameters tiles and calls initialisation subroutines

subroutine atebtype_thread(itype,diag,fp,fp_roof,fp_road,fp_wall,fp_slab, &
                           fp_intm,intl,upack,ufull)

implicit none

integer, intent(in) :: diag, ufull
integer ii,j,ierr,nlp
integer, dimension(imax), intent(in) :: itype
integer, dimension(ufull) :: itmp
integer, parameter :: maxtype = 8
real, dimension(ufull) :: tsigveg,tsigmabld
! In-canyon vegetation fraction
real, dimension(maxtype) ::    csigvegc=(/ 0.38, 0.45, 0.38, 0.34, 0.05, 0.40, 0.30, 0.20 /)
! Area fraction occupied by buildings
real, dimension(maxtype) ::   csigmabld=(/ 0.45, 0.40, 0.45, 0.46, 0.65, 0.40, 0.45, 0.50 /)
! Building height (m)
real, dimension(maxtype) ::  cbldheight=(/   6.,   4.,   6.,   8.,  18.,   4.,   8.,  12. /)
! Building height to width ratio
real, dimension(maxtype) ::    chwratio=(/  0.4,  0.2,  0.4,  0.6,   2.,  0.5,   1.,  1.5 /)
! Industrial sensible heat flux (W m^-2)
real, dimension(maxtype) :: cindustryfg=(/   0.,   0.,   0.,   0.,   0.,  10.,  20.,  30. /)
! Internal gains sensible heat flux [floor] (W m^-2)
real, dimension(maxtype) :: cintgains=(/ 5.,   5.,   5.,   5.,   5.,   5.,   5.,   5. /)
! Daily averaged traffic sensible heat flux (W m^-2)
real, dimension(maxtype) ::  ctrafficfg=(/  1.5,  1.5,  1.5,  1.5,  1.5,  1.5,  1.5,  1.5 /)
! Comfort temperature (K)
real, dimension(maxtype) :: cbldtemp=(/ 291.16, 291.16, 291.16, 291.16, 291.16, 291.16, 291.16, 291.16 /)
! Roof albedo  ! (Fortuniak 08) Masson = 0.15
real, dimension(maxtype) ::  croofalpha=(/ 0.20, 0.20, 0.20, 0.20, 0.20, 0.20, 0.20, 0.20 /)   
! Wall albedo  ! (Fortuniak 08) Masson = 0.25
real, dimension(maxtype) ::  cwallalpha=(/ 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30 /)    
! Road albedo  ! (Fortuniak 08) Masson = 0.08
real, dimension(maxtype) ::  croadalpha=(/ 0.10, 0.10, 0.10, 0.10, 0.10, 0.10, 0.10, 0.10 /)    
! Roof emissivity
real, dimension(maxtype) ::  croofemiss=(/ 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90 /)
! Wall emissivity
real, dimension(maxtype) ::  cwallemiss=(/ 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85 /) 
! Road emissivity
real, dimension(maxtype) ::  croademiss=(/ 0.94, 0.94, 0.94, 0.94, 0.94, 0.94, 0.94, 0.94 /)
! Slab emissivity
real, dimension(maxtype) ::  cslabemiss=(/ 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90 /) 
! Roughness length of in-canyon vegetation (m)
! real, dimension(maxtype) ::    czovegc=(/   0.1,   0.1,   0.1,   0.1,   0.1,   0.1,   0.1,   0.1 /)
! Infiltration air volume changes per hour (m^3 m^-3)
real, dimension(maxtype) ::  cinfilach=(/  0.50,  0.50,  0.50,  0.50,  0.50,  0.50,  0.50,  0.50 /)
! Ventilation air volume changes per hour (m^3 m^-3)
real, dimension(maxtype) :: cventilach=(/  0.00,  0.00,  0.00,  0.00,  0.00,  0.00,  0.00,  0.00 /)
! Comfort temperature for heating [k]
real, dimension(maxtype) ::  ctempheat=(/  288.,  288.,  288.,  288.,  288.,  0.00,  0.00,  0.00 /)
! Comfort temperature for cooling [k]
real, dimension(maxtype) ::  ctempcool=(/  296.,  296.,  296.,  296.,  296.,  999.,  999.,  999. /)

real, dimension(maxtype,nl) :: croofdepth
real, dimension(maxtype,nl) :: cwalldepth
real, dimension(maxtype,nl) :: croaddepth
real, dimension(maxtype,nl) :: cslabdepth
real, dimension(maxtype,nl) :: croofcp
real, dimension(maxtype,nl) :: cwallcp
real, dimension(maxtype,nl) :: croadcp
real, dimension(maxtype,nl) :: cslabcp
real, dimension(maxtype,nl) :: crooflambda
real, dimension(maxtype,nl) :: cwalllambda
real, dimension(maxtype,nl) :: croadlambda
real, dimension(maxtype,nl) :: cslablambda

logical, dimension(imax), intent(in) :: upack

type(fparmdata), intent(inout) :: fp
type(facetparams), intent(inout) :: fp_roof, fp_road, fp_wall, fp_slab, fp_intm
type(intldata), intent(inout) :: intl

namelist /atebnml/  acmeth,intairtmeth,intmassmeth,conductmeth,cvcoeffmeth,statsmeth,behavmeth,infilmeth 

namelist /atebgen/  acfactor,ac_heatcap,ac_coolcap,ac_heatprop,ac_coolprop,ac_smooth,ac_deltat

namelist /atebtile/ csigmabld,cbldheight,chwratio,cindustryfg,cintgains,ctrafficfg,cbldtemp,           &
                    croofalpha,cwallalpha,croadalpha,croofemiss,cwallemiss,croademiss,croofdepth,      &
                    cwalldepth,croaddepth,croofcp,cwallcp,croadcp,crooflambda,cwalllambda,croadlambda, &
                    cslabdepth,cslabcp,cslablambda,cinfilach,cventilach,ctempheat,ctempcool

! dynamic facet characteristic arrays where nl=material layers
nlp=nl/4 ! number of repeated segments (if nl=4, nlp=1)
! depths (m)
croofdepth= reshape((/ ((0.05/nlp, ii=1,maxtype),j=1,nlp),    &
                       ((0.05/nlp, ii=1,maxtype),j=1,nlp),    & 
                       ((0.05/nlp, ii=1,maxtype),j=1,nlp),    &
                       ((0.05/nlp, ii=1,maxtype),j=1,nlp) /), &
                       (/maxtype,nl/))
cwalldepth= reshape((/ ((0.05/nlp, ii=1,maxtype),j=1,nlp),    &
                       ((0.05/nlp, ii=1,maxtype),j=1,nlp),    & 
                       ((0.05/nlp, ii=1,maxtype),j=1,nlp),    &
                       ((0.05/nlp, ii=1,maxtype),j=1,nlp) /), &
                       (/maxtype,nl/))
croaddepth= reshape((/ ((0.05/nlp, ii=1,maxtype),j=1,nlp),    &
                       ((0.05/nlp, ii=1,maxtype),j=1,nlp),    & 
                       ((0.05/nlp, ii=1,maxtype),j=1,nlp),    &
                       ((0.05/nlp, ii=1,maxtype),j=1,nlp) /), &
                       (/maxtype,nl/))
cslabdepth=reshape((/  ((0.05/nlp, ii=1,maxtype),j=1,nlp),    &
                       ((0.05/nlp, ii=1,maxtype),j=1,nlp),    & 
                       ((0.05/nlp, ii=1,maxtype),j=1,nlp),    &
                       ((0.05/nlp, ii=1,maxtype),j=1,nlp) /), &
                       (/maxtype,nl/))
! heat capacity (J m^-3 K^-1)
croofcp =   reshape((/ ((2.11E6, ii=1,maxtype),j=1,nlp),    & ! dense concrete (Oke 87)
                       ((2.11E6, ii=1,maxtype),j=1,nlp),    & ! dense concrete (Oke 87)
                       ((0.28E6, ii=1,maxtype),j=1,nlp),    & ! light concrete (Oke 87)
                       ((0.29E6, ii=1,maxtype),j=1,nlp) /), & ! insulation (Oke 87)
                       (/maxtype,nl/))
cwallcp =   reshape((/ ((1.55E6, ii=1,maxtype),j=1,nlp),    & ! concrete (Mills 93)
                       ((1.55E6, ii=1,maxtype),j=1,nlp),    & ! concrete (Mills 93)
                       ((1.55E6, ii=1,maxtype),j=1,nlp),    & ! concrete (Mills 93)
                       ((0.29E6, ii=1,maxtype),j=1,nlp) /), & ! insulation (Oke 87)
                       (/maxtype,nl/))
croadcp =   reshape((/ ((1.94E6, ii=1,maxtype),j=1,nlp),    & ! asphalt (Mills 93)
                       ((1.94E6, ii=1,maxtype),j=1,nlp),    & ! asphalt (Mills 93)
                       ((1.28E6, ii=1,maxtype),j=1,nlp),    & ! dry soil (Mills 93)
                       ((1.28E6, ii=1,maxtype),j=1,nlp) /), & ! dry soil (Mills 93)
                       (/maxtype,nl/))
cslabcp=   reshape((/  ((1.55E6, ii=1,maxtype),j=1,nlp),    & ! concrete (Mills 93)
                       ((1.55E6, ii=1,maxtype),j=1,nlp),    & ! concrete (Mills 93)
                       ((1.55E6, ii=1,maxtype),j=1,nlp),    & ! concrete (Mills 93)
                       ((1.55E6, ii=1,maxtype),j=1,nlp) /), & ! concrete (Mills 93)
                       (/maxtype,nl/))
! heat conductivity (W m^-1 K^-1)
crooflambda=reshape((/ ((1.5100, ii=1,maxtype),j=1,nlp),    & ! dense concrete (Oke 87)
                       ((1.5100, ii=1,maxtype),j=1,nlp),    & ! dense concrete (Oke 87)
                       ((0.0800, ii=1,maxtype),j=1,nlp),    & ! light concrete (Oke 87)
                       ((0.0500, ii=1,maxtype),j=1,nlp) /), & ! insulation (Oke 87)
                       (/maxtype,nl/))
cwalllambda=reshape((/ ((0.9338, ii=1,maxtype),j=1,nlp),    & ! concrete (Mills 93)
                       ((0.9338, ii=1,maxtype),j=1,nlp),    & ! concrete (Mills 93)
                       ((0.9338, ii=1,maxtype),j=1,nlp),    & ! concrete (Mills 93)
                       ((0.0500, ii=1,maxtype),j=1,nlp) /), & ! insulation (Oke 87)
                       (/maxtype,nl/))
croadlambda=reshape((/ ((0.7454, ii=1,maxtype),j=1,nlp),    & ! asphalt (Mills 93)
                       ((0.7454, ii=1,maxtype),j=1,nlp),    & ! asphalt (Mills 93)
                       ((0.2513, ii=1,maxtype),j=1,nlp),    & ! dry soil (Mills 93)
                       ((0.2513, ii=1,maxtype),j=1,nlp) /), & ! dry soil (Mills 93)
                       (/maxtype,nl/))
cslablambda=reshape((/ ((0.9338, ii=1,maxtype),j=1,nlp),    & ! concrete (Mills 93)
                       ((0.9338, ii=1,maxtype),j=1,nlp),    & ! concrete (Mills 93)
                       ((0.9338, ii=1,maxtype),j=1,nlp),    & ! concrete (Mills 93)
                       ((0.9338, ii=1,maxtype),j=1,nlp) /), & ! concrete (Mills 93)
                       (/maxtype,nl/))

itmp=pack(itype,upack)
if ((minval(itmp)<1).or.(maxval(itmp)>maxtype)) then
  write(6,*) "ERROR: Urban type is out of range"
  stop
end if

if (atebnmlfile/=0) then
  open(unit=atebnmlfile,file='uclem.nml',action="read",iostat=ierr)
  if (ierr==0) then
    write(6,*) "Reading uclem.nml"
    read(atebnmlfile,nml=atebnml)
    read(atebnmlfile,nml=atebgen)
    read(atebnmlfile,nml=atebtile)
    close(atebnmlfile)  
  end if
end if

! full in-canyon vegetation (vegmode=2 in original ateb)
tsigveg=csigvegc(itmp)
tsigmabld=csigmabld(itmp)

fp%sigmabld=max(min(tsigmabld,1.),0.)
!fp%hwratio=chwratio(itmp)*fp%sigmabld/(1.-fp%sigmabld) ! MJT suggested new definition
fp%hwratio=chwratio(itmp)          ! MJL simple definition

fp%industryfg=cindustryfg(itmp)
fp%intgains_flr=cintgains(itmp)
fp%trafficfg=ctrafficfg(itmp)
fp%bldheight=cbldheight(itmp)
fp_roof%alpha=croofalpha(itmp)
fp_wall%alpha=cwallalpha(itmp)
fp_road%alpha=croadalpha(itmp)
fp_roof%emiss=croofemiss(itmp)
fp_wall%emiss=cwallemiss(itmp)
fp_road%emiss=croademiss(itmp)
fp%bldairtemp=cbldtemp(itmp) - urbtemp
do ii=1,nl
  fp_roof%depth(:,ii)=croofdepth(itmp,ii)
  fp_wall%depth(:,ii)=cwalldepth(itmp,ii)
  fp_road%depth(:,ii)=croaddepth(itmp,ii)
  fp_roof%lambda(:,ii)=crooflambda(itmp,ii)
  fp_wall%lambda(:,ii)=cwalllambda(itmp,ii)
  fp_road%lambda(:,ii)=croadlambda(itmp,ii)
  fp_roof%volcp(:,ii)=croofcp(itmp,ii)
  fp_wall%volcp(:,ii)=cwallcp(itmp,ii)
end do

! for varying internal temperature
fp_slab%emiss = cslabemiss(itmp)
fp%infilach = cinfilach(itmp)
fp%ventilach = cventilach(itmp)
fp%tempheat = ctempheat(itmp)
fp%tempcool = ctempcool(itmp)
do ii=1,nl
  fp_slab%depth(:,ii)=cslabdepth(itmp,ii)
  fp_intm%depth(:,ii)=cslabdepth(itmp,ii)  ! internal mass material same as slab
  fp_slab%lambda(:,ii)=cslablambda(itmp,ii)
  fp_intm%lambda(:,ii)=cslablambda(itmp,ii)
  fp_slab%volcp(:,ii)=cslabcp(itmp,ii)
  fp_intm%volcp(:,ii)=cslabcp(itmp,ii)
end do

! Here we modify the effective canyon geometry to account for in-canyon vegetation tall vegetation
! fp%coeffbldheight = max(fp%bldheight-6.*cnveg%zo,0.2)/fp%bldheight
! emulate ateb cnveg%zo with czovegc
! fp%coeffbldheight = max(fp%bldheight-6.*czovegc(itmp),0.2)/fp%bldheight
fp%coeffbldheight = 1.
fp%effhwratio   = fp%hwratio*fp%coeffbldheight
! fp%effhwratio   = fp%hwratio

if (diag>=1) write(6,*) "initialising internal parameters"
call init_internal(fp)
if (diag>=1) write(6,*) "initialising lw coefficients"
call init_lwcoeff(fp,intl,ufull)

if ( diag>0 ) then
  write(6,*) 'hwratio, eff',fp%hwratio, fp%effhwratio
  write(6,*) 'bldheight, eff',fp%bldheight, fp%coeffbldheight
  write(6,*) 'sigmabld', fp%sigmabld
end if

return
end subroutine atebtype_thread

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This is the primary input/output subroutine called from uclemwrap
! The (tile) variables are used to emulate threading capability of ateb
subroutine internal_main(dt,temp_ext,varout_main,diag)

implicit none 

integer, intent(in) :: diag
real, intent(in)    :: temp_ext
real, intent(in)    :: dt
real, dimension(ufull,7), intent(out) :: varout_main
! threading emulation variables
integer tile, is, ie

varout_main = varout

do tile = 1,ntiles 
  
  ! emulate fractional time of day
  f_g(tile)%ctime = f_g(tile)%ctime + dt/86400.
  where (f_g(tile)%ctime > 1.)
    f_g(tile)%ctime=0.
  end where

  ! threading compatibility (not used here)
  is = (tile-1)*imax + 1
  ie = tile*imax
  if ( ufull_g(tile)>0 ) then
    call internal_calc( temp_ext, dt, f_g(tile), p_g(tile),                        & 
                        f_road(tile),f_roof(tile),f_wall(tile),                    &
                        intl_g(tile),f_slab(tile),f_intm(tile),                    &
                        road_g(tile), roof_g(tile), walle_g(tile), wallw_g(tile),  &
                        room_g(tile), slab_g(tile), intm_g(tile),                  &
                        ufull_g(tile), diag )
  end if
end do

end subroutine internal_main

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This subroutine is the main calculation loop. Order is:
!
! initialise varibles
! call getdiurnal()       : calculate time of day fluxes
! call solvecanyon()      : boundary conditions for facets (wall/roof/slab etc)
! call solvetridiag()     : calculate conduction through materials
! call energyclosure()    : check energy conservation
! pass outputs back up    : write outputs to file


subroutine internal_calc( temp_ext, ddt, fp, pd,              &
                          fp_road, fp_roof, fp_wall,          &
                          intl, fp_slab, fp_intm,             &
                          road, roof, walle, wallw,           &
                          room, slab, intm,                   &
                          ufull, diag )

implicit none

integer, intent(in) :: ufull
integer, intent(in) :: diag
real, intent(in)    :: ddt
real, intent(in)    :: temp_ext
! type data
type(fparmdata), intent(in)    :: fp
type(pdiagdata), intent(inout) :: pd
type(facetdata), intent(inout) :: road, roof, walle, wallw, room, slab, intm
type(facetparams), intent(in)  :: fp_intm, fp_road, fp_roof, fp_slab, fp_wall
type(intldata),    intent(in)  :: intl
! local data
real, dimension(ufull) :: cyc_traffic,cyc_basedemand,cyc_proportion,cyc_translation ! diurnal cycle coefficients       [-]
real, dimension(ufull) :: d_traf                                                    ! traffic flux                     [W/m2] (full domain)
real, dimension(ufull) :: d_intgains_bld                                            ! internal gains flux              [W/m2] (building)
real, dimension(ufull) :: fg_roof,fg_road,fg_walle,fg_wallw                         ! facet sensible heat fluxes       [W/m2] (facet)
real, dimension(ufull) :: rgint_roof,rgint_walle,rgint_wallw,rgint_slab,zero_flux   ! facet longwave radiation fluxes  [W/m2] (facet)
real, dimension(ufull) :: ggint_roof,ggint_walle,ggint_wallw,ggint_slab             ! facet boundary conduction fluxes [W/m2] (facet)
real, dimension(ufull) :: ggext_intm,ggint_intm                                     ! facet boundary conduction fluxes [W/m2] (facet)
! real, dimension(ufull) :: ggext_roof,ggext_walle,ggext_wallw,ggext_road,ggext_slab  ! facet boundary conduction fluxes [W/m2] (facet)
real, dimension(ufull) :: acond_roof,acond_road,acond_walle,acond_wallw             ! aerodynamic conduction (fixed)
real, dimension(ufull) :: d_tempr,d_tempc                                           ! roof/canyon effective temps      [K]
real, dimension(ufull) :: d_canyontemp                                              ! canyon air temperature           [K]
real, dimension(ufull) :: a_rho,int_airden                                           ! air density                      [kg/m3]
real, dimension(ufull) :: sg_roof,sg_road,sg_walle,sg_wallw                         ! dummy external sw fluxes 
real, dimension(ufull) :: rg_roof,rg_road,rg_walle,rg_wallw                         ! dummy external lw fluxes
real, dimension(ufull) :: int_infilflux                                             ! infiltration flux  
real, dimension(ufull) :: int_infilfg
real, dimension(ufull) :: d_ac_inside                                               ! air conditioning flux inside     [W/m2] (building)
real, dimension(ufull) :: d_ac_canyon                                               ! air conditioning flux incanyon   [W/m2] (canyond_topu)

if ( diag>=1 ) write(6,*) "Evaluating aTEB"

! -------Initialise variables to emulate ateb behaviour------------------------
d_tempr = temp_ext - urbtemp
d_tempc = temp_ext - urbtemp
a_rho = air_pr/(air_rd*temp_ext)
d_canyontemp = temp_ext -urbtemp
sg_roof     = 0.
sg_road     = 0.
sg_walle    = 0.
sg_wallw    = 0.
rg_roof     = 0.
rg_road     = 0.
rg_walle    = 0.
rg_wallw    = 0.
zero_flux   = 0.
! here use fixed values based on ISO6946:2007 (4 m/s)
acond_roof  = 25./(a_rho*aircp)
acond_walle = 25./(a_rho*aircp)
acond_wallw = 25./(a_rho*aircp)
acond_road  = 0. ! zero flux

! -------Calculate time of day fluxes------------------------------------------
! get diurnal cycle information for traffic, electricity and ac proportion in use
call getdiurnal(fp%ctime,cyc_traffic,cyc_basedemand,cyc_proportion,cyc_translation)

! if statistical method is off, remove statistical energy use diurnal adjustments
if (statsmeth==0) then
  cyc_basedemand=1.
  cyc_proportion=1.
  cyc_translation=0.
end if

! traffic sensible heat flux
pd%traf = fp%trafficfg*cyc_traffic
d_traf = pd%traf/(1.-fp%sigmabld)
! internal gains sensible heat flux
d_intgains_bld = (fp%intmassn+1.)*fp%intgains_flr*cyc_basedemand ! building internal gains 
pd%intgains_full= fp%sigmabld*d_intgains_bld                     ! full domain internal gains

!--------Calculate boundary conditions for facets (wall/roof/slab etc)---------
call solvecanyon( a_rho,int_airden,d_canyontemp,d_tempc,d_ac_canyon,d_ac_inside,         &
                  d_intgains_bld,int_infilflux,int_infilfg,                  &
                  ggint_roof,ggint_walle,ggint_wallw,ggint_slab,             &
                  ggext_intm,ggint_intm,cyc_proportion,ddt,                  &
                  rgint_roof,rgint_walle,rgint_wallw,rgint_slab,             &
                  fp,fp_intm,fp_roof,fp_wall,fp_slab,fp_road,intm,pd,road,   &
                  acond_roof,acond_walle,acond_wallw,acond_road,d_tempr,     &
                  fg_roof,fg_wallw,fg_walle,fg_road,                         &
                  sg_roof,sg_road,sg_walle,sg_wallw,                         &
                  rg_roof,rg_road,rg_walle,rg_wallw,                         & 
                  intl,roof,room,slab,walle,wallw,ufull,diag )

!--------Check energy conservation---------------------------------------------
call energyclosure(sg_roof,rg_roof,fg_roof,sg_walle,rg_walle,fg_walle,     &
                   sg_road,rg_road,fg_road,sg_wallw,rg_wallw,fg_wallw,     &
                   rgint_roof,rgint_walle,rgint_wallw,rgint_slab,          &
                   ggint_roof,ggint_walle,ggint_wallw,                     &
                   ggext_intm,ggint_slab,ggint_intm,d_intgains_bld,        &
                   int_infilflux,d_ac_inside,fp,ddt,                       &
                   fp_intm,fp_road,fp_roof,fp_slab,fp_wall,intm,pd,        &
                   road,roof,room,slab,walle,wallw,int_airden,ufull)

end subroutine internal_calc

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine solvecanyon( a_rho,int_airden,d_canyontemp,d_tempc,d_ac_canyon,d_ac_inside,    &
                        d_intgains_bld,int_infilflux,int_infilfg,                  &
                        ggint_roof,ggint_walle,ggint_wallw,ggint_slab,             &
                        ggext_intm,ggint_intm,cyc_proportion,ddt,                  &
                        rgint_roof,rgint_walle,rgint_wallw,rgint_slab,             &
                        fp,fp_intm,fp_roof,fp_wall,fp_slab,fp_road,intm,pd,road,   &
                        acond_roof,acond_walle,acond_wallw,acond_road,d_tempr,     &
                        fg_roof,fg_wallw,fg_walle,fg_road,                         &
                        sg_roof,sg_road,sg_walle,sg_wallw,                         &
                        rg_roof,rg_road,rg_walle,rg_wallw,                         & 
                        intl,roof,room,slab,walle,wallw,ufull,diag )
implicit none

! This subroutine is the stripped down equivalent of the aTEB version
! It calculates the boundary conditions for conduction through facets
! aTEB uses an iterative loop as the canyon temperature is updated
! here we eliminate the loop by setting ncyits=1
!
!START------Initialise variables -------------------------------------------------
!--+-+------Canyon/internal air temperature loop (default ncyits=6) -----------+-+
!  | |                                                                         | |
!  | |        (intairtmeth=0) Evaluating with fixed internal air temperature   | |
!  | +---0.1: Estimate internal surface convection coefficients                | |
!  | +---0.2: Calculate internal conducted flux (ggint)                        | |
!  | +---0.3: Calculate cooling/heating fluxes (fixed air temp)                | |
!  | +---0.4: Calculate acflux into canyon and update canyon temperature ------+ |
!  |                                                                             |
!  |          (intairtmeth=1) Evaluating with varying internal air temperature   |
!  +-----1.1: Estimate internal surface convection coefficients                  |
!  +-----1.2: Calculate air exchange from infiltration and open windows          |
!  +-----1.3: Calculate internal air temperature implicitly                      |
!  +-----1.4: Calculate cooling/heating fluxes (varying air temp)                |
!  +-----1.5: Calculate acflux into canyon and update canyon temperature         |
!  +--+--1.6: Finalise infiltration fluxes with new canyontemp-------------------+
!     |  Final loop        
!     +--1.6: Explicit fluxes based on implicit calculations of airtemp/nodetemp
!     |    +---check if taylor expansion estimate is necessary for stability
!     +--1.7: Calculate LW fluxes between internal surfaces for solvetridiag
!     +--1.8: Final update of room air temperature -----------------------END LOOP
!---------------------------------------------------------------------------------

integer l
integer, intent(in) :: diag
integer, intent(in) :: ufull
real, intent(in)    :: ddt
real, dimension(ufull), intent(in) :: a_rho
real, dimension(ufull), intent(out) :: int_airden
real, dimension(ufull), intent(in) :: cyc_proportion
real, dimension(ufull), intent(out) :: d_ac_inside
real, dimension(ufull), intent(out) :: d_canyontemp,d_tempc
real, dimension(ufull), intent(inout) :: d_ac_canyon
real, dimension(ufull), intent(inout) :: d_intgains_bld
real, dimension(ufull), intent(out) :: int_infilflux
real, dimension(ufull), intent(out) :: int_infilfg
real, dimension(ufull), intent(out) :: ggint_roof, ggint_walle, ggint_wallw
real, dimension(ufull), intent(out) :: ggint_slab, ggext_intm, ggint_intm
real, dimension(ufull), intent(out) :: rgint_roof,rgint_walle,rgint_wallw,rgint_slab
real, dimension(ufull) :: infl,can,rm,rf,we,ww,sl,im1,im2
real, dimension(ufull) :: ac_coeff, d_ac_cool, d_ac_heat
real, dimension(ufull) :: ac_load,d_ac_behavprop,d_openwindows,xtemp
real, dimension(ufull) :: cvcoeff_roof,cvcoeff_walle,cvcoeff_wallw                      
real, dimension(ufull) :: cvcoeff_slab,cvcoeff_intm1,cvcoeff_intm2
real, dimension(ufull) :: iroomtemp,itemp       ! implicit/iterative room temperature   [K-urbtemp]
real, dimension(ufull) :: infl_dynamic          ! dynamic infiltration flux          
real, dimension(ufull) :: d_topu                ! wind speed at top of canyon           [m/s]
real, dimension(ufull,4) :: newskintemps        ! estimate of internal skin temperatures at tau+1    [K]
real, dimension(ufull) :: dummy ! dummy variable - not used

real, dimension(ufull), intent(in)  :: sg_roof,sg_road,sg_walle,sg_wallw                         ! dummy external sw fluxes 
real, dimension(ufull), intent(in)  :: rg_roof,rg_road,rg_walle,rg_wallw                         ! dummy external lw fluxes
real, dimension(ufull), intent(in)  :: acond_roof,acond_walle,acond_wallw,acond_road,d_tempr
real, dimension(ufull), intent(out) :: fg_roof,fg_wallw,fg_walle,fg_road
real, dimension(ufull)              :: ggext_roof,ggext_walle,ggext_wallw,ggext_slab,ggext_road
real, dimension(ufull)              :: ggext_impl,ggint_impl,ggint_road,zero_flux
real, dimension(ufull,0:nl)         :: roof_inodetemp,walle_inodetemp,wallw_inodetemp
real, dimension(ufull,0:nl)         :: slab_inodetemp,road_inodetemp,intm_inodetemp

type(facetparams), intent(in) :: fp_intm, fp_roof, fp_wall,fp_slab,fp_road
type(facetdata), intent(inout) :: intm
type(facetdata), intent(inout) :: roof, slab
type(facetdata), intent(inout) :: road, room, walle, wallw
type(fparmdata), intent(in) :: fp
type(pdiagdata), intent(inout) :: pd
type(intldata), intent(in) :: intl

if ( diag>=1 ) write(6,*) "Evaluating solvecanyon"

! -------Initialise variables -------------------------------------------------

! first guess for canyon, room and facet temperature 
d_canyontemp    = d_tempc
iroomtemp       = room%nodetemp(:,1)
roof_inodetemp  = roof%nodetemp
walle_inodetemp = walle%nodetemp
wallw_inodetemp = wallw%nodetemp
road_inodetemp  = road%nodetemp
slab_inodetemp  = slab%nodetemp
intm_inodetemp  = intm%nodetemp

! emulate ateb top of canyon wind (here fixed)
d_topu = 3.


if ( conductmeth==0 ) then
  road%nodetemp(:,0)  = road%nodetemp(:,1)
  walle%nodetemp(:,0) = walle%nodetemp(:,1)
  wallw%nodetemp(:,0) = wallw%nodetemp(:,1)
end if

! -------Canyon/internal air temperature loop (here ncyits=1) -----------------

! Solve for canyon air temperature and water vapour mixing ratio
do l = 1,ncyits

  !!!!!!!!!!!!!!!!!!!! start interior models !!!!!!!!!!!!!!!!!!!!!!!!!!
  rgint_roof  = 0.
  rgint_walle = 0.
  rgint_wallw = 0.
  rgint_slab  = 0.
  zero_flux   = 0.
  ggint_slab  = 0.
  ggext_intm  = 0.
  ggint_intm  = 0.
  ! first internal temperature estimation - used for ggint calculation
  select case(intairtmeth)
    case(0)
      if ( diag>=1 ) write(6,*) "Evaluating energy use with fixed internal air temperature"

      ! ---0.1: Estimate internal surface convection coefficients ---------------
      call calc_convcoeff(cvcoeff_roof,cvcoeff_walle,cvcoeff_wallw,cvcoeff_slab,  & 
                          cvcoeff_intm1,cvcoeff_intm2,roof,room,slab,intm,ufull)
      ! ---0.2: Calculate internal conducted flux (ggint)  ----------------------
      call calc_ggint(fp_roof%depth(:,nl),fp_roof%volcp(:,nl),fp_roof%lambda(:,nl),   &
                      cvcoeff_roof,roof%nodetemp(:,nl),dummy,iroomtemp,               &
                      ggint_roof,ddt,ufull)
      call calc_ggint(fp_wall%depth(:,nl),fp_wall%volcp(:,nl),fp_wall%lambda(:,nl),   &
                      cvcoeff_walle,walle%nodetemp(:,nl),dummy,iroomtemp,             &
                      ggint_walle,ddt,ufull)
      call calc_ggint(fp_wall%depth(:,nl),fp_wall%volcp(:,nl),fp_wall%lambda(:,nl),   &
                      cvcoeff_wallw,wallw%nodetemp(:,nl),dummy,iroomtemp,             &
                      ggint_wallw,ddt,ufull)

      ! ---0.3: Calculate cooling/heating fluxes (fixed air temp) --------------
      ! flux into room potentially pumped out into canyon (depends on AC method)
      d_ac_inside = -ggint_roof - (ggint_walle+ggint_wallw)*(fp%bldheight/fp%bldwidth)

      ! update heat pumped into canyon
      where (d_ac_inside < 0.)
        ac_coeff = max(1.+acfactor*(d_canyontemp-iroomtemp)/(iroomtemp+urbtemp), 1.+1./ac_copmax) ! T&H2012 Eq. 10
      elsewhere 
        ac_coeff = 1.
      end where

      select case(acmeth) ! AC heat pump into canyon (0=Off, 1=On, 2=Reversible, COP of 1.0)
        case(0) ! unrealistic cooling (buildings act as heat sink)
          d_ac_canyon  = 0.
          pd%bldheat = max(0.,d_ac_inside*fp%sigmabld)
          pd%bldcool = max(0.,d_ac_inside*fp%sigmabld)
        case(1) ! d_ac_canyon pumps conducted heat + ac waste heat back into canyon
          d_ac_canyon = max(0.,-d_ac_inside*ac_coeff*fp%sigmabld/(1.-fp%sigmabld))       ! canyon domain W/m/m
          pd%bldheat = max(0.,d_ac_inside*fp%sigmabld)                                   ! entire domain W/m/m
          pd%bldcool = max(0.,-d_ac_inside*(ac_coeff-1.)*fp%sigmabld)                    ! entire domain W/m/m
        case(2) ! reversible heating and cooling (for testing energy conservation)
          d_ac_canyon  = -d_ac_inside*fp%sigmabld/(1.-fp%sigmabld)
          pd%bldheat = 0.
          pd%bldcool = 0.
        case DEFAULT
          write(6,*) "ERROR: Unknown acmeth mode ",acmeth
          stop
      end select

      ! ---0.4: Calculate acflux into canyon and update canyon temperature
      ! canyon temperature forced in UCLEM

    case(1)
      if ( diag>=1 ) write(6,*) "Evaluating energy use with varying internal air temperature"

      if (l==1) then

        ! ---1.1: Estimate internal surface convection coefficients -------------------
        call calc_convcoeff(cvcoeff_roof,cvcoeff_walle,cvcoeff_wallw,cvcoeff_slab,       & 
                            cvcoeff_intm1,cvcoeff_intm2,roof,room,slab,intm,ufull)

        newskintemps(:,1) = slab%nodetemp(:,nl)
        newskintemps(:,2) = wallw%nodetemp(:,nl)
        newskintemps(:,3) = roof%nodetemp(:,nl)
        newskintemps(:,4) = walle%nodetemp(:,nl)

      end if

      ! ---1.2: Calculate air exchange from infiltration and open windows -----
      call calc_openwindows(d_openwindows,fp,iroomtemp,d_canyontemp,roof,walle,wallw,slab,ufull)

      select case(infilmeth)
        case(0) ! constant
          infl_dynamic = fp%infilach
        case(1) ! EnergyPlus ewith BLAST coefficients (Coblenz and Achenbach, 1963)
          infl_dynamic = fp%infilach*(0.606 + 0.03636*abs(iroomtemp-d_canyontemp) + 0.1177*d_topu + 0.)
        case(2) ! AccuRate (Chen, 2010)
          ! not yet implemented
      end select

      ! ---1.3: Calculate internal air temperature implicitly -----------------
      ! int_airden = air_pr/(air_rd*(iroomtemp+urbtemp))
      int_airden = 1.0
      infl = aircp*a_rho*fp%bldheight*(infl_dynamic+d_openwindows*fp%ventilach)/3600.
      rm = aircp*int_airden*fp%bldheight/ddt
      rf = cvcoeff_roof
      we = (fp%bldheight/fp%bldwidth)*cvcoeff_walle
      ww = (fp%bldheight/fp%bldwidth)*cvcoeff_wallw
      sl = cvcoeff_slab
      im1 = cvcoeff_intm1*real(fp%intmassn)
      im2 = cvcoeff_intm2*real(fp%intmassn)

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !! fully implicit - old temps with acflux included

      iroomtemp = (rm*room%nodetemp(:,1)     & ! room temperature
                 + rf*roof%nodetemp(:,nl)    & ! roof conduction
                 + we*walle%nodetemp(:,nl)   & ! wall conduction east
                 + ww*wallw%nodetemp(:,nl)   & ! wall conduction west
                 + sl*slab%nodetemp(:,nl)    & ! slab conduction
                 + im1*intm%nodetemp(:,0)    & ! mass conduction side 1
                 + im2*intm%nodetemp(:,nl)   & ! mass conduction side 2
                 + infl*d_canyontemp         & ! infiltration
                 + d_ac_inside               & ! ac flux (iterative)
                 + d_intgains_bld            & ! internal gains (explicit)
                 )/(rm+rf+we+ww+sl+im1+im2+infl)

      !! diagnose fluxes from implicit results
      ggint_roof  = cvcoeff_roof*(roof%nodetemp(:,nl)-iroomtemp)
      ggint_slab  = cvcoeff_slab*(slab%nodetemp(:,nl)-iroomtemp)
      ggint_walle = cvcoeff_walle*(walle%nodetemp(:,nl)-iroomtemp)
      ggint_wallw = cvcoeff_wallw*(wallw%nodetemp(:,nl)-iroomtemp)
      if ( intmassmeth/=0 ) then
        ggext_intm  = -cvcoeff_intm1*(intm%nodetemp(:,0)-iroomtemp) ! negative flux leaving facet
        ggint_intm  = cvcoeff_intm2*(intm%nodetemp(:,nl)-iroomtemp) ! positive flux leaving facet
      end if
      int_infilflux = infl*(d_canyontemp-iroomtemp)
      int_infilfg = (fp%sigmabld/(1.-fp%sigmabld))*int_infilflux

      ! ---1.4: Calculate cooling/heating fluxes (varying air temp)------------

      ! remove acflux component added to implicit formulation of air temp
      itemp = iroomtemp - d_ac_inside*ddt/(aircp*int_airden*fp%bldheight)

      d_ac_cool = 0.
      d_ac_heat = 0.
      select case(behavmeth)
        case(0) ! asynchronous heating and cooling, no behavioural smoothing
          ! heating load
          where ( itemp < (fp%bldairtemp - ac_deltat) )
            ac_load = (int_airden*aircp*fp%bldheight/ddt)*(fp%bldairtemp-ac_deltat-itemp)
            d_ac_heat = min(ac_heatcap*fp%bldheight,ac_load)*ac_heatprop*cyc_proportion
          end where
          ! cooling load
          where ( itemp > (fp%bldairtemp + ac_deltat) )
            ac_load = (int_airden*aircp*fp%bldheight/ddt)*(itemp-fp%bldairtemp-ac_deltat)
            d_ac_cool = min(ac_coolcap*fp%bldheight,ac_load)*ac_coolprop*cyc_proportion
          end where
        case(1) ! synchronous heating and cooling, behavioural smoothing
          ! heating load
          xtemp = itemp - (fp%bldairtemp - ac_deltat)
          d_ac_behavprop = (tanh(ac_smooth*xtemp)+1.)/2.               ! Eq 5 from MJT 2007
          d_ac_heat = ac_heatcap*ac_heatprop*fp%bldheight*(1.-d_ac_behavprop)*cyc_proportion
          ! cooling load
          xtemp = itemp - (fp%bldairtemp + ac_deltat)
          d_ac_behavprop = (tanh(ac_smooth*xtemp)+1.)/2.               ! Eq 5 from MJT 2007
          d_ac_cool = ac_coolcap*ac_coolprop*fp%bldheight*d_ac_behavprop*cyc_proportion
        case DEFAULT
          write(6,*) "ERROR: Unknown behavmeth mode ",behavmeth
          stop
      end select

      d_ac_inside = d_ac_heat-d_ac_cool

      ! replace acflux component into air temp calculation
      ! iroomtemp = itemp + d_ac_inside*ddt/(aircp*int_airden*fp%bldheight)

      !! explicit roomtemp calculation
      iroomtemp = room%nodetemp(:,1) + ddt/(aircp*int_airden*fp%bldheight) &
                * (ggint_roof + ggint_slab                                 &
                + real(fp%intmassn)*(ggint_intm - ggext_intm)              &
                + fp%bldheight/fp%bldwidth*(ggint_walle + ggint_wallw)     &
                + int_infilflux + d_ac_inside + d_intgains_bld)

      ! HVAC electricity demand (per building m2)
      varout(:,7) = max(0.,d_ac_heat) - max(0.,d_ac_cool*(ac_coeff-1.))

      ! ---1.5: Calculate acflux into canyon and update canyon temperature-----

      where (d_ac_cool > 0.)
        ac_coeff = max(1.+acfactor*(d_canyontemp-iroomtemp)/(iroomtemp+urbtemp), 1.+1./ac_copmax) ! T&H2012 Eq. 10
      elsewhere 
        ac_coeff = 1.
      end where

      select case(acmeth) ! AC heat pump into canyon (0=Off, 1=On, 2=Reversible)
        case(0) ! unrealistic cooling (buildings act as heat sink)
          d_ac_canyon  = 0.
          pd%bldheat = max(0.,d_ac_inside*fp%sigmabld)
          pd%bldcool = max(0.,d_ac_inside*fp%sigmabld)
          write (6,*) "ERROR: acmeth 0 not tested with internal physics on"
          stop
        case(1) ! d_ac_canyon pumps conducted heat + ac waste heat back into canyon
          d_ac_canyon = max(0.,d_ac_cool*ac_coeff*fp%sigmabld/(1.-fp%sigmabld))        ! canyon domain W/m/m
          pd%bldheat = max(0.,d_ac_heat*fp%sigmabld)                                   ! entire domain W/m/m
          pd%bldcool = max(0.,d_ac_cool*(ac_coeff-1.)*fp%sigmabld)                     ! entire domain W/m/m
        case(2) ! reversible heating and cooling (for testing energy conservation)
          d_ac_canyon  = -d_ac_inside*fp%sigmabld/(1.-fp%sigmabld)
          pd%bldheat = 0.
          pd%bldcool = 0.
          write (6,*) "ERROR: acmeth 2 not tested with internal physics on"
          stop
        case DEFAULT
          write(6,*) "ERROR: Unknown acmeth mode ",acmeth
          stop
      end select    

      int_infilfg = can*int_infilflux  
      
      if ( l==ncyits ) then

        ! if explicit calculations are causing internal skin temperature oscillations, then
        ! run taylor expansion estimate of future skin/air temp to reduce temperature gradient

        if ( (any(abs(roof%nodetemp(:,nl)-slab%nodetemp(:,nl))>10.)) .or.  & 
             (any(abs(walle%nodetemp(:,nl)-wallw%nodetemp(:,nl))>10.)) ) then
          
          call calc_ggint(fp_slab%depth(:,nl),fp_slab%volcp(:,nl),fp_slab%lambda(:,nl),   &
                          cvcoeff_slab,slab%nodetemp(:,nl),newskintemps(:,1),iroomtemp,   &
                          ggint_slab,ddt,ufull)
          call calc_ggint(fp_wall%depth(:,nl),fp_wall%volcp(:,nl),fp_wall%lambda(:,nl),   &
                          cvcoeff_wallw,wallw%nodetemp(:,nl),newskintemps(:,2),iroomtemp, &
                          ggint_wallw,ddt,ufull)
          call calc_ggint(fp_roof%depth(:,nl),fp_roof%volcp(:,nl),fp_roof%lambda(:,nl),   &
                          cvcoeff_roof,roof%nodetemp(:,nl),newskintemps(:,3),iroomtemp,   &
                          ggint_roof,ddt,ufull)
          call calc_ggint(fp_wall%depth(:,nl),fp_wall%volcp(:,nl),fp_wall%lambda(:,nl),   &
                          cvcoeff_walle,walle%nodetemp(:,nl),newskintemps(:,4),iroomtemp, &
                          ggint_walle,ddt,ufull)

          if (intmassmeth/=0) then
            call calc_ggint(fp_intm%depth(:,0),fp_intm%volcp(:,0),fp_intm%lambda(:,0),  &
                            cvcoeff_intm1,intm%nodetemp(:,0),dummy,iroomtemp,              &
                            ggext_intm,ddt,ufull)
            ggext_intm = -1.*ggext_intm

            call calc_ggint(fp_intm%depth(:,nl),fp_intm%volcp(:,nl),fp_intm%lambda(:,nl),  &
                            cvcoeff_intm2,intm%nodetemp(:,nl),dummy,iroomtemp,              &
                            ggint_intm,ddt,ufull)
          end if

          ! for taylor expansion, close energy budget with residual into slab
          ggint_slab = (aircp*int_airden*fp%bldheight/ddt)*(iroomtemp-room%nodetemp(:,1))  &
                     - (ggint_walle + ggint_wallw)*fp%bldheight/fp%bldwidth                &
                     - ggint_roof - fp%intmassn*(-ggext_intm + ggint_intm)                  &
                     - int_infilflux - d_ac_inside - d_intgains_bld

        end if

        ! ---1.7:Calculate LW fluxes between internal surfaces for solvetridiag ----------
            
        call internal_lwflux(rgint_slab,rgint_wallw,rgint_roof,rgint_walle,    &
                             newskintemps,fp,intl,ufull)

        room%nodetemp(:,1) = iroomtemp

      end if
           
    case DEFAULT
      write(6,*) "ERROR: Unknown intairtmeth mode ",intairtmeth
      stop
  end select
end do


!--------Solve conduction through facets with explicit interior boundary-----------
! tridiagonal solver coefficents for calculating roof, road and wall temperatures
ggext_roof = sg_roof  +rg_roof  +aircp*a_rho*d_tempr*acond_roof
ggext_impl = aircp*a_rho*acond_roof  ! later update fg_roof with final roof skin T
! ggint_roof = cvcoeff_roof*(roof%nodetemp(:,nl)-room%nodetemp(:,1))
ggint_impl = 0.
call solvetridiag(ggext_roof,ggint_roof,rgint_roof,ggext_impl,ggint_impl,      &
                  roof%nodetemp,fp_roof%depth,fp_roof%volcp,fp_roof%lambda,ddt,ufull)

ggext_walle= sg_walle +rg_walle +aircp*a_rho*d_canyontemp*acond_walle*fp%coeffbldheight
ggext_impl = aircp*a_rho*acond_walle*fp%coeffbldheight ! later update fg_walle with final walle skin T
! ggint_walle= cvcoeff_walle*(walle%nodetemp(:,nl)-room%nodetemp(:,1))
ggint_impl = 0.
call solvetridiag(ggext_walle,ggint_walle,rgint_walle,ggext_impl,ggint_impl,  &
                  walle%nodetemp,fp_wall%depth,fp_wall%volcp,fp_wall%lambda,ddt,ufull)

ggext_wallw= sg_wallw +rg_wallw +aircp*a_rho*d_canyontemp*acond_wallw*fp%coeffbldheight
ggext_impl = aircp*a_rho*acond_wallw*fp%coeffbldheight ! later update fg_wallw with final wallw skin T
! ggint_wallw= cvcoeff_wallw*(wallw%nodetemp(:,nl)-room%nodetemp(:,1))
ggint_impl = 0.
call solvetridiag(ggext_wallw,ggint_wallw,rgint_wallw,ggext_impl,ggint_impl,  &
                  wallw%nodetemp,fp_wall%depth,fp_wall%volcp,fp_wall%lambda,ddt,ufull)

ggext_road = sg_road  +rg_road  +aircp*a_rho*d_canyontemp*acond_road
ggext_impl = 0.  ! UCLEM UPDATE TO REMOVE ROAD FLUX
ggint_road = 0.  ! zero_flux boundary condition
ggint_impl = 0.  ! zero_flux boundary condition
call solvetridiag(ggext_road,ggint_road,zero_flux,ggext_impl,ggint_impl,     &
                  road%nodetemp,fp_road%depth,fp_road%volcp,fp_road%lambda,ddt,ufull)

! calculate internal facet conduction
if ( intairtmeth==1 ) then
  ggext_slab = 0.  ! zero_flux boundary condition
  ggext_impl = 0.  ! zero_flux boundary condition
  ! ggint_slab = cvcoeff_slab*(slab%nodetemp(:,nl)-room%nodetemp(:,1))
  ggint_impl = 0.  ! explicit boundary condition
  call solvetridiag(ggext_slab,ggint_slab,rgint_slab,ggext_impl,ggint_impl,       &
                    slab%nodetemp,fp_slab%depth,fp_slab%volcp,fp_slab%lambda,ddt,ufull)

  if ( intmassmeth/=0 ) then
    ggext_intm = cvcoeff_intm2*(intm%nodetemp(:,0)-room%nodetemp(:,1))
    ggext_impl = 0.  ! explicit boundary condition
    ! ggint_intm = cvcoeff_intm2*(intm%nodetemp(:,nl)-room%nodetemp(:,1))
    ggint_impl = 0.  ! explicit boundary condition
    call solvetridiag(ggext_intm,ggint_intm,zero_flux,ggext_impl,ggint_impl, &
                      intm%nodetemp,fp_intm%depth,fp_intm%volcp,fp_intm%lambda,ddt,ufull)
  end if
end if

! implicit update for fg to improve stability for thin layers
fg_roof  = aircp*a_rho*(roof%nodetemp(:,0)-d_tempr)*acond_roof
fg_walle = aircp*a_rho*(walle%nodetemp(:,0)-d_canyontemp)*acond_walle*fp%coeffbldheight
fg_wallw = aircp*a_rho*(wallw%nodetemp(:,0)-d_canyontemp)*acond_wallw*fp%coeffbldheight
fg_road  = aircp*a_rho*(road%nodetemp(:,0)-d_canyontemp)*acond_road

! if ( 1./(ac_coeff(1) - 1.)<12. ) print *, 'COP: ', 1./(ac_coeff(1) - 1.)

! print *, 'open window proportion: ', d_openwindows
! print *, 'diurnal cycle ac proportion: ', cyc_proportion
! print *, 'comfort temperature ac proportion: ', d_ac_behavprop
! print *, ' '

return
end subroutine solvecanyon

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Estimate of internal skin surface temperatures at tau+1 using a Taylor expansion
! This Taylor expansion is necessary for stability when internal wall/roof layer 
! has low heat capacity (insulation).
! May be depreciated in future.

subroutine calc_ggint(depth,volcp,lambda,cvcoeff,skintemp,      &
                      newskintemp,newairtemp,ggint,ddt,ufull)

implicit none

integer, intent(in) :: ufull
real, intent(in)                    :: ddt
real, intent(in), dimension(ufull)  :: depth,volcp,lambda,cvcoeff
real, intent(in), dimension(ufull)  :: skintemp, newairtemp
real, intent(out), dimension(ufull) :: newskintemp,ggint
real, dimension(ufull) :: condterm

select case(conductmeth)
  case(0) ! half-layer conduction
    condterm = 1./(0.5*depth/lambda +1./cvcoeff)
    newskintemp  = skintemp-condterm*(skintemp-newairtemp) &
                    /(volcp*depth/ddt+condterm)

  case(1) ! interface conduction
    condterm = cvcoeff
    newskintemp  = skintemp-condterm*(skintemp-newairtemp) &
                /(0.5*volcp*depth/ddt+condterm)
end select

ggint = condterm*(newskintemp-newairtemp)

end subroutine calc_ggint

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This subroutine calculates net longwave radiation flux (flux_rg) at each surface
! longwave flux is temperature dependent, so this subroutine should be run at each timestep
subroutine internal_lwflux(rgint_slab,rgint_wallw,rgint_roof,rgint_walle, &
                           skintemps,fp,intl,ufull)

implicit none
integer, intent(in) :: ufull
integer :: j
real, dimension(ufull,4), intent(in) :: skintemps  ! floor, wall, ceiling, wall temperature array
real, dimension(ufull,4) :: epsil     ! floor, wall, ceiling, wall emissivity array
real, dimension(ufull,4) :: radnet    ! net flux density on ith surface (+ve leaving)
real, dimension(ufull)   :: radtot    ! net leaving flux density (B) on ith surface
real, dimension(ufull), intent(out) :: rgint_slab,rgint_wallw,rgint_roof,rgint_walle
type(intldata), intent(in) :: intl
type(fparmdata), intent(in) :: fp

radnet = 0.
epsil  = 0.9

!epsil = reshape((/(fp_slab%emiss,fp_wall%emiss,fp_roof%emiss,fp_wall%emiss, & 
!                    i=1,ufull)/), (/ufull,4/))

do j = 1,4
  radnet(:,j) = epsil(:,j)/(1. -epsil(:,j))*((sbconst*(skintemps(:,j) +urbtemp)**4)  & 
               - sum(intl%psi(:,j,:)*(sbconst*(skintemps(:,:) +urbtemp)**4),dim=2))
end do

! MJT suggestion
radnet(:,1) = -fp%bldheight*(radnet(:,2)+radnet(:,4))/fp%bldwidth - radnet(:,3)

! energy conservation check
radtot(:) = abs(fp%bldwidth(:)*(radnet(:,1)+radnet(:,3)) + fp%bldheight*(radnet(:,2)+radnet(:,4)))

do j = 1,ufull
  if ( radtot(j)>energytol ) write(6,*) "error: radiation energy non-closure: ", radtot(j)
end do

rgint_slab  = real(radnet(:,1))
rgint_wallw = real(radnet(:,2))
rgint_roof  = real(radnet(:,3))
rgint_walle = real(radnet(:,4))

return
end subroutine internal_lwflux

! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Tridiagonal solver for temperatures

! This version has an implicit estimate for roof sensible heat flux

! [ ggB ggC         ] [ temp ] = [ ggD ]
! [ ggA ggB ggC     ] [ temp ] = [ ggD ]
! [     ggA ggB ggC ] [ temp ] = [ ggD ]
! [         ggA ggB ] [ temp ] = [ ggD ]

subroutine solvetridiag(ggext,ggint,rgint,ggext_impl,ggint_impl,    &
                        nodetemp,depth,volcp,lambda,ddt,ufull)

implicit none

integer, intent(in) :: ufull
real, dimension(ufull),     intent(in)    :: ggext,ggint,rgint  ! surface energy fluxes
real, dimension(ufull),     intent(in)    :: ggext_impl         ! implicit update for stability
real, dimension(ufull),     intent(in)    :: ggint_impl         ! implicit update for stability
real, dimension(ufull,0:nl),intent(inout) :: nodetemp           ! temperature of each node
real, dimension(ufull,nl),  intent(in)    :: depth,volcp,lambda ! facet depth, heat capacity, conductivity
real(kind=8), dimension(ufull,nl)         :: cap,res            ! layer capacitance & resistance
real(kind=8), dimension(ufull,0:nl)       :: ggA,ggB,ggC,ggD    ! tridiagonal matrices
real(kind=8), dimension(ufull)            :: ggX                ! tridiagonal coefficient
real(kind=8), dimension(ufull)            :: ans                ! tridiagonal solution
real, intent(in)                          :: ddt                ! timestep
integer k

res = real(depth,8)/real(lambda,8)
cap = real(depth,8)*real(volcp,8)

select case(conductmeth)
  case(0) !!!!!!!!! half-layer conduction !!!!!!!!!!!
    ggA(:,1)      =-2./res(:,1)
    ggA(:,2:nl)   =-2./(res(:,1:nl-1) +res(:,2:nl))
    ggB(:,0)      = 2./res(:,1) + ggext_impl
    ggB(:,1)      = 2./res(:,1) +2./(res(:,1)+res(:,2)) + cap(:,1)/ddt
    ggB(:,2:nl-1) = 2./(res(:,1:nl-2) +res(:,2:nl-1)) +2./(res(:,2:nl-1) & 
                      +res(:,3:nl)) +cap(:,2:nl-1)/ddt
    ggB(:,nl)     = 2./(res(:,nl-1)+res(:,nl)) + cap(:,nl)/ddt + ggint_impl
    ggC(:,0)      =-2./res(:,1)
    ggC(:,1:nl-1) =-2./(res(:,1:nl-1)+res(:,2:nl))
    ggD(:,0)      = ggext
    ggD(:,1:nl-1) = nodetemp(:,1:nl-1)*cap(:,1:nl-1)/ddt
    ggD(:,nl)     = nodetemp(:,nl)*cap(:,nl)/ddt - ggint - rgint
  case(1) !!!!!!!!! interface conduction !!!!!!!!!!!
    ggA(:,1:nl)   = -1./res(:,1:nl)
    ggB(:,0)      =  1./res(:,1) +0.5*cap(:,1)/ddt + ggext_impl
    ggB(:,1:nl-1) =  1./res(:,1:nl-1) +1./res(:,2:nl) +0.5*(cap(:,1:nl-1) &
                       +cap(:,2:nl))/ddt
    ggB(:,nl)     =  1./res(:,nl) + 0.5*cap(:,nl)/ddt + ggint_impl
    ggC(:,0:nl-1) = -1./res(:,1:nl)
    ggD(:,0)      = nodetemp(:,0)*0.5*cap(:,1)/ddt + ggext
    ggD(:,1:nl-1) = nodetemp(:,1:nl-1)*0.5*(cap(:,1:nl-1)+cap(:,2:nl))/ddt
    ggD(:,nl)     = nodetemp(:,nl)*0.5*cap(:,nl)/ddt - ggint - rgint
end select
! tridiagonal solver (Thomas algorithm) to solve node temperatures
do k=1,nl
  ggX(:)   = ggA(:,k)/ggB(:,k-1)
  ggB(:,k) = ggB(:,k)-ggX(:)*ggC(:,k-1)
  ggD(:,k) = ggD(:,k)-ggX(:)*ggD(:,k-1)
end do
ans = ggD(:,nl)/ggB(:,nl)
nodetemp(:,nl) = real(ans)
do k=nl-1,0,-1
  ans = (ggD(:,k) - ggC(:,k)*ans)/ggB(:,k)
  nodetemp(:,k) = real(ans)
end do

end subroutine solvetridiag

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This subroutine sets internal surface convective heat transfer coefficients
! Compares temperature of innermost layer temperature with air temperature
! Considers horizontal and vertical orientation
! Based on EnergyPlus: Simple Natural Convection Algorithm [W m^-2 K^-1]

subroutine calc_convcoeff(cvcoeff_roof,cvcoeff_walle,cvcoeff_wallw,cvcoeff_slab, & 
                          cvcoeff_intm1,cvcoeff_intm2,roof,room,slab,intm,ufull)
implicit none

integer, intent(in) :: ufull
real, dimension(ufull), intent(out) :: cvcoeff_roof,cvcoeff_walle,cvcoeff_wallw
real, dimension(ufull), intent(out) :: cvcoeff_intm1,cvcoeff_intm2,cvcoeff_slab
type(facetdata), intent(in) :: roof
type(facetdata), intent(in) :: room
type(facetdata), intent(in) :: slab
type(facetdata), intent(in) :: intm

select case(cvcoeffmeth)
  case(0) ! DOE Simple
    cvcoeff_walle = 3.067    ! vertical surface coefficient constant
    cvcoeff_wallw = 3.067    ! vertical surface coefficient constant
    where ( roof%nodetemp(:,nl)>=room%nodetemp(:,1) )
      cvcoeff_roof(:)=0.948  ! reduced convection
    elsewhere
      cvcoeff_roof(:)=4.040  ! enhanced convection  
    end where    
    cvcoeff_intm1 = 3.067   ! vertical surface coefficient constant
    cvcoeff_intm2 = 3.067   ! vertical surface coefficient constant
    where (slab%nodetemp(:,nl)<=room%nodetemp(:,1))
      cvcoeff_slab(:)=0.948  ! reduced convection
    elsewhere
      cvcoeff_slab(:)=4.040  ! enhanced convection
    end where
  case(1) ! dynamic, from international standard ISO6946:2007, Annex A
    cvcoeff_walle = 2.5    ! vertical surface coefficient constant
    cvcoeff_wallw = 2.5    ! vertical surface coefficient constant
    where ( roof%nodetemp(:,nl)>=room%nodetemp(:,1) )
      cvcoeff_roof(:)=0.7  ! reduced convection (upper surface)
    elsewhere
      cvcoeff_roof(:)=5.0  ! enhanced convection (upper surface)  
    end where
    where ( intm%nodetemp(:,nl)>=room%nodetemp(:,1) )
      cvcoeff_intm2 = 0.7+5.7   ! reduced convection (upper surface) + radiation @20 deg C
    elsewhere
      cvcoeff_intm2 = 5.0+5.7   ! reduced convection (upper surface) + radiation @20 deg C
    end where
    where ( intm%nodetemp(:,0)<=room%nodetemp(:,1) )    
      cvcoeff_intm1 = 0.7+5.7   ! reduced convection (lower surface) + radiation @20 deg C
    elsewhere
      cvcoeff_intm1 = 5.0+5.7   ! reduced convection (lower surface) + radiation @20 deg C
    end where
    where (slab%nodetemp(:,nl)<=room%nodetemp(:,1))
      cvcoeff_slab(:)=0.7   ! reduced convection (lower surface)
    elsewhere
      cvcoeff_slab(:)=5.0   ! enhanced convection (lower surface)
    end where
  case(2) ! fixed, from international standard IS6946:2007, 5.2 
    cvcoeff_roof  = 1./0.10 ! fixed coefficient up
    cvcoeff_walle = 1./0.13 ! fixed coefficient horizontal
    cvcoeff_wallw = 1./0.13 ! fixed coefficient horizontal
    cvcoeff_intm1 = 1./0.10 ! fixed coefficient up
    cvcoeff_intm2 = 1./0.17 ! fixed coefficient down
    cvcoeff_slab  = 1./0.17 ! fixed coefficient down
end select

end subroutine calc_convcoeff

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This subroutine calculates the proportion of open windows for ventilation
! It has not been comprehensively tested

subroutine calc_openwindows(d_openwindows,fp,iroomtemp,d_canyontemp, &
                            roof,walle,wallw,slab,ufull)
implicit none

integer, intent(in)                 :: ufull
real, dimension(ufull), intent(in)  :: d_canyontemp,iroomtemp
real, dimension(ufull), intent(out) :: d_openwindows
real, dimension(ufull)              :: xtemp, mrt
type(facetdata), intent(in) :: roof, walle, wallw, slab
type(fparmdata), intent(in) :: fp

! mean radiant temperature estimation
mrt = 0.5*(fp%bldheight/(fp%bldwidth+fp%bldheight)*(walle%nodetemp(:,nl) + wallw%nodetemp(:,nl))) & 
    + 0.5*(fp%bldwidth/ (fp%bldwidth+fp%bldheight)*(roof%nodetemp(:,nl) + slab%nodetemp(:,nl)))
! globe temperature approximation (average of mrt and air temperature) [Celsius]
xtemp = 0.5*(iroomtemp + mrt) + urbtemp - 273.15

select case(behavmeth)
  case(0)
    where (xtemp>26.)
      d_openwindows=1.0
    elsewhere
      d_openwindows=0.
    end where
  case(1)
    ! smooth function based on Rijal et al., 2007
    d_openwindows = 1./(1. + exp( 0.5*(23.-xtemp) ))*1./(1. + exp( (d_canyontemp-iroomtemp) ))
end select

return
end subroutine calc_openwindows

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This subroutine initialises internal variables; 
! building width and number of internal mass floors

subroutine init_internal(fp)


implicit none

type(fparmdata), intent(inout) :: fp

fp%bldwidth = fp%sigmabld*(fp%bldheight/fp%hwratio)/(1.-fp%sigmabld)
! define number of internal mass floors (based on building height)
select case(intmassmeth)
  case(0) ! no internal mass
    fp%intmassn = 0
  case(1) ! one floor of internal mass
    fp%intmassn = 1
  case(2) ! dynamic floors of internal mass
    fp%intmassn = max((nint(fp%bldheight/3.)-1),0)
end select

print *, 'fp%bldwidth',fp%bldwidth
end subroutine init_internal

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This subroutine calculates longwave reflection coefficients (int_psi) at each surface
! longwave coefficients do not change, so this subroutine should only be run once
! Infinite reflections per Harman et al., (2004) "Radiative Exchange in Urban Street Canyons"
! Per method in "Radiation Heat Transfer, Sparrow & Cess 1978, Ch 3-3"
! array surface order is: (1) floor; (2) wallw; (3) ceiling; (4) walle

subroutine init_lwcoeff(fp,intl,ufull)

! local variables
integer, intent(in) :: ufull
real, dimension(ufull,4,4) :: chi
real, dimension(4,4)       :: krondelta
real, dimension(ufull)     :: h, w
real, dimension(ufull,4)   :: epsil   ! floor, wall, ceiling, wall emissivity array
integer :: i, j
integer :: ierr       ! inverse matrix error flag
type(intldata), intent(inout) :: intl
type(fparmdata), intent(in) :: fp


krondelta = 0.
chi = 0.
intl%psi = 0.
h = fp%bldheight
w = fp%sigmabld*(fp%bldheight/fp%hwratio)/(1.-fp%sigmabld)

! set int_vfactors
intl%viewf(:,1,1) = 0.                                    ! floor to self
intl%viewf(:,1,2) = 0.5*(1.+(h/w)-sqrt(1.+(h/w)**2))      ! floor to wallw
intl%viewf(:,1,3) = sqrt(1.+(h/w)**2)-(h/w)               ! floor to ceiling
intl%viewf(:,1,4) = intl%viewf(:,1,2)                     ! floor to walle
intl%viewf(:,2,1) = 0.5*(1.+(w/h)-sqrt(1.+(w/h)**2))      ! wallw to floor
intl%viewf(:,2,2) = 0.                                    ! wallw to self
intl%viewf(:,2,3) = intl%viewf(:,2,1)                     ! wallw to ceiling
intl%viewf(:,2,4) = sqrt(1.+(w/h)**2)-(w/h)               ! wallw to walle
intl%viewf(:,3,1) = intl%viewf(:,1,3)                     ! ceiling to floor
intl%viewf(:,3,2) = intl%viewf(:,1,2)                     ! ceiling to wallw
intl%viewf(:,3,3) = 0.                                    ! ceiling to self
intl%viewf(:,3,4) = intl%viewf(:,1,2)                     ! ceiling walle
intl%viewf(:,4,1) = intl%viewf(:,2,1)                     ! walle to floor
intl%viewf(:,4,2) = intl%viewf(:,2,4)                     ! walle to wallw
intl%viewf(:,4,3) = intl%viewf(:,2,1)                     ! walle to ceiling
intl%viewf(:,4,4) = 0.                                    ! walle to self

!epsil = reshape((/(f_slab%emiss,f_wall%emiss,f_roof%emiss,f_wall%emiss, & 
!                    i=1,ufull_g)/), (/ufull_g,4/))
epsil = 0.9

do i = 1,4
  krondelta(i,i) = 1.
end do
do j = 1,4
  do i = 1,4
    chi(:,i,j) = (krondelta(i,j) - (1.-epsil(:,i))*intl%viewf(:,i,j))/(epsil(:,i))
  end do
end do

! invert matrix
intl%psi = chi
call minverse(intl%psi,ierr)

end subroutine init_lwcoeff

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Define weights during the diurnal cycle. Array starts at 1am.

subroutine getdiurnal(fp_ctime,icyc_traffic,icyc_basedemand,icyc_proportion,icyc_translation)                  

implicit none

real, dimension(:), intent(in) :: fp_ctime
real, dimension(:), intent(out) :: icyc_traffic,icyc_basedemand,icyc_proportion,icyc_translation
real, dimension(size(fp_ctime)) :: real_p
integer, dimension(size(fp_ctime)) :: int_p

! traffic diurnal cycle weights approximated from Chapman et al., 2016
real, dimension(25), parameter :: trafcycle = (/ 0.17, 0.12, 0.12, 0.17, 0.37, & 
                                                 0.88, 1.29, 1.48, 1.37, 1.42, 1.5 , &
                                                 1.52, 1.5 , 1.57, 1.73, 1.84, 1.84, &
                                                 1.45, 1.01, 0.77,0.65, 0.53, 0.41, 0.27, 0.17 /)
! base electricity demand cycle weights approximated from Thatcher (2007), mean for NSW, VIC, QLD, SA
real, dimension(25), parameter :: basecycle = (/ 0.92, 0.86, 0.81, 0.78, 0.8 , & 
                                                 0.87, 0.98, 1.06, 1.08, 1.09, 1.09, &
                                                 1.09, 1.08, 1.08, 1.06, 1.06, 1.08, &
                                                 1.11, 1.08, 1.06, 1.03, 1.00, 0.98, 0.95, 0.92 /)

! ! normalised proportion of heating/cooling appliances in use  approximated from Thatcher (2007)
! real, dimension(25), parameter :: propcycle = (/ 0.43, 0.40, 0.36, 0.33, 0.31 , &
!                                                  0.36, 0.53, 0.66, 0.70, 0.64, 0.60, &
!                                                  0.60, 0.64, 0.69 ,0.75, 0.81, 0.88, &
!                                                  1.00, 0.99, 0.91, 0.81, 0.69, 0.57, 0.43, 0.43 /)

! normalised proportion of heating/cooling appliances in use optimised for Melbourne
real, dimension(25), parameter :: propcycle = (/ 0.34, 0.30, 0.27, 0.25, 0.36 , &
                                                 0.43, 0.69, 0.73, 0.69, 0.65, 0.57, &
                                                 0.55, 0.51, 0.54 ,0.57, 0.66, 0.85, &
                                                 0.93, 1.00, 0.96, 0.93, 0.80, 0.59, 0.41, 0.34 /)

! base temperature translation cycle approximated from Thatcher (2007) (not used)
real, dimension(25), parameter :: trancycle =(/ -1.09, -1.21, -2.12, -2.77, -3.06, &
                                                -2.34, -0.37,  1.03,  1.88,  2.37,  2.44, &
                                                 2.26,  1.93,  1.41,  0.74,  0.16,  0.34, &
                                                 1.48,  1.03,  0.14, -0.74, -1.17, -1.15, -1.34, -1.09 /)

int_p=int(24.*fp_ctime)
real_p=24.*fp_ctime-real(int_p)
where (int_p<1) int_p=int_p+24

icyc_traffic     = ((1.-real_p)*trafcycle(int_p)+real_p*trafcycle(int_p+1))
icyc_basedemand  = ((1.-real_p)*basecycle(int_p)+real_p*basecycle(int_p+1))
icyc_proportion  = ((1.-real_p)*propcycle(int_p)+real_p*propcycle(int_p+1))
icyc_translation = ((1.-real_p)*trancycle(int_p)+real_p*trancycle(int_p+1))

return
end subroutine getdiurnal

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This subroutine calculates the inverse of a NxN matrix
! Subroutine supplied by Marcus Thatcher <marcus.thatcher@csiro.au>
! input/output = a, size=s, error flag=ier
 
subroutine minverse(a_inout,ierr)
 
implicit none
 
real, dimension(:,:,:), intent(inout) :: a_inout
real(kind=dp), dimension(size(a_inout,1),size(a_inout,2),size(a_inout,3)) :: a
real(kind=dp), dimension(size(a_inout,1)) :: det, d, amax
real(kind=dp), dimension(size(a_inout,2)) :: x
real(kind=dp) :: y
integer, intent(out)  :: ierr
integer s, ns, iq, i, j, nu
integer, dimension(size(a_inout,1),size(a_inout,2)) :: row, col
integer, dimension(size(a_inout,1)) :: prow, pcol
logical, dimension(size(a_inout,1),size(a_inout,2)) :: notpiv

a = real(a_inout,8)

nu = size(a_inout,1)
s = size(a_inout,2)

det = 0.
d = 1.
notpiv = .TRUE.
 
do ns = 1,s
 
  amax(:) = 0.
  do j = 1,s
    do i = 1,s
      where ( notpiv(:,j) .and. notpiv(:,i) .and. amax(:)<abs(a(:,i,j)) )
        amax(:) = abs(a(:,i,j))
        prow(:) = i
        pcol(:) = j
      end where  
    end do
  end do
 
  if ( any(amax<0.) ) then
    ierr=1
    return
  end if

  do iq = 1,nu
    notpiv(iq,pcol(iq)) = .FALSE.
    if ( prow(iq)/=pcol(iq) ) then
      d(iq) = -d(iq)
      x(1:s) = a(iq,prow(iq),1:s)
      a(iq,prow(iq),1:s) = a(iq,pcol(iq),1:s)
      a(iq,pcol(iq),1:s) = x(1:s)
    end if
  end do  
 
  row(:,ns) = prow(:)
  col(:,ns) = pcol(:)
  do iq = 1,nu
    amax(iq) = a(iq,pcol(iq),pcol(iq))
  end do  
  d(:) = d(:)*amax(:)
 
  if ( any(abs(d)<=0.) ) then
    ierr=1
    return
  end if
 
  amax(:) = 1./amax(:)
  do iq = 1,nu
    a(iq,pcol(iq),pcol(iq))=1.
    a(iq,pcol(iq),1:s) = a(iq,pcol(iq),1:s)*amax(iq)
  end do  
 
  do i=1,s
    do iq = 1,nu
      if ( i/=pcol(iq) ) then
        y = a(iq,i,pcol(iq))
        a(iq,i,pcol(iq)) = 0.
        do j = 1,s
          a(iq,i,j)=a(iq,i,j)-y*a(iq,pcol(iq),j)
        end do
      end if
    end do  
  end do
 
end do
 
det(:) = d(:)
 
do ns = s,1,-1
  prow(:) = row(:,ns)
  pcol(:) = col(:,ns)
  do iq = 1,nu
    if ( prow(iq)/=pcol(iq) ) then
      do i = 1,s
        y = a(iq,i,prow(iq))
        a(iq,i,prow(iq)) = a(iq,i,pcol(iq))
        a(iq,i,pcol(iq)) = y
      end do
    end if
  end do  
end do

a_inout = real(a)

ierr = 0
 
return
end subroutine minverse

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Conservation of energy check

subroutine energyclosure(sg_roof,rg_roof,fg_roof,sg_walle,rg_walle,fg_walle,     &
                         sg_road,rg_road,fg_road,sg_wallw,rg_wallw,fg_wallw,     &
                         rgint_roof,rgint_walle,rgint_wallw,rgint_slab,          &
                         ggint_roof,ggint_walle,ggint_wallw,                     &
                         ggext_intm,ggint_slab,ggint_intm,d_intgains_bld,        &
                         int_infilflux,d_ac_inside,fp,ddt,fp_intm,               &
                         fp_road,fp_roof,fp_slab,fp_wall,intm,pd,road,roof,room, &
                         slab,walle,wallw,int_airden,ufull)

implicit none

integer, intent(in) :: ufull
real, intent(in) :: ddt
real, dimension(ufull), intent(in) :: sg_roof,rg_roof,fg_roof,sg_walle,rg_walle,fg_walle
real, dimension(ufull), intent(in) :: sg_road,rg_road,fg_road,sg_wallw,rg_wallw,fg_wallw
real, dimension(ufull), intent(in) :: rgint_roof,rgint_walle,rgint_wallw,rgint_slab
real, dimension(ufull), intent(in) :: ggint_roof,ggint_walle,ggint_wallw
real, dimension(ufull), intent(in) :: ggext_intm,ggint_slab,ggint_intm,d_intgains_bld
real, dimension(ufull), intent(in) :: int_infilflux,d_ac_inside,int_airden
real(kind=8), dimension(ufull) :: d_roofflux,d_walleflux,d_wallwflux,d_roadflux,d_slabflux,d_intmflux,d_roomflux 
real(kind=8), dimension(ufull) :: d_roofstor,d_wallestor,d_wallwstor,d_roadstor,d_slabstor,d_intmstor,d_roomstor
real(kind=8), dimension(ufull) :: d_faceterr
real(kind=8), dimension(ufull) :: d_storageflux
real(kind=8), dimension(ufull,nl) :: roadstorage_prev, roofstorage_prev, wallestorage_prev, wallwstorage_prev
real(kind=8), dimension(ufull,nl) :: slabstorage_prev, intmstorage_prev
real(kind=8), dimension(ufull,1) :: roomstorage_prev
type(facetparams), intent(in) :: fp_intm, fp_road, fp_roof, fp_slab, fp_wall
type(facetdata), intent(inout) :: intm
type(facetdata), intent(inout) :: road, roof, room, slab, walle, wallw
type(fparmdata), intent(in) :: fp
type(pdiagdata), intent(inout) :: pd
logical, save :: init = .TRUE.

! Store previous calculation to determine flux
roofstorage_prev(:,:)  = roof%storage(:,:)
roadstorage_prev(:,:)  = road%storage(:,:)
wallestorage_prev(:,:) = walle%storage(:,:)
wallwstorage_prev(:,:) = wallw%storage(:,:)
slabstorage_prev(:,:)  = slab%storage(:,:)
intmstorage_prev(:,:)  = intm%storage(:,:)
roomstorage_prev(:,:)  = room%storage(:,:)
pd%surferr = 0.

! assume a_rho=1 for interior
room%storage(:,1) = real(fp%bldheight(:)*aircp*int_airden*room%nodetemp(:,1),8)
! Sum heat stored in urban materials from layer 1 to nl
select case(conductmeth)
  case(0) ! half-layer conduction
    roof%storage(:,:) = real(fp_roof%depth(:,:),8)*real(fp_roof%volcp(:,:),8)*real(roof%nodetemp(:,1:nl),8)
    road%storage(:,:) = real(fp_road%depth(:,:),8)*real(fp_road%volcp(:,:),8)*real(road%nodetemp(:,1:nl),8)
    walle%storage(:,:)= real(fp_wall%depth(:,:),8)*real(fp_wall%volcp(:,:),8)*real(walle%nodetemp(:,1:nl),8)
    wallw%storage(:,:)= real(fp_wall%depth(:,:),8)*real(fp_wall%volcp(:,:),8)*real(wallw%nodetemp(:,1:nl),8)
    slab%storage(:,:) = real(fp_slab%depth(:,:),8)*real(fp_slab%volcp(:,:),8)*real(slab%nodetemp(:,1:nl),8)
    intm%storage(:,:) = real(fp_intm%depth(:,:),8)*real(fp_intm%volcp(:,:),8)*real(intm%nodetemp(:,1:nl),8)
  case(1) ! interface conduction
    roof%storage(:,:)  = 0.5_8*real(fp_roof%depth(:,:),8)*real(fp_roof%volcp(:,:),8)                        & 
                            *(real(roof%nodetemp(:,0:nl-1),8)+real(roof%nodetemp(:,1:nl),8))
    road%storage(:,:)  = 0.5_8*real(fp_road%depth(:,:),8)*real(fp_road%volcp(:,:),8)                        & 
                            *(real(road%nodetemp(:,0:nl-1),8)+real(road%nodetemp(:,1:nl),8))
    walle%storage(:,:) = 0.5_8*real(fp_wall%depth(:,:),8)*real(fp_wall%volcp(:,:),8)                        & 
                            *(real(walle%nodetemp(:,0:nl-1),8)+real(walle%nodetemp(:,1:nl),8))
    wallw%storage(:,:) = 0.5_8*real(fp_wall%depth(:,:),8)*real(fp_wall%volcp(:,:),8)                        & 
                            *(real(wallw%nodetemp(:,0:nl-1),8)+real(wallw%nodetemp(:,1:nl),8))
    slab%storage(:,:)  = 0.5_8*real(fp_slab%depth(:,:),8)*real(fp_slab%volcp(:,:),8)                        & 
                            *(real(slab%nodetemp(:,0:nl-1),8)+real(slab%nodetemp(:,1:nl),8))
    intm%storage(:,:)  = 0.5_8*real(fp_intm%depth(:,:),8)*real(fp_intm%volcp(:,:),8)                        & 
                            *(real(intm%nodetemp(:,0:nl-1),8)+real(intm%nodetemp(:,1:nl),8))
end select

! first call do not calculate comparison with last timestep (return)
if (init) then
  init = .FALSE.
  return
end if
! if ( all(roofstorage_prev<1.e-20_8) ) return
  
d_roofstor = sum(roof%storage-roofstorage_prev,dim=2)/real(ddt,8)
d_roofflux = real(sg_roof,8)+real(rg_roof,8)-real(fg_roof,8) - real(ggint_roof,8) - real(rgint_roof,8)
d_faceterr  = d_roofstor - d_roofflux
pd%surferr = pd%surferr + d_faceterr
if (any(abs(d_faceterr)>=energytol)) write(6,*) "aTEB roof facet closure error:", maxval(abs(d_faceterr))
d_roadstor = sum(road%storage-roadstorage_prev,dim=2)/real(ddt,8)
d_roadflux = real(sg_road,8)+real(rg_road,8)-real(fg_road,8)
d_faceterr  = d_roadstor - d_roadflux
pd%surferr = pd%surferr + d_faceterr
if (any(abs(d_faceterr)>=energytol)) write(6,*) "aTEB road facet closure error:", maxval(abs(d_faceterr))
d_wallestor= sum(walle%storage-wallestorage_prev,dim=2)/real(ddt,8)
d_walleflux= real(sg_walle,8)+real(rg_walle,8)-real(fg_walle,8) - real(ggint_walle,8) - real(rgint_walle,8)
d_faceterr = d_wallestor - d_walleflux
pd%surferr = pd%surferr + d_faceterr
if (any(abs(d_faceterr)>=energytol)) write(6,*) "aTEB walle facet closure error:", maxval(abs(d_faceterr))
d_wallwstor= sum(wallw%storage-wallwstorage_prev,dim=2)/real(ddt,8)
d_wallwflux= real(sg_wallw,8)+real(rg_wallw,8)-real(fg_wallw,8) - real(ggint_wallw,8) - real(rgint_wallw,8)
d_faceterr = d_wallwstor - d_wallwflux
pd%surferr = pd%surferr + d_faceterr
if (any(abs(d_faceterr)>=energytol)) write(6,*) "aTEB wallw facet closure error:", maxval(abs(d_faceterr))
if (intairtmeth==1) then
  d_slabstor = sum(slab%storage-slabstorage_prev,dim=2)/real(ddt,8)
  d_slabflux = -real(ggint_slab,8) - real(rgint_slab,8)
  d_faceterr = d_slabstor - d_slabflux
  pd%surferr = pd%surferr + d_faceterr
  if (any(abs(d_faceterr)>=energytol)) write(6,*) "aTEB slab facet closure error:", maxval(abs(d_faceterr))
  d_intmstor = sum(intm%storage-intmstorage_prev,dim=2)/real(ddt,8)
  d_intmflux = -real(ggext_intm,8) - real(ggint_intm,8)
  d_faceterr = d_intmstor - d_intmflux
  pd%surferr = pd%surferr + d_faceterr
  if (any(abs(d_faceterr)>=energytol)) write(6,*) "aTEB intm facet closure error:", maxval(abs(d_faceterr))
  d_roomstor = (room%storage(:,1)-roomstorage_prev(:,1))/real(ddt,8)
  d_roomflux = real(ggint_roof,8)+real(ggint_slab,8)-real(fp%intmassn,8)*d_intmflux                    & 
            + (real(fp%bldheight,8)/real(fp%bldwidth,8))*(real(ggint_walle,8) + real(ggint_wallw,8))   &
            + real(int_infilflux,8) + real(d_ac_inside,8) + real(d_intgains_bld,8)
  d_faceterr = d_roomstor - d_roomflux
  pd%surferr = pd%surferr + d_faceterr
  if (any(abs(d_faceterr)>=energytol)) write(6,*) "aTEB room volume closure error:", maxval(abs(d_faceterr))
else
  d_slabstor = 0._8
  d_intmstor = 0._8
  d_roomstor = 0._8
end if

d_storageflux = d_roofstor*real(fp%sigmabld,8)                                     &
              + d_roadstor*(1._8-real(fp%sigmabld,8))                              &
              + d_wallestor*(1._8-real(fp%sigmabld,8))*real(fp%hwratio,8)          &
              + d_wallwstor*(1._8-real(fp%sigmabld,8))*real(fp%hwratio,8)          &
              + d_slabstor*real(fp%sigmabld,8)                                     &
              + d_intmstor*real(fp%sigmabld,8)*real(fp%intmassn,8)                 &
              + d_roomstor*real(fp%sigmabld,8)

! write (6,*) '======================================================'
! write (6,*) 'd_storageflux',d_storageflux
! write (6,*) 'roof  Qs' ,real(d_roofstor,8)*real(fp%sigmabld,8)     
! write (6,*) 'road  Qs' ,real(d_roadstor,8)*(1-real(fp%sigmabld,8))
! write (6,*) 'walle Qs' ,real(d_wallestor,8)*(1-real(fp%sigmabld,8))*real(fp%hwratio,8)       
! write (6,*) 'wallw Qs' ,real(d_wallwstor,8)*(1-real(fp%sigmabld,8))*real(fp%hwratio,8)       
! write (6,*) 'slab  Qs' ,real(d_slabstor,8)*real(fp%sigmabld,8)                              
! write (6,*) 'intm  Qs' ,real(d_intmstor,8)*real(fp%sigmabld,8)*real(fp%intmassn,8)           
! write (6,*) 'room  Qs' ,real(d_roomstor,8)*real(fp%sigmabld,8)
! write (6,*) 'infil', real(int_infilflux,8)*real(fp%sigmabld,8)
! write (6,*) 'ac_inside', real(d_ac_inside,8)*real(fp%sigmabld,8)
! write (6,*) 'intgains', real(d_intgains_bld,8)*real(fp%sigmabld,8)

! write (6,*) 'bldheight,bldwidth :',fp%bldheight,fp%bldwidth
! write (6,*) 'h/w',fp%hwratio
! write (6,*) 'infil,ac_inside,intgains',int_infilflux,d_ac_inside,d_intgains_bld
! write (6,*) 'storage flux: roof,walle,wallw ',d_roofstor,d_wallestor,d_wallwstor
! write (6,*) 'storage flux: slab,intm,room',d_slabstor,d_intmstor,d_roomstor
! write (6,*) 'skin temp:    roof,walle,wallw ',roof%nodetemp(:,nl),walle%nodetemp(:,nl),wallw%nodetemp(:,nl)
! write (6,*) 'skin temp:    slab,intm,room',slab%nodetemp(:,nl),intm%nodetemp(:,nl),room%nodetemp(:,1)
! write (6,*) 'ggint: roof, walle, wallw, slab', ggint_roof,ggint_walle,ggint_wallw,ggint_slab
! write (6,*) 'ggint: slab, intm', ggint_slab,d_intmflux
! write (6,*) 'room store, prev:', room%storage(:,1)/real(ddt,8), roomstorage_prev(:,1)/real(ddt,8)
! write (6,*) 'roomflux', ggint_roof+ggint_slab+(fp%bldheight/fp%bldwidth)*(ggint_walle+ggint_wallw)
! write (6,*), 'rgint: roof, walle, wallw, slab', rgint_roof,rgint_walle,rgint_wallw,rgint_slab
! write (6,*) '======================================================'

! print *, 'energyclosure:',d_roofstor


! fluxes out
varout(:,1) = real(ggint_roof + ggint_slab + (fp%bldheight/fp%bldwidth)*(ggint_walle + ggint_wallw))
varout(:,2) = real(d_ac_inside)
varout(:,3) = real(d_intgains_bld)
varout(:,4) = real(int_infilflux)
! varout(:,5) = real(d_roofstor+d_slabstor+d_intmstor + fp%bldheight/fp%bldwidth*(d_wallestor+d_wallwstor))
varout(:,5) = rgint_slab
varout(:,6) = room%nodetemp(:,1) + urbtemp - 273.16

! ! temperatures out
! varout(:,1) = walle%nodetemp(:,nl) + urbtemp - 273.16
! varout(:,2) = wallw%nodetemp(:,nl) + urbtemp - 273.16
! varout(:,3) = slab%nodetemp(:,nl) + urbtemp - 273.16
! varout(:,4) = roof%nodetemp(:,nl) + urbtemp - 273.16
! varout(:,5) = 0
! varout(:,6) = room%nodetemp(:,1) + urbtemp - 273.16

! ! ! Slab fluxes
! ! ggint fluxes
! varout(:,1) = -ggint_slab
! varout(:,2) = ggext_slab
! varout(:,3) = rgint_slab
! varout(:,4) = slab%nodetemp(:,nl) + urbtemp - 273.16
! varout(:,5) = 0.
! varout(:,6) = room%nodetemp(:,1) + urbtemp - 273.16

return
end subroutine energyclosure

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This subroutine deallocates arrays used by the scheme

subroutine finish(diag)

implicit none

integer, intent(in) :: diag
integer tile

if (diag>=1) write(6,*) "Deallocating aTEB arrays"

do tile = 1,ntiles
  
  deallocate(f_roof(tile)%depth,f_wall(tile)%depth,f_road(tile)%depth,f_slab(tile)%depth,f_intm(tile)%depth)
  deallocate(f_roof(tile)%volcp,f_wall(tile)%volcp,f_road(tile)%volcp,f_slab(tile)%volcp,f_intm(tile)%volcp)
  deallocate(f_roof(tile)%lambda,f_wall(tile)%lambda,f_road(tile)%lambda,f_slab(tile)%lambda,f_intm(tile)%lambda)
  deallocate(f_roof(tile)%alpha,f_wall(tile)%alpha,f_road(tile)%alpha)
  deallocate(f_roof(tile)%emiss,f_wall(tile)%emiss,f_road(tile)%emiss)
  deallocate(f_slab(tile)%emiss)
  deallocate(roof_g(tile)%nodetemp,road_g(tile)%nodetemp,walle_g(tile)%nodetemp,wallw_g(tile)%nodetemp)
  deallocate(slab_g(tile)%nodetemp,intm_g(tile)%nodetemp,room_g(tile)%nodetemp)
  deallocate(road_g(tile)%storage,roof_g(tile)%storage,walle_g(tile)%storage,wallw_g(tile)%storage)
  deallocate(slab_g(tile)%storage,intm_g(tile)%storage,room_g(tile)%storage)
  deallocate(intl_g(tile)%viewf,intl_g(tile)%psi)
  deallocate(f_g(tile)%sigmabld,f_g(tile)%hwratio,f_g(tile)%bldheight,f_g(tile)%coeffbldheight)
  deallocate(f_g(tile)%effhwratio)
  deallocate(f_g(tile)%industryfg,f_g(tile)%intgains_flr,f_g(tile)%trafficfg)
  deallocate(f_g(tile)%ctime)
  deallocate(f_g(tile)%intmassn,f_g(tile)%infilach,f_g(tile)%ventilach,f_g(tile)%tempheat,f_g(tile)%tempcool)
  deallocate(p_g(tile)%surferr,p_g(tile)%atmoserr,p_g(tile)%surferr_bias,p_g(tile)%atmoserr_bias)
  deallocate(p_g(tile)%bldheat,p_g(tile)%bldcool,p_g(tile)%traf,p_g(tile)%intgains_full)

end do

deallocate( roof_g, road_g, walle_g, wallw_g, slab_g, intm_g )
deallocate( room_g )
deallocate( f_roof, f_road, f_wall, f_slab, f_intm )
deallocate( intl_g )
deallocate( f_g )
deallocate( p_g )
deallocate( ufull_g )
deallocate( upack_g )

deallocate( varout )
    
return
end subroutine finish

end module uclem

