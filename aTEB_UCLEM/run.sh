#!/bin/bash
# This is an extended wrapper for atebwrap for automatically archiving experiments files
# First it asks for an experiment name
# It then runs aTEB with the current parameter namelist ateb.nml
# It then archives the namelist (ateb.nml) into nml folder 
# Then copies current git log and diff information to output file
# Then archives output file (atebout.dat) into out folder
# Then archives energy file (energyout.dat) into energy folder
# Then archives variables (variables.out) into variables folder
# REQUIRED FOLDERS:
# 	out: records surface fluxes (ateb output)
# 	energy: stores building energy use and temperature information (UCLEM output)
# 	variables: records screen output
#	nml: records namelist used
# 	src: records copy of ateb.f90 used

echo Enter experiment designation:
read EXP

./atebwrap >variables.out
echo "Now copying ateb.nml to nml/$EXP.nml"
cp ateb.nml nml/$EXP.nml


echo "Now adding atebout.dat to out/$EXP.dat"
# add git log info to .dat file
if git rev-parse --git-dir > /dev/null 2>&1; then
	echo '-----------------git build version-------------------' > out/$EXP.dat
	git log --date=short --max-count=1 >> out/$EXP.dat

	# add diff information, piped through grep to only include + or - lines
	echo '    with the following DIFF to ateb.f90:' >> out/$EXP.dat
	echo '' >> out/$EXP.dat
	git diff --ignore-space-at-eol ateb.f90 | grep '^+\|^-' >> out/$EXP.dat	
	echo '<=------------------------------------------------=>' >> out/$EXP.dat
        # insert (i) comment character (#) start of each line (^) until pattern 'Year' reached
        sed -i '' '1,/<=--/ s/^/# /' out/$EXP.dat
        sed -i '' '/^     /d' out/$EXP.dat
	cat atebout.dat >> out/$EXP.dat
else 
	cat atebout.dat > out/$EXP.dat	
fi 

sed -i '' '/^     /d' out/$EXP.dat
grep "Year" out/$EXP.dat
tail -n4 out/$EXP.dat

echo "Now copying energyout.dat to energy/$EXP.dat"
mv energyout.dat energy/$EXP.dat
head -n2 energy/$EXP.dat
tail -n4 energy/$EXP.dat

echo "Now copying variables.out to variables/$EXP.var"
mv variables.out variables/$EXP.var
tail -n3 variables/$EXP.var

echo "Now copying ateb.f90 to src/$EXP.f90"
cp ateb.f90 src/$EXP.f90

echo "Analysing output vs observation"
python analysis/internal.py $EXP

echo "Done!"
exit
