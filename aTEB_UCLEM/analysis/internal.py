import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sys
import os

oshome=os.getenv('HOME')
pd.set_option('display.width', 150)

atebpath = '.'

expname = sys.argv[1]
# expname = 'V74_cyc_r2'

def import_sim(expname):
    ''' function to loop through all experiments in fulllist
    returns:
        finalsim: predicted fluxes in dataframe
        finalsimQF: as above, for waste heat fluxes '''
    
    # read experiment flux data
    sim=pd.read_csv('%s/out/%s.dat' %(atebpath,expname), na_values=['nan'], delim_whitespace=True, comment='#',engine='c')
    simQF=pd.read_csv('%s/energy/%s.dat' %(atebpath,expname), delim_whitespace=True, skiprows=[0], header=None, 
        usecols=[8,9,10,11,12,13], names=['Heating','Cooling','IntGains','Traffic','IntAirTemp','ScrnTemp'],engine='c')
    sim.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')
    sim.Time=sim.index.hour + sim.index.minute/60
    
    assert(sim.columns[0] == 'Year')
    # place all NaN in obs within corresponding simulated data
    sim = sim[~obs.isnull()] 
    simQF.index = sim.index

    simQF['ElecGas']=simQF['Heating']+simQF['Cooling']+simQF['IntGains']
    simQF['NetQF']=simQF['ElecGas']+simQF['Traffic']

    sim['NetQF'] = simQF['NetQF']
    sim['ElecGas'] = simQF['ElecGas']
    sim['dQS']   = sim['NetRad']+sim['NetQF']-sim['QHup']-sim['QEup']

    return sim, simQF

def RMSE(in1,in2):
    '''Function to calculate Root Mean Square Error (RMSE) for two input arrays ie (sqrt(N^-1(sum(in1 - in2)^2)).'''
    return np.sqrt(np.mean((in1-in2)**2, axis=0))

def MAE(in1,in2):
    ''' Mean Absolute Error (MAE) as a %.
    Default inputs (P-O), returns mean.|P-O|'''
    return np.mean(abs(in1-in2))

def MBE(in1,in2):
    '''This function calculates Mean Bias Error  (N^-1(Sum(input1-input2)).
        Default inputs (P - O)'''
    return np.mean(in1-in2, axis=0)

def r2(in1,in2):
    '''Calculates coefficient of determination (r^2) from stats.linregress()'''
    r = in1.corrwith(in2)
    return r**2
def NSD(in1,in2):
    ''' Normalised Standard Deviation (NSD) as a %.
    Default inputs SD(prediction)/SD(observation)'''
    # return np.nanstd(in1,axis=0)/np.nanstd(in2,axis=0)
    return in1.std(ddof=0)/in2.std(ddof=0)

def create_stats():
    ''' Create all year statistics dataframe '''
    fluxes = ['SWup','LWup','QHup','QEup','ElecGas','dQS']
    stats_year = pd.DataFrame([
        RMSE(sim[fluxes],obs[fluxes]),
        MBE(sim[fluxes],obs[fluxes]),
        MAE(sim[fluxes],obs[fluxes]),
        r2(sim[fluxes],obs[fluxes]),
        NSD(sim[fluxes],obs[fluxes])],
        index=['RMSE', 'MBE', 'MAE', 'r2','nSD'])
    return stats_year

obs = pd.read_csv('%s/analysis/preston_obs.csv' %(atebpath),index_col=0)
# old obsQF
# obsQF = pd.read_csv('%s/analysis/preston_obsQF_old.csv' %(atebpath),index_col=0)
obsQF = pd.read_csv('%s/analysis/preston_obsQF.csv' %(atebpath),index_col=0,header=None,skiprows=63340,nrows=22772,
                    names=['Traffic',' Metabolism','Gas','Electricity','ElecGas','NetQF','ExtAir'])

sim,simQF = import_sim(expname)
obs.index = sim.index
obsQF.index = simQF.index

###### DEFINE SPECIAL PERIODS ########

obsQF['weekend'] = obsQF.loc[obsQF.index.dayofweek >= 5, ['ElecGas']]
obsQF['week'] = obsQF.loc[obsQF.index.dayofweek < 5, ['ElecGas']]

obsQF['hot']  = obsQF.loc[obsQF.ExtAir > 25, ['ElecGas']]
obsQF['cold'] = obsQF.loc[obsQF.ExtAir < 15, ['ElecGas']]

simQF['hot']  = simQF.loc[obsQF.ExtAir > 25, ['ElecGas']]
simQF['cold'] = simQF.loc[obsQF.ExtAir < 15, ['ElecGas']]

######################################

stats_year = create_stats()

print(stats_year)

###### plot summer elec use and temps ########
plt.close('all')

start, end = '2003-11-18','2003-12-17'

fig,ax = plt.subplots(1,figsize=(8,3))

ax.set_title('Electricity and gas demand (warm period)',fontsize=10)
obsQF.loc[start:end,'week'].plot(ax=ax,color='black',lw=1.0,label='Obs. demand (weekday)')
obsQF.loc[start:end,'weekend'].plot(ax=ax,color='black',lw=1.0,label='Obs. demand (sat/sun)',alpha=1.0, ls='dashed')
simQF.loc[start:end,'ElecGas'].plot(ax=ax,color='red',lw=1.0,alpha=0.9,label='Simulated demand')
ax.set_ylabel(r'Demand ($W/m^2$)')
ax.set_ylim([0,17])
# ax.set_xlabel(expname,fontsize=10,labelpad=1)

ax2 = ax.twinx()
obsQF.loc[start:end,'ExtAir'].plot(ax=ax2,color='blue',lw=0.5,ls='solid',label='40m air temp (obs)',zorder=-20,alpha=0.6)
(simQF.loc[start:end,'ScrnTemp']-273.16).plot(ax=ax2,color='lightblue',lw=0.5,ls='solid',label='screen level temp (sim)')
(simQF.loc[start:end,'IntAirTemp']-273.16).plot(ax=ax2,color='orange',lw=0.5,ls='solid',label='internal air temp (sim)')
ax2.set_ylabel(r'Air temperature (°C)')
ax2.set_ylim([0,41])

ax.legend(fontsize='x-small',loc='upper left')
ax2.legend(fontsize='x-small',loc='upper right')

# fix month label
labels = [item.get_text() for item in ax.get_xticklabels()]
labels[1] = 'Dec'
ax.set_xticklabels(labels)
# limits
ax.set_xlim([pd.Timestamp(start),pd.Timestamp(end)])

# set ax2 behind ax
ax.set_zorder(10)
ax.patch.set_visible(False)

fig.savefig('%s/analysis/Dec_%s.pdf' %(atebpath,expname), dpi=200,bbox_inches='tight',pad_inches=0.05)

###### plot summer elec use and temps ########

plt.close('all')

start, end = '2004-01-31','2004-02-28'

fig,ax = plt.subplots(1,figsize=(8,3))

ax.set_title('Electricity and gas demand (warm period)',fontsize=10)
obsQF.loc[start:end,'week'].plot(ax=ax,color='black',lw=1.0,label='Obs. demand (weekday)')
obsQF.loc[start:end,'weekend'].plot(ax=ax,color='black',lw=1.0,label='Obs. demand (sat/sun)',alpha=1.0, ls='dashed')
simQF.loc[start:end,'ElecGas'].plot(ax=ax,color='red',lw=1.0,alpha=0.9,label='Simulated demand')
ax.set_ylabel(r'Demand ($W/m^2$)')
ax.set_ylim([0,17])
# ax.set_xlabel(expname,fontsize=10,labelpad=1)

ax2 = ax.twinx()
obsQF.loc[start:end,'ExtAir'].plot(ax=ax2,color='blue',lw=0.5,ls='solid',label='40m air temp (obs)',zorder=-20,alpha=0.6)
(simQF.loc[start:end,'ScrnTemp']-273.16).plot(ax=ax2,color='lightblue',lw=0.5,ls='solid',label='screen level temp (sim)')
(simQF.loc[start:end,'IntAirTemp']-273.16).plot(ax=ax2,color='orange',lw=0.5,ls='solid',label='internal air temp (sim)')
ax2.set_ylabel(r'Air temperature (°C)')
ax2.set_ylim([0,41])

ax.legend(fontsize='x-small',loc='upper left')
ax2.legend(fontsize='x-small',loc='upper right')

# fix month label
labels = [item.get_text() for item in ax.get_xticklabels()]
labels[1] = 'Feb'
ax.set_xticklabels(labels)
# limits
ax.set_xlim([pd.Timestamp(start),pd.Timestamp(end)])

# set ax2 behind ax
ax.set_zorder(10)
ax.patch.set_visible(False)

fig.savefig('%s/analysis/Feb_%s.pdf' %(atebpath,expname), dpi=200,bbox_inches='tight',pad_inches=0.05)


# ###### plot winter elec use and temps ########

plt.close('all')

start, end = '2004-06-30','2004-07-31'

fig,ax = plt.subplots(1,figsize=(8,3))

ax.set_title('Electricity and gas demand (cold period)',fontsize=10)
obsQF.loc[start:end,'week'].plot(ax=ax,color='black',lw=1.0,label='Obs. demand (weekday)')
obsQF.loc[start:end,'weekend'].plot(ax=ax,color='black',lw=1.0,label='Obs. demand (sat/sun)',alpha=1.0, ls='dashed')
simQF.loc[start:end,'ElecGas'].plot(ax=ax,color='red',lw=1.0,alpha=0.9,label='Simulated demand')
ax.set_ylabel(r'Demand ($W/m^2$)')
ax.set_ylim([0,17])
# ax.set_xlabel(expname,fontsize=10,labelpad=1)

ax2 = ax.twinx()
obsQF.loc[start:end,'ExtAir'].plot(ax=ax2,color='blue',lw=0.5,ls='solid',label='40m air temp (obs)',zorder=-20,alpha=0.6)
(simQF.loc[start:end,'ScrnTemp']-273.16).plot(ax=ax2,color='lightblue',lw=0.5,ls='solid',label='screen level temp (sim)')
(simQF.loc[start:end,'IntAirTemp']-273.16).plot(ax=ax2,color='orange',lw=0.5,ls='solid',label='internal air temp (sim)')
ax2.set_ylabel(r'Air temperature (°C)')
ax2.set_ylim([0,41])

ax.legend(fontsize='x-small',loc='upper left')
ax2.legend(fontsize='x-small',loc='upper right')

ax.set_zorder(ax2.get_zorder()+1) # put ax in front of ax2 
ax.patch.set_visible(False)

# fix month label
labels = [item.get_text() for item in ax.get_xticklabels()]
labels[1] = 'Jul'
ax.set_xticklabels(labels)
# limits
ax.set_xlim([pd.Timestamp(start),pd.Timestamp(end)])

# set ax2 behind ax
ax.set_zorder(10)
ax.patch.set_visible(False)

fig.savefig('%s/analysis/Jul_%s.pdf' %(atebpath,expname), dpi=200,bbox_inches='tight',pad_inches=0.05)

# ########################################################## 

def perkins(a,b,bins=100):

    amax,amin = a.max(),a.min()
    bmax,bmin = b.max(),b.min()

    # histogram with min/max extends from both data
    acount, aedge = np.histogram(a,bins=bins,range=(min(amin,bmin),max(amax,bmax)))
    aprob = acount/acount.sum()

    bcount, bedge = np.histogram(b,bins=bins,range=(min(amin,bmin),max(amax,bmax)))
    bprob = bcount/bcount.sum()

    skill = np.minimum(aprob,bprob).sum()

    return skill

def plot_qq(obs,data,ax,colour='red'):
    x = np.arange(0,1.01,0.01)
    obs_q  = obs.quantile(x)
    data_q = data.quantile(x)

    # qq
    ax.scatter(obs_q,data_q, c=colour, s=4, linewidths=0.5, zorder=10)

    ax.scatter([obs_q[0.1],obs_q[0.5],obs_q[0.9]],[data_q[0.1],data_q[0.5],data_q[0.9]], color='black', s=4, linewidths=0.5, zorder=10)

    ax.text(obs_q[0.1],data_q[0.1],'10% ',fontsize=6, ha='right',va='bottom')
    ax.text(obs_q[0.5],data_q[0.5],'50% ',fontsize=6, ha='right',va='bottom')
    ax.text(obs_q[0.9],data_q[0.9],'90% ',fontsize=6, ha='right',va='bottom')

    # identity line
    imin = min(obs_q.min(),data_q.min())
    imax = max(obs_q.max(),data_q.max())
    # extend range by 1%
    imin = imin-0.01*(abs(imax)-abs(imin))
    imax = imax+0.01*(abs(imax)-abs(imin))
    ax.plot([imin,imax],[imin,imax], color='k', lw=0.5, zorder=5)

    # ax.axis('tight')
    # # set yticks=xticks

    xticks=ax.get_xticks()
    ax.set_yticks(xticks)

    ax.grid(lw=0.3,color='0.8',zorder=-100)

    ax.set_xlim([imin,imax])
    ax.set_ylim([imin,imax])

    return ax

bins=50
colours = ['brown','red','orange','green','blue']

########### Distribution Plot ##############

for flux in ['hot','cold']:

    fig, ax = plt.subplots(figsize=(7,3))
    
    o = obsQF[flux]
    s = simQF[flux]

    skill = perkins(o.dropna(),s.dropna(),bins)
    ax.text(0.02,0.94,'Perkins Skill: %s' %skill.round(3),transform=ax.transAxes,fontsize=8,ha='left')

    gmin =  min(o.min(),s.min())
    gmax = max(o.max(),s.max())
    evalpoints=np.linspace(gmin,gmax,bins*2)


    o.plot(kind='kde',ind=evalpoints,bw_method=0.1,ax=ax,color='k',label='Observations',lw=2.0)
    o.plot(kind='hist',density=True,bins=bins,color='black',alpha=0.2, range=(gmin,gmax) )

    s.plot(kind='kde',ind=evalpoints,bw_method=0.1,ax=ax,color='red',label='Simulation')
    s.plot(kind='hist',density=True,bins=bins,color='red',alpha=0.4, range=(gmin,gmax) )

    # plt.legend()
    # ax.set_ylim(0,1)

    fig.savefig('%s/analysis/kde_%s_%s.png' %(atebpath,expname,flux), dpi=300,bbox_inches='tight',pad_inches=0.05)
    print('elec skill: %s' %skill)
    plt.close('all')

    ########### QQ Plot ##############


    print('plotting QQ: %s' %(flux))

    fig, ax = plt.subplots(figsize=(3.3,3.25)) 
    ax.set_title('%s QQ (2003-2004)' %(flux.capitalize()))
    ax.text(0.02,0.94,'%s' %expname,transform=ax.transAxes,fontsize=8,ha='left',color='k')

    plot_qq(obsQF[flux],simQF[flux],ax)

    ax.set_xlabel('Observation (derived) ($W m^{-2}$)', fontsize=8)
    ax.set_ylabel('Simulation ($W m^{-2}$)', fontsize=8)
    # ax.axis('equal')
    # ax.set_ylim((0,15))
    # ax.set_xlim((0,15))

    fig.savefig('%s/analysis/QQ_%s_%s.png' %(atebpath,flux,expname), dpi=300,bbox_inches='tight',pad_inches=0.05)


print('nMAE ElecGas: %.1f%%' %(100*(stats_year.loc['MAE','ElecGas']/obsQF.ElecGas.mean())) )

# obsQF.to_csv('obsQF_%s.csv' %expname)
# simQF.to_csv('simQF_%s.csv' %expname)

# ########################################################## 

# colours = ['brown','red','orange','green','blue']

# fluxes = ['NetRad','SWup','LWup','QHup','QEup','dQS','NetQF']
# fluxes = ['NetRad','QHup','NetQF']

# fluxes = ['NetQF']

# for flux in fluxes:

#     ind=np.linspace(obs[flux].min(),obs[flux].max()+1,bins)

#     fig, ax = plt.subplots(figsize=(7,3))

#     obs[flux].plot(kind='kde',ind=ind,bw_method=0.1,ax=ax,color='k',label='Observations',lw=2.0)
#     obs[flux].plot(kind='hist',density=True,bins=bins,color='black',alpha=0.2,
#         range=(gmin,gmax) )

#     # for i,expname in enumerate(shortexplist):

#     sim[flux].plot(kind='kde',ind=ind,bw_method=0.1,ax=ax,color='red',label='Simulation')
#     sim[flux].plot(kind='hist',density=True,bins=bins,color='red',alpha=0.4,
#         range=(gmin,gmax) )

#     assert obs[flux].dropna().shape == sim[flux].dropna().shape

#     skill = perkins(obs[flux].dropna(),sim[flux].dropna(),bins)
#     ax.text(0.02,0.94,'Perkins Skill: %s' %skill.round(3),transform=ax.transAxes,fontsize=8,ha='left')
#     # plt.legend()
#     ax.set_title('%s PDF estimate by KDE' %flux)
#     # ax.set_ylim(0.,0.2)
#     fig.savefig('%s/analysis/%s_kde_%s.png' %(atebpath,expname,flux), dpi=300,bbox_inches='tight',pad_inches=0.05)

# plt.close('all')






