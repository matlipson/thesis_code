program atebwrap

use ateb
use zenith_m

implicit none

integer i,ierr,iyr,iday,itime,diag
integer iyr_f,iday_f,itime_f
integer ncount,ndt_out
real dt_in, dt_out
real dectime,swdown,lwdown,windv,windu,psurf,tair,qair,rainf
real dectime_f,swdown_f,lwdown_f,windv_f,windu_f,psurf_f,tair_f,qair_f,rainf_f
real swdown_d,lwdown_d,windv_d,windu_d,psurf_d,tair_d,qair_d,rainf_d
real dt,vmodmin,bpyear
real rnet,swup,lwup,fgup,egup,fjd,slag,dlt,dhr,r1,alp
real rnet_avg,swup_avg,lwup_avg,fgup_avg,egup_avg,swdown_avg,lwdown_avg
real dayload
real :: start_time, stop_time
real, dimension(1) :: atmoserr,atmoserr_bias,surferr,surferr_bias,bldheat,bldcool,traf,intgain
real, dimension(1) :: bldtemp,scrntemp
real, dimension(1) :: bldtemp_avg,bldheat_avg,bldcool_avg,intgain_avg,traf_avg,scrntemp_avg
real, dimension(1) :: sigmu,fg,eg,tss,wetfac,ps,alb,storage,storage_avg
real, dimension(1) :: newrunoff,t,qg,sg,azmin,rg,rnd,snd
real, dimension(1) :: rho,uzon,vmer,coszro2,rlongg,rlatt,taudar2
character(len=80) dum

real, parameter :: sbconst=5.67e-8    ! Stefan-Boltzmann constant

dt=30.*60.                      ! timestep calculation (sec)
dt_in=30.*60.                   ! timestep input data (sec)
dt_out=30.*60.                  ! timestep output data (sec)  

diag=0                          ! diag = diagnostic message mode (0=off, 1=basic, 2=detailed, etc)
sigmu=1.                        ! fraction of urban
azmin=40.                       ! height to met data (assume 2m)
vmodmin=0.1                     ! min wind speed
rlongg(1)=144.97*3.1415927/180. ! lon
rlatt(1)=-37.81*3.1415927/180.  ! lat
dhr = dt/3600.                  ! timestep in hours (for zenith_m)
bpyear=0.                       ! zenith_m mode 0=present day
dayload=1.                      ! elec/gas day of week usage loading (default 1.0)
ndt_out = int(dt_out/dt)        ! number of timesteps in each output step

call cpu_time(start_time)
! initialise urban model
call atebinit(1,sigmu,diag)

! open files for input and output
open(1,file="alpha04.dat")
open(2,file="atebout.dat")
open(4,file="energyout.dat")

write(6,'(A15,F6.0,A4)') 'timestep (dt):', dt, " [s]"
write(6,'(A15,F6.0,A4)') 'input dt:', dt_in, " [s]"
write(6,'(A15,F6.0,A4)') 'output dt:', dt_out, " [s]"

do i=1,7
  read(1,*) dum
end do
write(2,'(A98)') "Year  Dectime   Day Time   NetRad    SWup      LWup      QHup      QEup     SWdown LWdown     dQS"
! write(2,*)       "                             W/m2      W/m2      W/m2      W/m2      W/m2"

write(4,'(A113)') "Year  Dectime  Day  Time    Atmos  Surf.     Atmos   Surf.   Heat    Cool    Base    Traffic  & 
                    &BldAirTemp ScrnTemp"
! write(6,'(A113)') "Year  Dectime  Day  Time    Atmos  Surf.     Atmos   Surf.   Heat    Cool    Base    Traffic  & 
!                     &BldAirTemp ScrnTemp"

ierr=0
ncount=0

iyr = 0
iday = 0
itime = 0 

dectime = 0.
swdown  = 0.
lwdown  = 0.
windv   = 0.
windu   = 0.
psurf   = 0.
tair    = 0.
qair    = 0.
rainf   = 0.

rnet_avg = 0. 
swup_avg = 0. 
lwup_avg = 0. 
fgup_avg = 0. 
egup_avg = 0. 
swdown_avg = 0. 
lwdown_avg = 0. 
storage_avg = 0. 

bldtemp_avg  = 0.
bldheat_avg  = 0. 
bldcool_avg  = 0.
intgain_avg  = 0. 
traf_avg     = 0. 
scrntemp_avg = 0. 

read(1,*,iostat=ierr) iyr_f,dectime_f,iday_f,itime_f,swdown_f,lwdown_f, &
                      windv_f,windu_f,psurf_f,tair_f,qair_f,rainf_f

do
  ! if calc timestep matches intput timestep
  if ( mod( dt*ncount,dt_in ) == 0 ) then
    ! print *, 'input timestep'

    iyr = iyr_f
    iday = iday_f
    itime = itime_f

    dectime = dectime_f
    swdown  = swdown_f
    lwdown  = lwdown_f
    windv   = windv_f
    windu   = windu_f
    psurf   = psurf_f
    tair    = tair_f
    qair    = qair_f
    rainf   = rainf_f

    read(1,*,iostat=ierr) iyr_f,dectime_f,iday_f,itime_f,swdown_f,lwdown_f, &
                          windv_f,windu_f,psurf_f,tair_f,qair_f,rainf_f

    swdown_d  = swdown_f - swdown 
    lwdown_d  = lwdown_f - lwdown 
    windv_d   = windv_f - windv 
    windu_d   = windu_f - windu 
    psurf_d   = psurf_f - psurf 
    tair_d    = tair_f - tair 
    qair_d    = qair_f - qair 
    rainf_d   = rainf_f - rainf 

  else ! if between input timestep, linearly interpolate
    ! print *, 'between timestep'

    itime = itime + int(dt/60.)
    dectime = dectime + dt/(24.*60.*60)

    swdown  = swdown + swdown_d/(dt_in/dt) 
    lwdown  = lwdown + lwdown_d/(dt_in/dt) 
    windv   = windv + windv_d/(dt_in/dt) 
    windu   = windu + windu_d/(dt_in/dt) 
    psurf   = psurf + psurf_d/(dt_in/dt) 
    tair    = tair + tair_d/(dt_in/dt) 
    qair    = qair + qair_d/(dt_in/dt) 
    rainf   = rainf + rainf_d/(dt_in/dt) 

  end if

  ! if (ierr==0) then

    ! write(6,*) dectime !mjl
    
    ! set-up solar angles
    fjd=dectime-rlongg(1)*0.5/3.1415927
    call solargh(fjd,bpyear,r1,dlt,alp,slag)
    call zenith(fjd,r1,dlt,slag,rlatt,rlongg,dhr,1,coszro2,taudar2)
    call atebccangle(1,1,coszro2,rlongg,rlatt,fjd,slag,dt,sin(dlt))
    
    ! Update urban prognostic variables
    t(1)=tair     ! air temperature
    qg(1)=qair    ! mixing ratio of water vapour
    ps(1)=psurf   ! surface pressure
    sg(1)=swdown  ! downwelling shortwave
    rg(1)=lwdown  ! downwelling longwave
    rnd(1)=rainf  ! rainfall
    snd(1)=0.     ! snowfall
    rho(1)=1.     ! air density
    uzon(1)=windu ! wind U
    vmer(1)=windv ! wind V

    ! use spitter 1986 to estimate diffuse/direct components of sg
    call atebspitter(1,1,fjd,sg,coszro2,diag)

    ! find weekends for day of week loading
    dayload=1.
    select case(iyr)
        case(1)
            if ( (mod(floor(fjd),7)==4) .or. (mod(floor(fjd),7)==5) ) then
                dayload=0.9
            end if
        case(2)
            if ( (mod(floor(fjd),7)==3) .or. (mod(floor(fjd),7)==4) ) then
                dayload=0.9
            end if
        case DEFAULT
            write(6,*) 'ERROR: year not set'
    end select
    ! print *, 'atebwrap dayload:', dayload

    ! Extract albedo
    call atebalb1(1,1,alb,diag,raw=.true.,split=0)
    call atebcalc(fg,eg,tss,wetfac,newrunoff,dt,azmin,sg,rg, &
                  rnd,snd,rho,t,qg,ps,uzon,vmer,vmodmin,diag)

    ! store flux data                  
    fgup=fg(1)
    egup=eg(1)
    lwup=sbconst*tss(1)**4
    swup=swdown*alb(1)
    rnet=swdown-swup+lwdown-lwup

    ! store cumulative fluxes
    rnet_avg = rnet_avg + rnet 
    swup_avg = swup_avg + swup 
    lwup_avg = lwup_avg + lwup 
    fgup_avg = fgup_avg + fgup 
    egup_avg = egup_avg + egup 
    swdown_avg = swdown_avg + swdown 
    lwdown_avg = lwdown_avg + lwdown 
    storage_avg = storage_avg + storage 

    ! store energy flux data
    call energyrecord(atmoserr,atmoserr_bias,surferr,surferr_bias,bldheat,bldcool,intgain,traf,bldtemp,scrntemp)
    ! write(4,'(I5,F9.4,I5,I5,2F9.5,2F8.2,2F8.2,2F8.2,2F10.2)') iyr,dectime,iday,itime,atmoserr,surferr,   &
    !   atmoserr_bias,surferr_bias,bldheat,bldcool,intgain,traf,bldtemp,scrntemp

    bldtemp_avg  = bldtemp_avg + bldtemp
    bldheat_avg  = bldheat_avg + bldheat 
    bldcool_avg  = bldcool_avg + bldcool
    intgain_avg  = intgain_avg + intgain 
    traf_avg     = traf_avg + traf 
    scrntemp_avg = scrntemp_avg + scrntemp 

    call atebenergy(storage,"storage",0)

    ! write to standard output
    ! write(6,'(I5,F9.4,I5,I5,5F10.4,F9.2,F8.2,F10.3)') iyr,dectime,iday,itime,rnet,swup,lwup,fgup,egup,swdown,lwdown,storage
    ! write(6,'(I5,F9.4,I5,I5,2F9.5,2F8.2,2F8.2,2F8.2,2F10.2)') iyr,dectime,iday,itime,atmoserr,surferr,   &
    !   atmoserr_bias,surferr_bias,bldheat_avg,bldcool_avg,intgain_avg,traf_avg,bldtemp_avg,scrntemp_avg

    if ( mod( ncount,ndt_out ) == 0 ) then

      ! calculate average fluxes per output timestep
      rnet_avg = rnet_avg/real(ndt_out)
      swup_avg = swup_avg/real(ndt_out)
      lwup_avg = lwup_avg/real(ndt_out)
      fgup_avg = fgup_avg/real(ndt_out)
      egup_avg = egup_avg/real(ndt_out)
      swdown_avg = swdown_avg/real(ndt_out)
      lwdown_avg = lwdown_avg/real(ndt_out)
      storage_avg = storage_avg/real(ndt_out)

      bldtemp_avg = bldtemp_avg/real(ndt_out)
      bldheat_avg = bldheat_avg/real(ndt_out)
      bldcool_avg = bldcool_avg/real(ndt_out)
      intgain_avg = intgain_avg/real(ndt_out)
      traf_avg = traf_avg/real(ndt_out)
      scrntemp_avg = scrntemp_avg/real(ndt_out)

      ! print *, 'heat',bldheat,bldheat_avg
      ! print *, 'cool',bldcool,bldcool_avg

      ! write(2,'(I5,F9.4,I5,I5,5F10.4,F9.2,F8.2,F10.3)') iyr,dectime,iday,itime,rnet,swup,lwup,fgup,egup,swdown,lwdown,storage
      write(2,'(I5,F9.4,I5,I5,5F10.4,F9.2,F8.2,F10.3)') iyr,dectime,iday,itime,rnet_avg,swup_avg,lwup_avg,fgup_avg,  &
                                                        egup_avg,swdown_avg,lwdown_avg,storage_avg

      write(4,'(I5,F9.4,I5,I5,2F9.5,2F8.2,2F8.2,2F8.2,2F10.2)') iyr,dectime,iday,itime,atmoserr,surferr,   &
        atmoserr_bias,surferr_bias,bldheat_avg,bldcool_avg,intgain_avg,traf_avg,bldtemp_avg,scrntemp_avg

      ! zero out avg fluxes
      rnet_avg = 0.  
      swup_avg = 0.  
      lwup_avg = 0.  
      fgup_avg = 0.  
      egup_avg = 0.  
      swdown_avg = 0.
      lwdown_avg = 0.
      storage_avg = 0.

      bldtemp_avg = 0. 
      bldheat_avg = 0.
      bldcool_avg = 0.
      intgain_avg = 0.
      traf_avg = 0.
      scrntemp_avg = 0.

    end if

  ! end if
  ncount = ncount + 1
  if ( ierr /= 0 ) exit
end do

close(1)
close(2)
close(4)

call atebend(diag)

call cpu_time(stop_time)
print *, "Time:", stop_time - start_time, "seconds"

end program
