#!/bin/bash

module load openmpi
module load netcdf-c/4.4.1.1-intel
module load netcdf-f/4.4.4-intel
module load ncview

set -xv

# This script runs the SCM for multiple months in climate mode

startyear=1999
endyear=2100

basedir=/srv/ccrc/data02/z9901702/phd/UCLEM_SCM/ch5/ACCESS_85_control
metdat=/srv/ccrc/data02/z9901702/phd/UCLEM_SCM/metdata/ACCESS
cmip5=/srv/ccrc/data02/z9901702/phd/UCLEM_SCM/cmip5
ccamdata=/home/z9901702/phd/03-Code/CCAM_SCM/ccamdata
sctail=RCP85
gcm=C192_27_0.0_0.0_1.0_ACCESS1-0_rcp85
scm=/home/z9901702/phd/03-Code/CCAM_SCM/scm

cd $basedir

# configure CO2 file
radfile=$cmip5/$sctail'_MIDYR_CONC.DAT'

runyear=$startyear
until [ $runyear -gt $endyear ]; do

  runmonth=1
  until [ $runmonth -gt 12 ]; do
  
    cmonth=$runmonth
    if [[ $runmonth -lt 10 ]]; then
      cmonth=0$runmonth
    fi

    # output filenames
    timeoutput=$gcm'_time_scm_CCAM.'$runyear$cmonth'.nc'
    profileoutput=$gcm'_profile_scm_CCAM.'$runyear$cmonth'.nc'

    if [[ (! -f $timeoutput)&&(! -f $profileoutput) ]]; then
    
      # calculate number of days in month  
      ndays=31
      if [[ $runmonth -eq 2 ]];then
        ndays=28
#        ((itest=runyear/4))
#        ((itest=itest*4))
#        if [[ $itest -eq $runyear ]];then
#          ndays=29
#          ((itest=runyear/100))
#          ((itest=itest*100))
#          if [[ $itest -eq $runyear ]];then
#            ndays=28
#            ((itest=runyear/400))
#            ((itest=itest*400))
#            if [[ $itest -eq $runyear ]];then
#              ndays=29
#            fi
#          fi
#        fi
      fi
      if [[ $runmonth -eq 4 ]];then
        ndays=30
      fi
      if [[ $runmonth -eq 6 ]];then
        ndays=30
      fi
      if [[ $runmonth -eq 9 ]];then
        ndays=30
      fi
      if [[ $runmonth -eq 11 ]];then
        ndays=30
      fi
    
      ((ntau=ndays*48))
      
      # calculate decade start and end
      ((ddyear=runyear/10))
      ((ddyear=ddyear*10))
      ((deyear=ddyear+9))  
  
      # configure ozone file
      if [[ $runyear -ge 2005 ]];then
        o3file=$cmip5/$sctail/'pp.Ozone_CMIP5_ACC_SPARC_'$ddyear'-'$deyear'_'$sctail'_T3M_O3.nc'
      else
        o3file=$cmip5/historic/'pp.Ozone_CMIP5_ACC_SPARC_'$ddyear'-'$deyear'_historic_T3M_O3.nc'
      fi
    
      # input files
      pyear=$runyear
      ((pmonth=runmonth-1))
      if [[ $pmonth -lt 1 ]]; then
        pmonth=12
        ((pyear=pyear-1))
      fi
      pcmonth=$pmonth
      if [[ $pmonth -lt 10 ]]; then
        pcmonth=0$pmonth
      fi
      ifile='restart_'$pyear$pcmonth'.nc'
      if [[ ! -f $ifile ]]; then
        if [[ $runyear -eq $startyear ]]; then
	  if [[ $runmonth -eq 1 ]]; then
            ifile=""
	  else
            echo "ERROR: missing restart file $ifile"
            exit
	  fi
	else
          echo "ERROR: missing restart file $ifile"
          exit
	fi    
      fi  
    
      restfile='restart_'$runyear$cmonth'.nc'
    
      metforcing=$metdat'/metdat_'$gcm'.'$runyear$cmonth'.nc'
    
      kdate_s=$runyear$cmonth'01'    
    
      # create namelist for scm
      rm input
      cat << END1 > input  
&SCMNML 

  scm_mode="CCAM"

  rlong_in=144.97 rlat_in=-37.81

  gridres = 50.
  z_in=15.
  ivegt_in=14
  isoil_in=2
  soil_albedo=0.2
  
  press_surf=102400.
  
  kl = 59
  press_in=102277.1 101981.7 101588.2 101099.5 100518.6 99848.19 99091.15 98250.24 97328.44 96328.5 95253.3 94105.7 92888.47 91604.48 90256.59 88847.56 87380.38 85857.79 84282.68 82657.79 80986.11 79270.4 77513.42 75718.14 73887.33 72023.96 70130.69 68210.38 66266.01 64300.24 62316.03 60316.26 58303.69 56281.19 54251.52 52217.65 50182.35 48148.48 46118.81 44096.31 42083.74 40083.97 38099.76 36133.99 34189.62 32269.31 30376.04 28512.67 26681.86 24886.58 23129.6 21413.89 19742.21 18117.32 16542.21 15019.62 13552.44 12143.41 10795.52

  kl = 58
  press_in=101800. 101588.2 101099.5 100518.6 99848.19 99091.15 98250.24 97328.44 96328.5 95253.3 94105.7 92888.47 91604.48 90256.59 88847.56 87380.38 85857.79 84282.68 82657.79 80986.11 79270.4 77513.42 75718.14 73887.33 72023.96 70130.69 68210.38 66266.01 64300.24 62316.03 60316.26 58303.69 56281.19 54251.52 52217.65 50182.35 48148.48 46118.81 44096.31 42083.74 40083.97 38099.76 36133.99 34189.62 32269.31 30376.04 28512.67 26681.86 24886.58 23129.6 21413.89 19742.21 18117.32 16542.21 15019.62 13552.44 12143.41 10795.52

  ateb_bldheight = 6.4
  ateb_hwratio = 0.42
  ateb_sigvegc = 0.38
  ateb_sigmabld = 0.45

  ateb_infilach=1.5
  ateb_intgains=4.5
  ateb_bldairtemp=293.16
  ateb_trafficfg=4.0

  ateb_slab_thick = 0.01, 0.04, 0.04, 0.01
  ateb_roofalpha = 0.17

  fixtsurf=.false. nolatent=.false. noradiation=.false.
  nogwdrag=.false. noconvection=.false. nocloud=.false.
  noaerosol=.true. novertmix=.false.
  vert_adv=.true. lsm_only=.false. urbanscrn=.true.
  use_file_for_rain=.false.
  use_file_for_cloud=.false.
  nud_ql=1
  nud_qf=1
  ps_adj=1500.

  metforcing    = "$metforcing"

  timeoutput    = "$timeoutput"
  profileoutput = "$profileoutput"
&end
&CARDIN COMMENT='globpea            '
  ja=2 jb=2 id=2 jd=2


  dt=1800 nwt=1 ntau=$ntau
  
  kdate_s=$kdate_s ktime_s=0000 leap=1 

  nmaxpr=99999
  rescrn=1
  nbd=0 mbd=20
  nud_hrs=1

  epsp=0.1 epsu=0.1 epsh=1. precon=-10000 restol=2.e-7 nh=5 knh=9 mex=4 nstagu=1

  nmlo=0 ol=30 mxd=5001.47 mindep=1.

  nsib=7 ntaft=3 nurban=-1 vmodmin=0.1
  newrough=0 jalbfix=0 nsigmf=0

  nvmix=3 nlocal=6 nhorjlm=0 nhorps=-1 amxlsq=100.
  nvmix=7 nlocal=6 nhorjlm=0 nhorps=-1 amxlsq=9.
  nvmix=6 nlocal=7 nhorjlm=0 nhorps=-1
  nvmix=3 nlocal=6 nhorjlm=0 nhorps=-1 amxlsq=9.

  nvmix=6 nlocal=7 nhorjlm=0 nhorps=-1


  nhor=-157 khor=0

  nrad=5 nmr=1 qgmin=2.E-7
  iaero=0 ch_dust=3.E-10

  localhist=.true.
&end
&EXTRAS  &end
&SKYIN
  mins_rad=-1 liqradmethod=0 iceradmethod=1
  o3_vert_interpolate=1 o3_time_interpolate=0
&end
&datafile
  ifile      ="$ifile"
  restfile   ="$restfile"
  eigenv     ="$ccamdata/eigenv.35"
  o3file     ="$o3file"
  radfile    ="$radfile"
  cnsdir     ="$ccamdata"
&end
&kuonml
  alfsea=1.05 alflnd=1.20
  convfact=1.05 convtime=-2030.60
  fldown=-0.3
  iterconv=3
  ksc=0 kscsea=0 kscmom=1 dsig2=0.1
  mbase=4 nbase=-2
  methprec=5 detrain=0.1 methdetr=-2
  mdelay=0
  ncvcloud=0
  nevapcc=0 entrain=-0.5
  nuvconv=-3
  rhmois=0. rhcv=0.1
  tied_con=0. tied_over=2626.
  nclddia=12 ldr=1
  nevapls=0 ncloud=3 acon=0. bcon=0.02
&end
&turbnml
  buoymeth=1 mineps=1.e-11 qcmf=1.e-4 ezmin=10.
  ngwd=-5 helim=800. fc2=1.
  sigbot_gwd=0. alphaj=0.000001  

  ent0=0.5 ent1=0. ent_min=0.001
  be=1. b1=1. b2=2.

&end
&landnml
  ateb_resmeth=1
  ateb_zohmeth=1
  ateb_acmeth=1
  ateb_intairtmeth=1
  ateb_intmassmeth=2
  ateb_cvcoeffmeth=1
  ateb_statsmeth=1
  ateb_behavmeth=0
  ateb_nrefl=3
  ateb_vegmode=2
  ateb_soilunder=1
  ateb_conductmeth=1
  ateb_scrnmeth=2
  ateb_lweff=2
  ateb_refheight=0.6
  ateb_zomratio=0.0625
  ateb_zocanyon=0.01
  ateb_zoroof=0.01
  ateb_maxrfwater=1.
  ateb_maxrdwater=1.
  ateb_maxrfsn=1.
  ateb_maxrdsn=1.
  ateb_maxvwatf=0.1
  ateb_acfactor=10.
  ateb_ac_heatcap=10.
  ateb_ac_coolcap=10.

  ateb_ac_heatprop=0.5
  ateb_ac_coolprop=0.33
  ateb_ac_deltat=0.0
  ateb_ac_copmax=10.

  ateb_energytol=0.005
&end
&mlonml
  mlodiff=1
&end
&tin  &end
&soilin  &end
  
END1

      $scm > log.$runyear$cmonth

      if [[ ! -f $restfile ]]; then
        rm $timeoutput $profileoutput
      fi

    fi
    
    ((runmonth=runmonth+1))
  done

  ((runyear=runyear+1))
done

exit
