&SCMNML 

  scm_mode="sublime"

  rlong_in=-0.118 rlat_in=51.509
  kl = 59
  gridres = 1.
  z_in=15.
  ivegt_in=14
  isoil_in=2

  press_surf=102400.
  press_in=102277.1 101981.7 101588.2 101099.5 100518.6 99848.19 99091.15 98250.24 97328.44 96328.5 95253.3 94105.7 92888.47 91604.48 90256.59 88847.56 87380.38 85857.79 84282.68 82657.79 80986.11 79270.4 77513.42 75718.14 73887.33 72023.96 70130.69 68210.38 66266.01 64300.24 62316.03 60316.26 58303.69 56281.19 54251.52 52217.65 50182.35 48148.48 46118.81 44096.31 42083.74 40083.97 38099.76 36133.99 34189.62 32269.31 30376.04 28512.67 26681.86 24886.58 23129.6 21413.89 19742.21 18117.32 16542.21 15019.62 13552.44 12143.41 10795.52

  spinup_start = 4
  ntau_spinup = 864
  ntau_spinup = 288

  ! ! Sublime Stage 1
  ateb_bldheight = 17.5
  ateb_hwratio   = 1.4
  ateb_sigvegc   = 0.15
  ateb_sigmabld  = 0.45

  ateb_roof_thick = 0.05, 0.05, 0.05, 0.05
  ateb_roof_cp    = 2.25E6, 2.25E6, 2.25E6, 2.25E6 
  ateb_roof_cond  = 1.0 , 1.0, 1.0, 1.0
  ateb_wall_thick = 0.05, 0.05, 0.05, 0.05
  ateb_wall_cp    = 2.25E6, 2.25E6, 2.25E6, 2.25E6 
  ateb_wall_cond  = 1.0 , 1.0, 1.0, 1.0
  ateb_road_thick = 0.05, 0.20, 0.45, 0.80
  ateb_road_cp    = 2.25E6, 2.25E6, 2.25E6, 2.25E6 
  ateb_road_cond  = 1.0 , 1.0, 1.0, 1.0
  ateb_slab_thick = 0.05, 0.05, 0.05, 0.05
  ateb_slab_cp    = 2.25E6, 2.25E6, 2.25E6, 2.25E6 
  ateb_slab_cond  = 1.0 , 1.0, 1.0, 1.0

  ! ! Sublime Stage 2
  ! ateb_bldheight = 22.
  ! ateb_hwratio   = 1.467
  ! ateb_sigvegc   = 0.15
  ! ateb_sigmabld  = 0.42

  ! ateb_roofalpha = 0.18
  ! ateb_roadalpha = 0.08
  ! ateb_wallalpha = 0.20
  ! ateb_roofemiss = 0.92
  ! ateb_roademiss = 0.95
  ! ateb_wallemiss = 0.90

  ! ateb_roof_thick = 0.05, 0.05, 0.05, 0.05
  ! ateb_roof_cp    = 0.9E6, 0.9E6, 0.9E6, 0.9E6 
  ! ateb_roof_cond  = 0.3 , 0.3, 0.3, 0.3
  ! ateb_wall_thick = 0.05, 0.05, 0.05, 0.05
  ! ateb_wall_cp    = 1.5E6, 1.5E6, 1.5E6, 1.5E6 
  ! ateb_wall_cond  = 0.6 , 0.6, 0.6, 0.6
  ! ateb_road_thick = 0.05, 0.20, 0.45, 0.80
  ! ateb_road_cp    = 1.5E6, 1.5E6, 1.5E6, 1.5E6 
  ! ateb_road_cond  = 0.6 , 0.6, 0.6, 0.6
  ! ateb_slab_thick = 0.05, 0.05, 0.05, 0.05
  ! ateb_slab_cp    = 2.25E6, 2.25E6, 2.25E6, 2.25E6 
  ! ateb_slab_cond  = 1.0 , 1.0, 1.0, 1.0

  fixtsurf=.false.  nolatent=.false. noradiation=.false.

  metforcing    = 'SCM_forcing_July2012.nc'

  timeoutput    = 'sublime_time_scm_CCAM_stage2_v01.nc'
  profileoutput = 'sublime_profile_scm_CCAM_stage2_v01.nc'
&end
&CARDIN COMMENT='globpea            '
  ja=2 jb=2 id=2 jd=2
  dt=200 nwt=180 leap=1
  dt=600 nwt=180 leap=1

  kdate_s=20120723 ktime_s=0000 ntau=972
  kdate_s=20120723 ktime_s=0000 ntau=324


  nmaxpr=99999 nrungcm=0 nextout=1
  io_in=1 io_nest=1 io_out=1 io_rest=1 newtop=1

  mfix=0 mfix_qg=0 mfix_aero=0 mfix_tr=1 rescrn=1
  nbd=0 mbd=20
  nud_p=1 nud_q=1 nud_t=1 nud_uv=1 nud_aero=1
  nud_sst=1 nud_sss=1 nud_ouv=1 nud_sfh=1
  nud_hrs=1 kbotdav=-900 ktopdav=-10 sigramplow=0.05 ktopmlo=1 kbotmlo=30 mloalpha=10

  epsp=0.1 epsu=0.1 epsh=1. precon=-10000 restol=2.e-7 nh=5 knh=9 mex=4 nstagu=1

  nmlo=0 ol=30 mxd=5001.47 mindep=1.

  nsib=7 ntaft=3 nurban=-1 vmodmin=0.1
  newrough=0 jalbfix=0 nsigmf=0

  nvmix=3 nlocal=6 nhorjlm=0 nhorps=-1 amxlsq=9.  ! standard local-Ri  closure
  nvmix=6 nlocal=7 nhorjlm=0 nhorps=-1            ! k-e (TKE-eops) closure



  nhor=-157 khor=0 cgmap_offset=600. cgmap_scale=200.

  nrad=5 nmr=1 qgmin=2.E-7
  iaero=0 ch_dust=3.E-10

  ngwd=-5 helim=800. fc2=1.
  sigbot_gwd=0. alphaj=0.000001

  localhist=.true.
&end
&EXTRAS  &end
&SKYIN
  mins_rad=-1 liqradmethod=0 iceradmethod=1
&end
&datafile
  ifile      ='ccamout_3.nc'
  mesonest   ='ccamout_3.nc'
  ofile      ='ccamout_5.nc'
  restfile   ='restart.nc'
  eigenv     ='ccamdata/eigenv.35'
  o3file     ='ccamdata/pp.Ozone_CMIP5_ACC_SPARC_2010-2019_RCP4.5_T3M_O3.nc'
  radfile    ='ccamdata/co2_data.35'
  cnsdir     ='ccamdata/'
  surf_00    =''
&end
&kuonml
  alfsea=1.05 alflnd=1.20
  convfact=1.05 convtime=-2030.60
  fldown=-0.3
  iterconv=3
  ksc=0 kscsea=0 kscmom=1 dsig2=0.1
  mbase=4 nbase=-2
  methprec=5 detrain=0.1 methdetr=-2
  mdelay=0
  ncvcloud=0
  nevapcc=0 entrain=-0.5
  nuvconv=-3
  rhmois=0. rhcv=0.1
  tied_con=0. tied_over=2626.
  nclddia=12 ldr=1
  nevapls=0 ncloud=3 acon=0. bcon=0.04
&end
&turbnml 
  ent0=0.5 
  ent1=0. 
  ent_min=0.001 
&end
&turbnml
  buoymeth=1 mineps=1.e-11 qcmf=1.e-4 ezmin=10.
  ngwd=-5 helim=800. fc2=1.
  sigbot_gwd=0. alphaj=0.000001  

  ent0=0.5 ent1=0. ent_min=0.001
&end
&landnml
  ateb_resmeth=1 
  ateb_zohmeth=1 
  ateb_acmeth=1
  ateb_intairtmeth=1
  ateb_intmassmeth=2
  ateb_energytol=0.005
  ateb_nrefl=3 
  ateb_vegmode=2 
  ateb_soilunder=1 
  ateb_conductmeth=1
  ateb_cvcoeffmeth=1
  ateb_statsmeth=1 
  ateb_behavmeth=1
  ateb_scrnmeth=1 
  ateb_lweff=2

  ateb_refheight=0.6 
  ateb_zocanyon=0.01 
  ateb_zoroof=0.01
  ateb_maxrfwater=1. 
  ateb_maxrdwater=1. 
  ateb_maxrfsn=1. 
  ateb_maxrdsn=1.
  ateb_maxvwatf=0.1 
  ateb_acfactor=5. 
  ateb_ac_heatcap=3. 
  ateb_ac_coolcap=3.
  ateb_ac_heatprop=1. 
  ateb_ac_coolprop=1. 
  ateb_ac_smooth=0.5 
  ateb_ac_deltat=1.
  ! Stage 1
  ! ateb_zomratio=0.0571 
  ! Stage 2
  ateb_zomratio=0.0864
&end
&mlonml
  mlodiff=1
&end
&tin  &end
&soilin  &end
