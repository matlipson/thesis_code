import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

half_standard = pd.read_csv('output_0.txt',delim_whitespace=True,skiprows=[0],usecols=[1,2,3,4],names=['temp_ext','storageflux','node0','node1'])
half_modified = pd.read_csv('output_1.txt',delim_whitespace=True,skiprows=[0],usecols=[1,2,3,4],names=['temp_ext','storageflux','node0','node1'])
interface     = pd.read_csv('output_2.txt',delim_whitespace=True,skiprows=[0],usecols=[1,2,3,4],names=['temp_ext','storageflux','node0','node1'])
highres       = pd.read_csv('output_3.txt',delim_whitespace=True,skiprows=[0],usecols=[1,2,3,4],names=['temp_ext','storageflux','node0','node1'])

hours=np.arange(0,24.5,0.5)
half_standard.index = hours
half_modified.index = hours
interface.index = hours
highres.index = hours


# Heat storage figure
plt.close('all')
fig, ax = plt.subplots(nrows=1,ncols=1)
fig.set_size_inches(8,4)

highres.storageflux.plot(ls='solid',lw=1.0,color='grey',label='high-res (200-layer, 1-sec)', ax=ax)
interface.storageflux.plot(marker='+',ms=5,mew=0.75,color='royalblue',lw=0.0,label='interface (4-layer, 30-min)', ax=ax)
half_standard.storageflux.plot(marker='o',ms=4,mew=0.5,mfc='none',color='orangered',lw=0.0,label='standard (4-layer, 30-min)', ax=ax)
half_modified.storageflux.plot(marker='x',ms=3,mew=0.5,color='orangered',ls=':',lw=0.0,label='modified (4-layer, 30-min)', ax=ax)
ax.set_ylabel(r'Storage heat flux ($ W m^{-2}$)')
ax.set_xlabel(r'Time (hours)')
ax.set_xticks([0,4,8,12,16,20,24])
# ax.axhline(y=0,c='k',lw=0.25)

ax.set_title('Storage heat flux approximations at half-hour timesteps')

plt.legend()
plt.tight_layout()

plt.savefig('conduction.png',dpi=300)

# Temperature figure
plt.close('all')
fig, ax = plt.subplots(nrows=1,ncols=1)
fig.set_size_inches(8,4)

highres.node1.plot(ls='solid',lw=1.0,color='grey',label='high-res (200-layer, 1-sec)', ax=ax)
interface.node0.plot(marker='x',ms=4,mew=0.75,color='royalblue',lw=0.0,label='interface (4-layer, 30-min)', ax=ax)
half_standard.node1.plot(marker='o',ms=4,mew=0.5,mfc='none',color='orangered',lw=0.0,label='standard (4-layer, 30-min)', ax=ax)
half_modified.node0.plot(marker='+',ms=4,mew=0.5,color='orangered',ls=':',lw=0.0,label='modified (4-layer, 30-min)', ax=ax)
ax.set_ylabel(r'Temperature ($ K $)')
ax.set_xlabel(r'Time (hours)')
ax.set_xticks([0,4,8,12,16,20,24])
# ax.axhline(y=290,c='k',lw=0.5)

ax.set_title('First node temperature approximations at half-hour timesteps')

plt.legend()
plt.tight_layout()

plt.savefig('temperature.png',dpi=200)