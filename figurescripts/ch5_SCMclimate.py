#!/Users/MatLipson/miniconda2/envs/py3x/bin/python

__title__ = "Wrangle netCDF for UCLEM_SCM"
__author__ = "Mathew Lipson"
__version__ = "180718"
__email__ = "m.lipson@unsw.edu.au"

''' This script plots timeseries for energy use for the 21st Century
for Chapter 5 of Model Development for Urban Climates'''

import xarray as xr
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import matplotlib.dates as mdates
import numpy as np
import pandas as pd
import os

from ch5_SCMresults import perkins_skill, plot_kde, plot_hist, plot_hist_kde, plot_qq

plt.rcParams['legend.fontsize'] = 'x-small'

pd.set_option('display.width', 150)
scriptname = "A7_SCMtimeseries.py"
oshome=os.getenv('HOME')

# User inputs
projpath = '.'
datapath = '%s/output/SCM' %projpath
plotpath = '%s/figures/ch5' %projpath
obspath  = '%s/ownCloud/phd/03-Code/phd_python/anthro/data' %oshome

model,exp = {},{}

exp[1] = 'control'
exp[2] = 'cp67'
exp[3] = 'cp100'

exp[4] = 'halfinf'
exp[5] = 'ctplus1'
exp[6] = 'ctminus1'
exp[7] = 'risingAC'

model[1] = 'ACCESS1-0'
model[2] = 'MPI-ESM-LR'
model[3] = 'GFDL-CM3'
model[4] = 'CCSM4'
model[5] = 'NorESM1-M'
model[6] = 'CNRM-CM5'

exps = [(model[1],exp[1]), 
        (model[2],exp[1]), 
        (model[3],exp[1]),
        (model[4],exp[1]),
        (model[5],exp[1]),
        (model[6],exp[1]),
        (model[1],exp[2]),
        (model[1],exp[3]),
        (model[1],exp[4]),
        (model[1],exp[5]),
        (model[1],exp[6]),
        (model[1],exp[7])]

fnames = ['%s_rcp85_time_scm_CCAM.2000_2099_%s.nc' %exps[i] for i in range(len(exps))]

hot_thresh = 25
cld_thresh = 15

def import_data(exps):
    '''imports and organises NetCDF into xarray and pandas data formats'''
    ds = {}
    
    for i,file in enumerate(fnames):
        expid = '%s_%s' %exps[i]
        fpath = '%s/%s' %(datapath,file)
        print('Exp %s' %expid)
        print('File %s: %s' %(i,file))

        # import data
        ds[expid] = xr.open_dataset(fpath)
        # correct off-hour values and localize to Melbourne from utc
        ds[expid].time.values = ds[expid].time.to_index().round('30Min').tz_localize(None) + pd.offsets.Hour(10)
        # remove shortwave at night
        ds[expid].qdw[ds[expid].qdw==0] = np.nan

    return ds

def resample_yearly(dataset):
    '''organises NetCDF into xarray and pandas data formats'''

    fluxlist = ['qf_bem','qf_heat','qf_cool','t2m']

    # convert to pandas dataframe
    df = dataset[fluxlist].to_dataframe()['2000':'2099'].dropna()

    # seperate electricity and gas 
    df['qf_base'] = df['qf_bem'] - df['qf_heat'] - df['qf_cool']
    df['qf_elec'] = 0.76*df['qf_base'] + 0.14*df['qf_heat'] + 1.00*df['qf_cool']
    df['qf_gas']  = 0.24*df['qf_base'] + 0.86*df['qf_heat'] + 0.00*df['qf_cool']

    assert (abs(df['qf_elec'] + df['qf_gas'] - df['qf_bem']).max() < 1E-3),'elec/gas partitioning not conserved!'

    # define tmax dateframe
    df_tmax = df[['t2m']].resample('1D').max()
    # resample to fill whole period with tmax for comparison
    df_tmax_30min = df_tmax.resample('30min').pad()
    df_tmax_30min = df_tmax_30min[df_tmax_30min.index.isin(df.index)]
    # select hot and cold day periods
    df_hot_pds = df_tmax_30min[df_tmax_30min.t2m>hot_thresh +273.16]
    df_cld_pds = df_tmax_30min[df_tmax_30min.t2m<cld_thresh+273.16]

    # select hot and cold day energy use periods
    df_hot = df.loc[df.index.isin(df_hot_pds.index),['qf_bem','t2m']]
    df_cld = df.loc[df.index.isin(df_cld_pds.index),['qf_bem','t2m']]

    # resample yearly statistics into pandas dataframe
    syear = pd.DataFrame()
    syear['Txx'] = df['t2m'].resample('1Y').max() - 273.16
    syear['T99'] = df['t2m'].resample('1Y').apply(lambda x: x.quantile(0.99)) - 273.16
    syear['Tavg']= df['t2m'].resample('1Y').mean() - 273.16
    syear['T01'] = df['t2m'].resample('1Y').apply(lambda x: x.quantile(0.01)) - 273.16
    syear['Tnn'] = df['t2m'].resample('1Y').min() - 273.16

    syear['Exx']  = df['qf_bem'].resample('1Y').max() 
    syear['Eavg'] = df['qf_bem'].resample('1Y').mean()
    syear['Enn']  = df['qf_bem'].resample('1Y').min() 

    syear['Exx_hot']  = df_hot['qf_bem'].resample('1Y').max()
    syear['Eavg_hot'] = df_hot['qf_bem'].resample('1Y').mean()
    syear['Enn_hot']  = df_hot['qf_bem'].resample('1Y').min()

    syear['Exx_cld'] = df_cld['qf_bem'].resample('1Y').max()
    syear['Eavg_cld'] = df_cld['qf_bem'].resample('1Y').mean()
    syear['Enn_cld'] = df_cld['qf_bem'].resample('1Y').min()

    # syear['Exx_heat']  = df['qf_heat'].resample('1Y').max()
    # syear['Eavg_heat'] = df['qf_heat'].resample('1Y').mean()
    # syear['Enn_heat']  = df['qf_heat'].resample('1Y').min()

    # syear['Exx_cool']  = df['qf_cool'].resample('1Y').max()
    # syear['Eavg_cool'] = df['qf_cool'].resample('1Y').mean()
    # syear['Enn_cool']  = df['qf_cool'].resample('1Y').min()

    syear['Exx_elec']  = df['qf_elec'].resample('1Y').max()
    syear['Eavg_elec'] = df['qf_elec'].resample('1Y').mean()
    syear['Enn_elec']  = df['qf_elec'].resample('1Y').min()

    syear['Exx_gas']  = df['qf_gas'].resample('1Y').max()
    syear['Eavg_gas'] = df['qf_gas'].resample('1Y').mean()
    syear['Enn_gas']  = df['qf_gas'].resample('1Y').min()

    # syear['E99']  = df['qf_bem'].resample('1Y').apply(lambda x: x.quantile(0.99))
    # syear['E98']  = df['qf_bem'].resample('1Y').apply(lambda x: x.quantile(0.98))
    # syear['E50']  = df['qf_bem'].resample('1Y').apply(lambda x: x.quantile(0.50))
    # syear['E99_hot'] = df_hot['qf_bem'].resample('1Y').apply(lambda x: x.quantile(0.99))
    # syear['E99_cld'] = df_cld['qf_bem'].resample('1Y').apply(lambda x: x.quantile(0.99))

    return syear

def resample_monthly(df,fluxlist):
    '''calculates monthly statistics for pandas dataframe format'''

    smonth = pd.DataFrame()

    for flux in fluxlist:
        if flux == 't2m':
            smonth['Txx']  = df['t2m'].resample('1M').max() - 273.16
            smonth['Tavg'] = df['t2m'].resample('1M').mean() - 273.16
            smonth['Tnn']  = df['t2m'].resample('1M').mean() - 273.16
        else:
            smonth['Exx'] = df[flux].resample('1M').max()
            smonth['Eavg'] = df[flux].resample('1M').mean()
            smonth['Enn'] = df[flux].resample('1M').min()

    return smonth

def calc_ensemble_mean(var,exp):
    # create means across all models

    ens_mean[var] = pd.concat( [syear['%s_%s' %(model[1],exp)][var],
                                syear['%s_%s' %(model[2],exp)][var],
                                syear['%s_%s' %(model[3],exp)][var],
                                syear['%s_%s' %(model[4],exp)][var],
                                syear['%s_%s' %(model[5],exp)][var],
                                syear['%s_%s' %(model[6],exp)][var]], axis=1).mean(axis=1)
    return ens_mean

def calc_simday(df):
    # calculate daily Tmin and Tmax in Celcius
    simday = pd.DataFrame()
    # simday['t2_avg'] = df['t2m'].resample('1D').mean() - 273.16
    simday['t2_max'] = df['t2m'].resample('1D').max() - 273.16
    simday['t2_min'] = df['t2m'].resample('1D').min() - 273.16
    simday['t2_avg'] = (simday['t2_max']+simday['t2_min']).mean()
    simday['qf_bem_avg'] = df['qf_bem'].resample('1D').mean()
    simday['qf_bem_max'] = df['qf_bem'].resample('1D').max()
    simday['qf_bem_min'] = df['qf_bem'].resample('1D').min()
    simday['rain_day'] = df['rain'].resample('1D').mean()
    return simday

##################################################################
####################### IMPORT SIMULATIONS #######################
##################################################################

ds = import_data(exps)

# calculate yearly info over all decades
syear= {}
for i,file in enumerate(fnames):
    expid = '%s_%s' %exps[i]
    syear[expid] = resample_yearly(ds[expid])
    

ens_exp = exp[1]
print('calculating ensemble means')
ens_mean = pd.DataFrame()

ens_mean = calc_ensemble_mean('Tavg','%s' %ens_exp)
ens_mean = calc_ensemble_mean('Exx_cld','%s' %ens_exp)
ens_mean = calc_ensemble_mean('Exx_hot','%s' %ens_exp)
ens_mean = calc_ensemble_mean('Exx_gas','%s' %ens_exp)
ens_mean = calc_ensemble_mean('Exx_elec','%s' %ens_exp)
ens_mean = calc_ensemble_mean('Eavg','%s' %ens_exp)

# load metdata forcing average theta at 10m (for sanity check)
# metdata = pd.read_csv('%s/inputs/maxminmean_metdata.csv' %projpath,index_col=[0], parse_dates=[0])
# cmipACCESS = pd.read_csv('%s/inputs/tas_minmaxmean_ACCESS1-0_rcp85_r1i1p1_2006_2100.csv' %projpath, index_col=[0], parse_dates=[0])


###################################################################
#######################  IMPORT OBSERVATIONS ######################
###################################################################

obsday = {}
obsday['Bundoora'] = pd.read_csv('%s/inputs/ObsBundoora.csv' %projpath,index_col=[0],parse_dates=[0])
obsday['Melbourne'] = pd.read_csv('%s/inputs/ObsMelb.csv' %projpath,index_col=[0],parse_dates=[0])
# obsday['Preston'] = obsQF.loc['2000':'2009','ElecGas'].resample('1D').mean()

#resample to 30min for later comparison
obsday_30min = obsday['Bundoora'].resample('30Min').pad()

# select hot and cold day periods
obs_hot_pds = obsday_30min[obsday_30min.t2max>hot_thresh]
obs_cld_pds = obsday_30min[obsday_30min.t2max<cld_thresh]

obsQF = pd.read_csv('%s/inputs/Preston_obsQF.csv' %(projpath),index_col=[0])
obsQF.index = pd.date_range(start='20000101-0030',periods=308255,freq='30Min')

# obsQF_sum = obsQF[obsQF.index.month.isin([12,1,2])]
# obsQF_win = obsQF[obsQF.index.month.isin([6,7,8])]

# select hot and cold day energy use periods
obs_hot = obsQF.loc[obsQF.index.isin(obs_hot_pds.index),['Electricity','Gas','ElecGas']]
obs_cld = obsQF.loc[obsQF.index.isin(obs_cld_pds.index),['Electricity','Gas','ElecGas']]

obsend = '2014'
oyear = pd.DataFrame()
oyear['Exx_hot']  = obs_hot.loc['2000':obsend,'ElecGas'].resample('1Y').max()
oyear['Eavg_hot'] = obs_hot.loc['2000':obsend,'ElecGas'].resample('1Y').mean()
oyear['Enn_hot']  = obs_hot.loc['2000':obsend,'ElecGas'].resample('1Y').min()

oyear['Exx_cld']  = obs_cld.loc['2000':obsend,'ElecGas'].resample('1Y').max()
oyear['Eavg_cld'] = obs_cld.loc['2000':obsend,'ElecGas'].resample('1Y').mean()
oyear['Enn_cld']  = obs_cld.loc['2000':obsend,'ElecGas'].resample('1Y').min()

oyear['E99_hot']  = obs_hot.loc['2000':obsend,'ElecGas'].resample('1Y').apply(lambda x: x.quantile(0.99))
oyear['E99_cld']  = obs_cld.loc['2000':obsend,'ElecGas'].resample('1Y').apply(lambda x: x.quantile(0.99))
oyear['E99']      = obsQF.loc['2000':obsend,'ElecGas'].resample('1Y').apply(lambda x: x.quantile(0.99))
oyear['Exx']      = obsQF.loc['2000':obsend,'ElecGas'].resample('1Y').max()
oyear['E50']      = obsQF.loc['2000':obsend,'ElecGas'].resample('1Y').apply(lambda x: x.quantile(0.50))
oyear['Eavg']     = obsQF.loc['2000':obsend,'ElecGas'].resample('1Y').mean()
oyear['Enn']      = obsQF.loc['2000':obsend,'ElecGas'].resample('1Y').min()
oyear['Txx']      = obsday['Bundoora'].loc['2000':obsend,['t2max']].resample('1Y').max()
oyear['Tavg']     = obsday['Bundoora'].loc['2000':obsend,['t2max','t2min']].mean(axis=1).resample('1Y').mean()

oyear['GASxx'] = obsQF.loc['2000':obsend,'Gas'].resample('1Y').max()
oyear['GASnn'] = obsQF.loc['2000':obsend,'Gas'].resample('1Y').min()
oyear['ELCxx'] = obsQF.loc['2000':obsend,'Electricity'].resample('1Y').max()
oyear['ELCnn'] = obsQF.loc['2000':obsend,'Electricity'].resample('1Y').min()

# seperate elec and gas
omonth = pd.DataFrame()
omonth['ELCxx']  = obsQF['Electricity'].resample('1M').max()
omonth['ELCavg'] = obsQF['Electricity'].resample('1M').mean()
omonth['ELCnn']  = obsQF['Electricity'].resample('1M').min()
omonth['GASxx']  = obsQF['Gas'].resample('1M').max()
omonth['GASavg'] = obsQF['Gas'].resample('1M').mean()
omonth['GASnn']  = obsQF['Gas'].resample('1M').min()
omonth['Exx']    = obsQF['ElecGas'].resample('1M').max()
omonth['Eavg']   = obsQF['ElecGas'].resample('1M').mean()
omonth['Enn']    = obsQF['ElecGas'].resample('1M').min()

# seperate gas and electricity from control simulation for months 2000-2009
control_df = ds['ACCESS1-0_control'][['qf_heat','qf_cool','qf_bem']].to_dataframe()['2000':'2009']
control_df['qf_base'] = control_df['qf_bem'] - control_df['qf_heat'] - control_df['qf_cool']
control_df['qf_gas']  = 0.24*control_df['qf_base'] + 0.86*control_df['qf_heat'] + 0.00*control_df['qf_cool']
control_df['qf_elec'] = 0.76*control_df['qf_base'] + 0.14*control_df['qf_heat'] + 1.00*control_df['qf_cool']

assert (abs(control_df['qf_elec'] + control_df['qf_gas'] - control_df['qf_bem']).max() < 1E-3),'elec/gas partitioning not conserved!'

control_m = pd.DataFrame()
control_m['qf_gas_max'] = control_df['qf_gas'].resample('1M').max()
control_m['qf_gas_avg'] = control_df['qf_gas'].resample('1M').mean()
control_m['qf_gas_min'] = control_df['qf_gas'].resample('1M').min()
control_m['qf_elec_max'] = control_df['qf_elec'].resample('1M').max()
control_m['qf_elec_avg'] = control_df['qf_elec'].resample('1M').mean()
control_m['qf_elec_min'] = control_df['qf_elec'].resample('1M').min()


# import proportion of cooling values for risingAC scenario
rising = pd.read_csv('%s/inputs/risingAC.csv' %(projpath), header=None)[:-1]
rising.index = syear['ACCESS1-0_control'].index

# ###########################################################################
# ########## FIGURE: OBSERVED ELECTRICITY AND GAS USE 2000-2009 #############
# ###########################################################################

plt.close('all')

print('plotting observed elec gas usage (2000-2009)')

# annotation properties
alw = 0.40  # arrow lineweight
asa = 0     # arrow shrink a
asb = 0.75  # arrow shrink b
afs = 7     # arrow fontsize

fig,ax = plt.subplots(figsize=(6,2.8))
title = 'Observed electricity and gas energy demand (2000-2009)'
# subtitle = 'downscaled to Preston and accounting for population changes'

gcolour = 'SeaGreen'
ecolour = 'DarkOrchid'

# data
ax.fill_between(omonth.index, omonth['ELCxx'], omonth['ELCnn'],
    lw=0.0,alpha=0.25, color=ecolour, label='Electricity monthly range')
ax.fill_between(omonth.index, omonth['GASxx'], omonth['GASnn'],
    lw=0.0,alpha=0.25, color=gcolour, label='Gas monthly range')
omonth['GASavg'].plot(ax=ax, color=gcolour, lw=1, x_compat=True, label='Gas monthly mean')
omonth['ELCavg'].plot(ax=ax, color=ecolour, lw=1, x_compat=True, label='Electricity monthly mean')

omonth['GASpeak']=omonth.loc[omonth.GASxx>omonth.ELCxx,'GASxx']
omonth['GASpeak'].plot(ax=ax, color=gcolour, lw=0.75, ls='dashed', x_compat=True, label='Gas peak period')
omonth['ELCpeak']=omonth.loc[omonth.ELCxx>omonth.GASxx,'ELCxx']
omonth['ELCpeak'].plot(ax=ax, color=ecolour, lw=0.75, ls='dashed', x_compat=True, label='Electricity peak period')                           

# annotations
xlabel= pd.Timestamp('2004-03-01')
GASpeak = omonth.loc['2005','GASpeak']
ELCpeak = omonth.loc['2005','ELCpeak']

ax.annotate('Gas peak demand',xy=(GASpeak.idxmax(),GASpeak.max()), 
            xycoords='data', xytext=(xlabel, 10.4), fontsize=afs,color='green',ha='right',va='center',
            arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=-40,rad=0',
            color=gcolour,linewidth=alw,shrinkA=asa,shrinkB=asb),clip_on=True)
ax.annotate('Electricity peak demand',xy=(ELCpeak.idxmax(),ELCpeak.max()), 
            xycoords='data', xytext=(xlabel, 9.5), fontsize=afs,color=ecolour,ha='right',va='center',
            arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=-84,rad=0',
            color=ecolour,linewidth=alw,shrinkA=asa,shrinkB=asb),clip_on=True)

# ax.text(x=0.5,y=1.10, s=title, fontsize=11,transform=ax.transAxes,ha='center')
ax.set_title(title, fontsize=8, va='center')
ax.set_ylabel(r'Energy use density [$W/m^{2}$]',fontsize=9)
ax.grid(lw=0.5,color='0.9')
ax.set_axisbelow(True)
ax.set_ylim(bottom=0,top=11)

# # legend
# handles, labels = ax.get_legend_handles_labels()
# legend_order = [0,1,5,4,2,3]
# handles = [handles[i] for i in legend_order]
# labels = [labels[i] for i in legend_order]
# leg = ax.legend(fontsize=6.4,loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.5,-0.3), ncol=3, frameon=True)
# frame = leg.get_frame()
# frame.set_linewidth(0.5)

ax.tick_params(axis='x',labelsize=8,rotation=0)

ax.set_xlim([pd.Timestamp('2000'),pd.Timestamp('2010')])
for tick in ax.get_xticklabels():
    tick.set_horizontalalignment('center')

fig.savefig('%s/ch5_obs_energy_demand.pdf' %(plotpath), dpi=300,bbox_inches='tight',pad_inches=0.05)

# ############################################################################
# ########## FIGURE: SIMULATED ELECTRICITY AND GAS USE 2000-2009 #############
# ############################################################################

plt.close('all')

print('plotting simulated elec/gas usage (2000-2009)')

# annotation properties
alw = 0.40  # arrow lineweight
asa = 0     # arrow shrink a
asb = 0.75  # arrow shrink b
afs = 7     # arrow fontsize

fig,ax = plt.subplots(figsize=(6,2.8))
title = 'Simulated electricity and gas energy demand (2000-2009)'
# subtitle = 'downscaled to Preston and accounting for population changes'

gcolour = 'SeaGreen'
ecolour = 'DarkOrchid'

# data
ax.fill_between(control_m.index, control_m['qf_elec_max'], control_m['qf_elec_min'],
    lw=0.0,alpha=0.25, color=ecolour, label='Electricity monthly range')
ax.fill_between(control_m.index, control_m['qf_gas_max'], control_m['qf_gas_min'],
    lw=0.0,alpha=0.25, color=gcolour, label='Gas monthly range')
control_m['qf_gas_avg'].plot(ax=ax, color=gcolour, lw=1, x_compat=True, label='Gas monthly mean')
control_m['qf_elec_avg'].plot(ax=ax, color=ecolour, lw=1, x_compat=True, label='Electricity monthly mean')

control_m['GASpeak']=control_m.loc[control_m.qf_gas_max>control_m.qf_elec_max,'qf_gas_max']
control_m['GASpeak'].plot(ax=ax, color=gcolour, lw=0.75, ls='dashed', x_compat=True, label='Gas peak period')
control_m['ELCpeak']=control_m.loc[control_m.qf_elec_max>control_m.qf_gas_max,'qf_elec_max']
control_m['ELCpeak'].plot(ax=ax, color=ecolour, lw=0.75, ls='dashed', x_compat=True, label='Electricity peak period')                           

# # annotations
# xlabel= pd.Timestamp('2004-03-01')
# GASpeak = control_m.loc['2005','GASpeak']
# ELCpeak = control_m.loc['2005','ELCpeak']

# ax.annotate('Gas peak demand',xy=(GASpeak.idxmax(),GASpeak.max()), 
#             xycoords='data', xytext=(xlabel, 10.4), fontsize=afs,color='green',ha='right',va='center',
#             arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=-20,rad=0',
#             color=gcolour,linewidth=alw,shrinkA=asa,shrinkB=asb),clip_on=True)
# ax.annotate('Electricity peak demand',xy=(ELCpeak.idxmax(),ELCpeak.max()), 
#             xycoords='data', xytext=(xlabel, 9.5), fontsize=afs,color=ecolour,ha='right',va='center',
#             arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=-84,rad=0',
#             color=ecolour,linewidth=alw,shrinkA=asa,shrinkB=asb),clip_on=True)

# ax.text(x=0.5,y=1.10, s=title, fontsize=11,transform=ax.transAxes,ha='center')
ax.set_title(title, fontsize=8, va='center')
ax.set_ylabel(r'Energy use density [$W/m^{2}$]',fontsize=9)
ax.grid(lw=0.5,color='0.9')
ax.set_axisbelow(True)
ax.set_ylim(bottom=0,top=11)

# legend
handles, labels = ax.get_legend_handles_labels()
legend_order = [0,1,5,4,2,3]
handles = [handles[i] for i in legend_order]
labels = [labels[i] for i in legend_order]
leg = ax.legend(fontsize=6.4,loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.5,-0.3), ncol=3, frameon=True)
frame = leg.get_frame()
frame.set_linewidth(0.5)

ax.tick_params(axis='x',labelsize=8,rotation=0)
ax.set_xlabel('')

ax.set_xlim([pd.Timestamp('2000'),pd.Timestamp('2010')])
for tick in ax.get_xticklabels():
    tick.set_horizontalalignment('center')

fig.savefig('%s/ch5_sim_energy_demand.pdf' %(plotpath), dpi=300,bbox_inches='tight',pad_inches=0.05)

#############################################################################
############### FIGURE: SIMULATED COMBINED DEMAND 2000-2009 #################
#############################################################################

print('plotting decadal simulated combined demand')

plt.close('all')

plt.rcParams['hatch.linewidth'] = 0.2

sim = 'ACCESS1-0_control'

# create monthly statistics for simulated
smonth = {}
dataset = ds[sim][['qf_bem','t2m']].to_dataframe()['2000':'2009'].dropna()
# offline = pd.read_csv('%s/inputs/simQF_ACCESS_85_cp35.csv' %projpath,usecols=[0,7],index_col=[0],parse_dates=[0])
read = pd.read_csv('%s/output/offline/ch5_control_offline.nrg' %projpath,usecols=[8,9,10],delim_whitespace=True) #index_col=[0],parse_dates=[0])
offline = pd.DataFrame( data=read.sum(axis=1).values,
                        index=pd.date_range(start='20030812-1330',periods=22772,freq='30Min'),
                        columns=['ElecGas'])

smonth[sim]= resample_monthly(dataset,['qf_bem','t2m'])
smonth['offline'] = resample_monthly(offline,['ElecGas'])

fig,ax = plt.subplots(figsize=(6,3))
title = 'Electricity and gas energy demand (2000-2009)'
ax.set_title(title, fontsize=8, va='center')
# subtitle = 'downscaled to Preston and accounting for population changes'

# observed data
ax.fill_between(omonth.index, omonth['Exx'], omonth['Enn'],
    lw=0.0,alpha=0.25, color='black', label='Observed monthly range')
omonth['Eavg'].plot(ax=ax, color='black', lw=1, x_compat=True, label='Observed monthly mean')

# # dummy data
# ax.fill_between([],[],[],lw=0.0,alpha=0.25, color='red', label='Simulated monthly range (coupled)')
# ax.plot([],[], color='red', lw=1, label='Simulated monthly mean (coupled)')

# coupled data
ax.fill_between(smonth[sim].index, smonth[sim]['Exx'], smonth[sim]['Enn'],
    # lw=0.5,alpha=0.75, facecolor='none', hatch='XXXXXXXXX', edgecolor='royalblue', label='Simulated monthly range (coupled)')
    lw=0.0,alpha=0.25, color='royalblue', label='Simulated monthly range (coupled)')
smonth[sim]['Eavg'].plot(ax=ax, color='royalblue', lw=1, x_compat=True, label='Simulated monthly mean (coupled)')

# offline data
ax.fill_between(smonth['offline'].index, smonth['offline']['Exx'], smonth['offline']['Enn'],
    # lw=0.0,alpha=0.25, color='red', label='Simulated monthly range (offline)')
    lw=0.4,alpha=0.5, zorder=10, facecolor='none', hatch='XXXXXXX', edgecolor='red', label='Simulated monthly range (offline)')
smonth['offline']['Eavg'].plot(ax=ax, color='red', alpha=0.65, lw=1, x_compat=True, label='Simulated monthly mean (offline)')

# offline annotation
df = smonth['offline']
ax.plot( [df.index[0],df.index[0],df.index[-1],df.index[-1]], 
         [df.iloc[0,0]+0.4,14.5,14.5,df.iloc[-1,0]+0.4], color='red', linewidth=0.3)
ax.plot( [df.index[0],df.index[0],df.index[-1],df.index[-1]], 
         [df.iloc[0,2]-0.4, 2, 2,df.iloc[-1,2]-0.4], color='red', linewidth=0.3)
ax.text(x=pd.Timestamp('2004-04-15') ,y=1.8, s='Flux tower\nobservation period',
    fontsize=7,color='red',ha='center',va='top')

# labels
ax.set_ylabel(r'Energy use density [$W/m^{2}$]',fontsize=9)
ax.grid(lw=0.5,color='0.9')
ax.set_axisbelow(True)
ax.set_ylim(bottom=0)
ax.set_xlabel('')

# legend
handles, labels = ax.get_legend_handles_labels()
legend_order = [0,3,2,5,1,4]
handles = [handles[i] for i in legend_order]
labels = [labels[i] for i in legend_order]
leg = ax.legend(fontsize=6.4,loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.5,-0.3), ncol=3, frameon=True)
frame = leg.get_frame()
frame.set_linewidth(0.5)

ax.tick_params(axis='x',labelsize=8,rotation=0)

ax.set_xlim([pd.Timestamp('2000'),pd.Timestamp('2010')])
ax.set_ylim([0,15])
for tick in ax.get_xticklabels():
    tick.set_horizontalalignment('center')

fig.savefig('%s/ch5_obs_combined_demand.pdf' %(plotpath), dpi=300,bbox_inches='tight',pad_inches=0.05)

#########################################################################
############### FIGURE: CLIMATE SCALE ENERGY USE COMBINED ###############
#########################################################################

print('plotting combined demand to 2100')

plt.close('all')

control = syear['ACCESS1-0_control']
title  = 'Future energy demand in RCP 8.5 (control)'
top    = 'Exx'

fig,ax = plt.subplots(figsize=(6,3))
degtick = mtick.StrMethodFormatter('{x:,.0f}°C')

# simulated data
ax.fill_between(control['Exx'].index, control['Exx'], control['Enn'],
                                lw=0.25,alpha=0.25, color='royalblue', label='Energy use: yearly range')
control['Eavg'].plot(ax=ax, label='Energy use: yearly mean', 
                                lw=1.25, alpha=0.75,color='royalblue', x_compat=True)
control['Exx'].plot(ax=ax, label='Energy use: yearly peak', 
                                lw=1.0, alpha=0.75, ls='dashed', color='royalblue', x_compat=True)

# end of century decadal average change: max
label_offset = 0.0
df1 = control.loc['2000':'2009','Exx']
df2 = control.loc['2090':'2099','Exx']
ax.plot([ pd.Timestamp('2000-12-31'),pd.Timestamp('2009-12-31') ], [ df1.mean(),df1.mean() ],color='blue',alpha=0.75)
ax.plot([ pd.Timestamp('2090-12-31'),pd.Timestamp('2099-12-31') ], [ df2.mean(),df2.mean() ],color='blue',alpha=0.75)
ax.text(pd.Timestamp('2100-06-01'), df2.mean()+label_offset, '$%+.0f%%$%%' %(((df2.mean()/df1.mean())-1.)*100), 
    fontsize=7, ha='left', va='center', color='blue')

# # end of century decadal average change: min
# df1 = syear['%s_%s' %('ACCESS1-0',sims[0])].loc['2000':'2009','Enn']
# df2 = control.loc['2090':'2099','Enn']
# ax.plot([ pd.Timestamp('2000-12-31'),pd.Timestamp('2009-12-31') ], [ df1.mean(),df1.mean() ],color='blue',alpha=0.75)
# ax.plot([ pd.Timestamp('2090-12-31'),pd.Timestamp('2099-12-31') ], [ df2.mean(),df2.mean() ],color='blue',alpha=0.75)
# ax.text(pd.Timestamp('2100-06-01'), df2.mean()-label_offset, '$%+.0f%%$%%' %(((df2.mean()/df1.mean())-1.)*100), 
#     fontsize=7, ha='left', va='center', color='blue')

# end of century decadal average change: mean
df1 = control.loc['2000':'2009','Eavg']
df2 = control.loc['2090':'2099','Eavg']
ax.plot([ pd.Timestamp('2000-12-31'),pd.Timestamp('2009-12-31') ], [ df1.mean(),df1.mean() ],color='blue',alpha=0.75)
ax.plot([ pd.Timestamp('2090-12-31'),pd.Timestamp('2099-12-31') ], [ df2.mean(),df2.mean() ],color='blue',alpha=0.75,
    label = r'percent change: first to last decade')
ax.text(pd.Timestamp('2100-06-01'), df2.mean(), '$%+.0f%%$%%' %(((df2.mean()/df1.mean())-1.)*100), 
    fontsize=7, ha='left', va='center', color='blue')

# observed data
oyear['Exx'].plot(ax=ax, label='Energy use: yearly peak (obs)',
                      color='black', ls='none', marker='^', ms=4, mew=0.9, mfc='none', x_compat=True)
oyear['Enn'].plot(ax=ax, label='Energy use: yearly min. (obs)',
                      color='black', ls='none', marker='v', ms=4, mew=0.9, mfc='none', x_compat=True)
oyear['Eavg'].plot(ax=ax, label='Energy use: yearly mean (obs)', 
                                lw=0.75, alpha=0.75, color='black', x_compat=True)

df1 = control.loc['2000':'2009','Eavg']
df2 = control.loc['2090':'2099','Eavg']
# ax.plot([ pd.Timestamp('2090-12-31'),pd.Timestamp('2099-12-31') ], [ df2.mean(),df2.mean() ],color='red')
# ax.text(pd.Timestamp('2099-12-31'), df2.mean()+0.1, '%.1f%%' %(((df2.mean()/df1.mean())-1.)*100), 
#     fontsize=8, ha='right', va='bottom', color='royalblue')

ax.set_title(title, fontsize=8, va='center')
ax.set_ylabel(r'Energy use density [$W/m^{2}$]',fontsize=9)
ax.grid(lw=0.5,color='0.9')
ax.set_axisbelow(True)
ax.set_ylim(bottom=0)
ax.set_xlabel('')

ax.tick_params(axis='x',labelsize=8,rotation=0)

ax.set_xlim([pd.Timestamp('2000'),pd.Timestamp('2100')])
ax.set_ylim([0,21])
ax.set_yticks([0,5,10,15,20])
for tick in ax.get_xticklabels():
    tick.set_horizontalalignment('center')

# temperature plot on second axis
ax2 = ax.twinx()
control[['Tavg']].plot(ax=ax2, color='gold', linewidth=1.0, x_compat=True, legend=False)
ax2.plot([0,0],[0,0],color='w',label=' ')
ax2.set_ylim([0,21])
ax2.set_yticks([15,20])
ax2.yaxis.set_major_formatter(degtick)
ax2.text(x=0.99,y=1.01, s='Air temp.', fontsize=8,transform=ax.transAxes,ha='left',va='bottom')
print('2000-2009 Txx: %.1f' %control.loc['2000':'2009','Txx'].mean())
print('2000-2009 Tavg: %.1f' %control.loc['2000':'2009','Tavg'].mean())
print('2000-2009 Tnn: %.1f' %control.loc['2000':'2009','Tnn'].mean())
print('2090-2099 Txx: %.1f' %control.loc['2090':'2099','Txx'].mean())
print('2090-2099 Tavg: %.1f' %control.loc['2090':'2099','Tavg'].mean())
print('2090-2099 Tnn: %.1f' %control.loc['2090':'2099','Tnn'].mean())

ax.tick_params(axis='both',labelsize=8,rotation=0)
ax2.tick_params(axis='both',labelsize=8,rotation=0)

# legend
handles, labels = ax.get_legend_handles_labels()
handles2, labels2 = ax2.get_legend_handles_labels()
labels2 = ['2m temp: yearly mean','']
handles += handles2
labels += labels2

legend_order = [3,5,4,1,0,6,2,7]
handles = [handles[i] for i in legend_order]
labels = [labels[i] for i in legend_order]
ax.legend(fontsize=6.0,loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.5,-0.35), ncol=3, frameon=True)

fig.savefig('%s/ch5_climatescale_combined.pdf' %(plotpath), dpi=300,bbox_inches='tight',pad_inches=0.05)

#########################################################################
############### FIGURE: CLIMATE SCALE ENERGY USE HOT/COLD ###############
#########################################################################

plt.close('all')

sims   = ['control','cp67','cp100','halfinf','ctplus1','ctminus1','risingAC']
# title  = 'Future energy demand (hot and cold days) in RCP 8.5'
annots = ['control','67% spaces with AC','100% spaces with AC','half infiltration','thermostat +1°C','thermostat -1°C','AC rising b/w 28-67%']
ecolour = 'DarkOrchid'
gcolour = 'SeaGreen'
degtick = mtick.StrMethodFormatter('{x:,.0f}°C')

for i,sim in enumerate(sims):
    print('plotting hot/cold demand for %s' %sim)
    s = '%s_%s' %('ACCESS1-0',sim)
    annot = annots[i]

    title  = 'Future building energy demand (%s)' %annot

    fig,ax = plt.subplots(figsize=(6,3))


    syear[s]['Eavg'].plot(ax=ax, label='All days yearly mean', 
                                    lw=1.00, ls='solid', alpha=1.0, color='royalblue', x_compat=True)
    syear[s]['Exx_cld'].plot(ax=ax, label='Cold day yearly peak', 
                                    lw=1.00, ls='dashed',alpha=1.0, color=gcolour, x_compat=True)
    syear[s]['Exx_hot'].plot(ax=ax, label='Hot day yearly peak', 
                                    lw=1.00, ls='dashed',alpha=1.0, color=ecolour, x_compat=True)

    control['Eavg'].plot(ax=ax, label='All days mean (control)',
                            color='royalblue', ls='solid', lw=0.5, alpha=0.75, x_compat=True)
    control['Exx_cld'].plot(ax=ax, label='Cold day peak (control)',
                            color=gcolour, ls='dashed', lw=0.5, alpha=0.75, x_compat=True)
    control['Exx_hot'].plot(ax=ax, label='Hot day peak (control)',
                            color=ecolour, ls='dashed', lw=0.5, alpha=0.75, x_compat=True)

    oyear['Exx_cld'].plot(ax=ax, label='Cold peak (obs)', marker='x', ms=2.5,
                            color=gcolour, lw=0.0, alpha=0.75, x_compat=True)
    oyear['Exx_hot'].plot(ax=ax, label='Hot peak (obs)', marker='x', ms=2.5,
                            color=ecolour, lw=0.0, alpha=0.75, x_compat=True)

    # end of century decadal average change: cold
    label_offset = 0.0
    if sim == 'cp67':
        label_offset = 0.4
    df1 = syear['%s_%s' %('ACCESS1-0',sims[0])].loc['2000':'2009','Exx_cld']
    df2 = syear[s].loc['2090':'2099','Exx_cld']
    ax.plot([ pd.Timestamp('2000-12-31'),pd.Timestamp('2009-12-31') ], [ df1.mean(),df1.mean() ],color=gcolour,alpha=0.75)
    ax.plot([ pd.Timestamp('2090-12-31'),pd.Timestamp('2099-12-31') ], [ df2.mean(),df2.mean() ],color=gcolour,alpha=0.75,
        label='Cold day peak: % change from control')
    ax.text(pd.Timestamp('2100-06-01'), df2.mean()+label_offset, '$%+.0f%%$%%' %(((df2.mean()/df1.mean())-1.)*100), 
        fontsize=7, ha='left', va='center', color=gcolour)

    # end of century decadal average change: hot
    df1 = syear['%s_%s' %('ACCESS1-0',sims[0])].loc['2000':'2009','Exx_hot']
    df2 = syear[s].loc['2090':'2099','Exx_hot']
    ax.plot([ pd.Timestamp('2000-12-31'),pd.Timestamp('2009-12-31') ], [ df1.mean(),df1.mean() ],color=ecolour,alpha=0.75)
    ax.plot([ pd.Timestamp('2090-12-31'),pd.Timestamp('2099-12-31') ], [ df2.mean(),df2.mean() ],color=ecolour,alpha=0.75,
        label='Hot day peak: % change from control')
    ax.text(pd.Timestamp('2100-06-01'), df2.mean()-label_offset, '$%+.0f%%$%%' %(((df2.mean()/df1.mean())-1.)*100), 
        fontsize=7, ha='left', va='center', color=ecolour)

    # end of century decadal average change: mean
    df1 = syear['%s_%s' %('ACCESS1-0',sims[0])].loc['2000':'2009','Eavg']
    df2 = syear[s].loc['2090':'2099','Eavg']
    ax.plot([ pd.Timestamp('2000-12-31'),pd.Timestamp('2009-12-31') ], [ df1.mean(),df1.mean() ],color='royalblue',alpha=0.75)
    ax.plot([ pd.Timestamp('2090-12-31'),pd.Timestamp('2099-12-31') ], [ df2.mean(),df2.mean() ],color='royalblue',alpha=0.75,
        label='All days mean: % change from control')
    ax.text(pd.Timestamp('2100-06-01'), df2.mean(), '$%+.0f%%$%%' %(((df2.mean()/df1.mean())-1.)*100), 
        fontsize=7, ha='left', va='center', color='royalblue')

    ax.set_title(title, fontsize=8, va='center')
    ax.set_ylabel(r'Energy use density [$W/m^{2}$]',fontsize=9)
    ax.grid(lw=0.5,color='0.9')
    ax.set_axisbelow(True)
    ax.set_ylim(bottom=0,top=20)
    ax.set_yticks([0,5,10,15,20])
    ax.set_xlabel('')

    # annotations
    if sim in ['control']:
        xlabel  = pd.Timestamp('2049-01-01')
        xypeak1 = control.loc['2055':'2060','Exx_cld']
        xypeak2 = control.loc['2055':'2060','Exx_hot']
        xypeak3 = control.loc['2055':'2060','Eavg']
        ax.annotate('Peak demand on cold days', xy=(xypeak1.idxmin(),xypeak1.min()), 
                    xycoords='data', xytext=(xlabel, 10.7), fontsize=afs,color=gcolour,ha='right',va='center',
                    arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=70,rad=0',
                    color=gcolour,linewidth=alw,shrinkA=asa,shrinkB=asb),clip_on=True)
        ax.annotate('Peak demand on hot days', xy=(xypeak2.idxmax(),xypeak2.max()), 
                    xycoords='data', xytext=(xlabel, 9.3), fontsize=afs,color=ecolour,ha='right',va='center',
                    arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=-70,rad=0',
                    color=ecolour,linewidth=alw,shrinkA=asa,shrinkB=asb),clip_on=True)
        # ax.annotate('Combined mean demand', xy=(xypeak3.idxmax(),xypeak3.max()), 
        #             xycoords='data', xytext=(xlabel, 3), fontsize=afs,color='royalblue',ha='right',va='center',
        #             arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=70,rad=0',
        #             color='royalblue',linewidth=alw,shrinkA=asa,shrinkB=asb),clip_on=True)

    # legend
    if sim in ['cp100','ctminus1']:
        handles, labels = ax.get_legend_handles_labels()
        legend_order = [1,2,0,4,5,3,6,7,8]
        handles = [handles[i] for i in legend_order]
        labels = [labels[i] for i in legend_order]
        ax.legend(fontsize=6,loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.5,-0.35), ncol=3, frameon=True)

    ax.tick_params(axis='both',labelsize=8,rotation=0)

    ax.set_xlim([pd.Timestamp('2000'),pd.Timestamp('2100')])
    if sim not in ['london','newyork']:
        ax.set_ylim([0,20])
    for tick in ax.get_xticklabels():
        tick.set_horizontalalignment('center')

    fig.savefig('%s/ch5_climatescale_hotcold_%s.pdf' %(plotpath,s), dpi=300,bbox_inches='tight',pad_inches=0.05)

#########################################################################
############### FIGURE: CLIMATE SCALE ENERGY USE ELEC/GAS ###############
#########################################################################

plt.close('all')

sims   = ['control','cp67','cp100','halfinf','ctplus1','ctminus1','risingAC']
# title  = 'Future energy demand (hot and cold days) in RCP 8.5'
annots = ['control','67% spaces with AC','100% spaces with AC','half infiltration','thermostat +1°C','thermostat -1°C','AC rising b/w 28-67%']
ecolour = 'DarkOrchid'
gcolour = 'SeaGreen'

for i,sim in enumerate(sims):
    print('plotting elec/gas demand for %s' %sim)
    s = '%s_%s' %('ACCESS1-0',sim)
    annot = annots[i]

    title  = 'Future electricity and gas demand (%s)' %annot

    fig,ax = plt.subplots(figsize=(6,3))


    syear[s]['Eavg'].plot(ax=ax, label='Combined yearly mean', 
                                    lw=1.00, ls='solid', alpha=1.0, color='royalblue', x_compat=True)
    syear[s]['Exx_gas'].plot(ax=ax, label='Gas yearly peak', 
                                    lw=1.00, ls='dashed',alpha=1.0, color=gcolour, x_compat=True)
    syear[s]['Exx_elec'].plot(ax=ax, label='Electricity yearly peak', 
                                    lw=1.00, ls='dashed',alpha=1.0, color=ecolour, x_compat=True)

    control['Eavg'].plot(ax=ax, label='Combined mean (control)',
                            color='royalblue', ls='solid', lw=0.5, alpha=0.75, x_compat=True)
    control['Exx_gas'].plot(ax=ax, label='Gas peak (control)',
                            color=gcolour, ls='dashed', lw=0.5, alpha=0.75, x_compat=True)
    control['Exx_elec'].plot(ax=ax, label='Electricity peak (control)',
                            color=ecolour, ls='dashed', lw=0.5, alpha=0.75, x_compat=True)

    oyear['GASxx'].plot(ax=ax, label='Gas peak (obs)', marker='x', ms=2.5,
                            color=gcolour, lw=0.0, alpha=0.75, x_compat=True)
    oyear['ELCxx'].plot(ax=ax, label='Electricity peak (obs)', marker='x', ms=2.5,
                            color=ecolour, lw=0.0, alpha=0.75, x_compat=True)

    # end of century decadal average change: cold
    label_offset = 0.0
    if sim == 'cp67':
        label_offset = 0.4
    df1 = syear['%s_%s' %('ACCESS1-0',sims[0])].loc['2000':'2009','Exx_gas']
    df2 = syear[s].loc['2090':'2099','Exx_gas']
    ax.plot([ pd.Timestamp('2000-12-31'),pd.Timestamp('2009-12-31') ], [ df1.mean(),df1.mean() ],color=gcolour,alpha=0.75)
    ax.plot([ pd.Timestamp('2090-12-31'),pd.Timestamp('2099-12-31') ], [ df2.mean(),df2.mean() ],color=gcolour,alpha=0.75,
        label='Gas peak: % change from control')
    ax.text(pd.Timestamp('2100-06-01'), df2.mean()+label_offset, '$%+.0f%%$%%' %(((df2.mean()/df1.mean())-1.)*100), 
        fontsize=7, ha='left', va='center', color=gcolour)

    # end of century decadal average change: hot
    df1 = syear['%s_%s' %('ACCESS1-0',sims[0])].loc['2000':'2009','Exx_elec']
    df2 = syear[s].loc['2090':'2099','Exx_elec']
    ax.plot([ pd.Timestamp('2000-12-31'),pd.Timestamp('2009-12-31') ], [ df1.mean(),df1.mean() ],color=ecolour,alpha=0.75)
    ax.plot([ pd.Timestamp('2090-12-31'),pd.Timestamp('2099-12-31') ], [ df2.mean(),df2.mean() ],color=ecolour,alpha=0.75,
        label='Electricity peak: % change from control')
    ax.text(pd.Timestamp('2100-06-01'), df2.mean()-label_offset, '$%+.0f%%$%%' %(((df2.mean()/df1.mean())-1.)*100), 
        fontsize=7, ha='left', va='center', color=ecolour)

    # end of century decadal average change: mean
    df1 = syear['%s_%s' %('ACCESS1-0',sims[0])].loc['2000':'2009','Eavg']
    df2 = syear[s].loc['2090':'2099','Eavg']
    ax.plot([ pd.Timestamp('2000-12-31'),pd.Timestamp('2009-12-31') ], [ df1.mean(),df1.mean() ],color='royalblue',alpha=0.75)
    ax.plot([ pd.Timestamp('2090-12-31'),pd.Timestamp('2099-12-31') ], [ df2.mean(),df2.mean() ],color='royalblue',alpha=0.75,
        label='Combined mean: % change from control')
    ax.text(pd.Timestamp('2100-06-01'), df2.mean(), '$%+.0f%%$%%' %(((df2.mean()/df1.mean())-1.)*100), 
        fontsize=7, ha='left', va='center', color='royalblue')

    ax.set_title(title, fontsize=8, va='center')
    ax.set_ylabel(r'Energy use density [$W/m^{2}$]',fontsize=9)
    ax.grid(lw=0.5,color='0.9')
    ax.set_axisbelow(True)
    ax.set_ylim(bottom=0,top=20)
    ax.set_yticks([0,5,10,15,20])
    ax.set_xlabel('')

    # annotations
    if sim in ['control']:
        xlabel  = pd.Timestamp('2047-01-01')
        xypeak1 = syear[s].loc['2055':'2060','Exx_gas']
        xypeak2 = syear[s].loc['2055':'2060','Exx_elec']
        xypeak3 = syear[s].loc['2055':'2060','Eavg']
        ax.annotate('Gas peak demand', xy=(xypeak1.idxmax(),xypeak1.max()), 
                    xycoords='data', xytext=(xlabel, 12.8), fontsize=afs,color=gcolour,ha='right',va='center',
                    arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=-70,rad=0',
                    color=gcolour,linewidth=alw,shrinkA=asa,shrinkB=asb),clip_on=True)
        ax.annotate('Electricity peak demand', xy=(xypeak2.idxmax(),xypeak2.max()), 
                    xycoords='data', xytext=(xlabel, 11.4), fontsize=afs,color=ecolour,ha='right',va='center',
                    arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=-70,rad=0',
                    color=ecolour,linewidth=alw,shrinkA=asa,shrinkB=asb),clip_on=True)
        # ax.annotate('Combined mean demand', xy=(xypeak3.idxmin(),xypeak3.max()),
        #             xycoords='data', xytext=(xlabel, 3), fontsize=afs,color='royalblue',ha='right',va='center',
        #             arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=70,rad=0',
        #             color='royalblue',linewidth=alw,shrinkA=asa,shrinkB=asb),clip_on=True)

    # legend
    # if sim in ['cp100','ctminus1']:
    handles, labels = ax.get_legend_handles_labels()
    legend_order = [1,2,0,4,5,3,6,7,8]
    handles = [handles[i] for i in legend_order]
    labels = [labels[i] for i in legend_order]
    ax.legend(fontsize=6,loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.5,-0.35), ncol=3, frameon=True)

    ax.tick_params(axis='both',labelsize=8,rotation=0)

    ax.set_xlim([pd.Timestamp('2000'),pd.Timestamp('2100')])

    for tick in ax.get_xticklabels():
        tick.set_horizontalalignment('center')

    if sim in ['risingAC']:
        ax2 = ax.twinx()
        rising.plot(ax=ax2, color='gold', linewidth=1.0, x_compat=True, legend=False)
        ax2.set_ylim(0,1)
        ax2.set_yticks([0,0.67,1])
        ax2.tick_params(axis='y', which='both', labelsize=8)

    fig.savefig('%s/ch5_climatescale_elecgas_%s.pdf' %(plotpath,s), dpi=300,bbox_inches='tight',pad_inches=0.05)

########################################################################
############### FIGURE: CLIMATE SCALE ENSEMBLE VARIABILITY #############
########################################################################

plt.close('all')

title = 'Future energy demand (hot and cold days) in RCP 8.5'
alpha = 0.15 

fig,ax = plt.subplots(figsize=(6,3))
degtick = mtick.StrMethodFormatter('{x:,.0f}°C')

# ensemble members
for i in [1,2,3,4,5,6]:
    # cold days
    syear['%s_%s' %(model[i],'control')]['Exx_cld'].plot(ax=ax, 
            label='Ensemble member: cold day peak (67% AC)',
            lw=0.75, alpha=alpha, color='green', ls='solid', x_compat=True, legend=False)
    syear['%s_%s' %(model[i],'control')]['Enn_cld'].plot(ax=ax, 
            label='Ensemble member: cold day min. (67% AC)',
            lw=0.75, alpha=alpha, color='green', ls='dashed', x_compat=True, legend=False)
    # hot days
    syear['%s_%s' %(model[i],'control')]['Exx_hot'].plot(ax=ax, 
            label='Ensemble member: hot day peak (67% AC)', 
            lw=0.75, alpha=alpha, color='blue',  ls='solid', x_compat=True, legend=False)
    syear['%s_%s' %(model[i],'control')]['Enn_hot'].plot(ax=ax, 
            label='Ensemble member: hot day min. (67% AC)', 
            lw=0.75, alpha=alpha, color='blue', ls='dashed', x_compat=True, legend=False)

# ensemble mean
ens_mean['Exx_cld'].plot(ax=ax, 
            label='Ensemble mean: cold day peak (67% AC)', 
            lw=1, color='green', ls='solid', x_compat=True, legend=True)
ens_mean['Exx_hot'].plot(ax=ax, 
            label='Ensemble mean: hot day peak (67% AC)', 
            lw=1, color='blue', ls='solid', x_compat=True, legend=True)

# observed data
oyear['Exx_cld'].plot(ax=ax, label='Observed cold day peak',
                      color='green', ls='none', marker='^', ms=4, mew=0.9, mfc='none', x_compat=True)
oyear['Enn_cld'].plot(ax=ax, label='Observed cold day min.',
                      color='green', ls='none', marker='v', ms=4, mew=0.9, mfc='none', x_compat=True)

oyear['Exx_hot'].plot(ax=ax, label='Observed hot day peak',
                      color='blue', ls='none', marker='^', ms=4, mew=0.9, mfc='none', x_compat=True)
oyear['Enn_hot'].plot(ax=ax, label='Observed hot day min.',
                      color='blue', ls='none', marker='v', ms=4, mew=0.9, mfc='none', x_compat=True)

label_offset = 0.0
df1 = control.loc['2000':'2009','Exx_cld']
df2 = ens_mean.loc['2090':,'Exx_cld']
ax.plot([ pd.Timestamp('2090-12-31'),pd.Timestamp('2099-12-31') ], [ df2.mean(),df2.mean() ],color='green',alpha=0.5)
ax.text(pd.Timestamp('2100-06-01'), df2.mean()+label_offset, '%+.0f%%' %(((df2.mean()/df1.mean())-1.)*100), 
    fontsize=7, ha='left', va='center', color='green')

df1 = control.loc['2000':'2009','Exx_hot']
df2 = ens_mean.loc['2090':,'Exx_hot']
ax.plot([ pd.Timestamp('2090-12-31'),pd.Timestamp('2099-12-31') ], [ df2.mean(),df2.mean() ],color='blue',alpha=0.5)
ax.text(pd.Timestamp('2100-06-01'), df2.mean()-label_offset, '%+.0f%%' %(((df2.mean()/df1.mean())-1.)*100), 
    fontsize=7, ha='left', va='center', color='blue')


ax.set_title(title, fontsize=8, va='center')
ax.set_ylabel(r'Energy use density ($W m^{-2}$)')
ax.grid(lw=0.5,color='0.9')
ax.set_axisbelow(True)
ax.set_ylim(bottom=0)
ax.set_xlabel('')

# legend
handles, labels = ax.get_legend_handles_labels()
handles = handles[-6:]
labels = labels[-6:]
# legend_order = [0,3,2,5,1,4]
# handles = [handles[i] for i in legend_order]
# labels = [labels[i] for i in legend_order]
ax.legend(fontsize=6.5,loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.5,-0.3), ncol=3, frameon=True)

ax.tick_params(axis='x',labelsize=8,rotation=0)

ax.set_xlim([pd.Timestamp('2000'),pd.Timestamp('2100')])
ax.set_ylim([0,20])
for tick in ax.get_xticklabels():
    tick.set_horizontalalignment('center')

fig.savefig('%s/ch5_climate_ensemble.pdf' %(plotpath), dpi=300,bbox_inches='tight',pad_inches=0.05)

# # # ###############################################################################
# # # #################### FIGURE: Prop of cooled spaces ############################
# # # ###############################################################################

# fig,ax = plt.subplots(figsize=(6,1))
# rising.plot(ax=ax,lw=1,color='gold')
# ax.set_ylim([0,1])
# ax.set_ylabel('Proportion of cooled spaces')

# plt.show()

# # # #################################################################
# # # #################### FIGURE: SCATTER ############################
# # # #################################################################

# times = ['06:00','09:00','12:00','15:00','18:00','21:00','00:00','03:00','06:00']
# times = ['05:00','08:00','11:00','14:00','17:00','20:00','23:00','02:00','05:00']
# colours=['cyan','red','orangered','orange','brown','royalblue','darkblue','green']

# # colours=['0.25']*8

# # idx = obsQF['ExtAir'].dropna().index
# idx = obsQF.loc[obsQF.index.dayofweek<5,'ExtAir'].dropna().index
# combined = pd.concat([obsQF.loc[idx,['ElecGas','Electricity','Gas']],obsQF.loc[idx,'ExtAir']],axis=1,join='inner')

# yval = 'ElecGas'

# plt.close('all')
# fig, ax = plt.subplots(figsize=(4,4))

# for it,time in enumerate(times):
#     if it != len(times)-1:
#         combined.between_time(times[it],times[it+1]).plot.scatter(x='ExtAir',y=yval,
#             marker='o',s=5,alpha=0.4,color=colours[it],lw=0,ax=ax)
#         ax.scatter([],[],s=10,color=colours[it],label='%s - %s' %(times[it],times[it+1]))

# handles, labels = ax.get_legend_handles_labels()
# # legend_order = range(1,17,2)
# # handles = [handles[i] for i in legend_order]
# # labels = [labels[i] for i in legend_order]
# ax.legend(handles=handles, labels=labels, frameon=True,loc='upper right')

# ax.set_ylabel(r'Energy use density ($W m^{-2}$)')
# ax.set_xlabel(r'Temperature (°C)')
# ax.xaxis.set_major_formatter(degtick)
# ax.set_xlim([0,40])
# ax.set_ylim([1.9,12])

# fig.savefig('%s/ch5_vplot1_we.png' %(plotpath), dpi=300,bbox_inches='tight',pad_inches=0.05)

# ###############################

# plt.close('all')
# fig, ax = plt.subplots(figsize=(4,4))

# t1 = 3
# t2 = 7
# combined.between_time(times[t1],times[t1+1]).plot.scatter(x='ExtAir',y=yval,
#             marker='o',s=5,alpha=0.4,color=colours[t1],lw=0,ax=ax)
# combined.between_time(times[t2],times[t2+1]).plot.scatter(x='ExtAir',y=yval,
#             marker='o',s=5,alpha=0.4,color=colours[t2],lw=0,ax=ax)
# ax.scatter([],[],s=10,color=colours[t1],label='%s - %s' %(times[t1],times[t1+1]))
# ax.scatter([],[],s=10,color=colours[t2],label='%s - %s' %(times[t2],times[t2+1]))
# ax.legend(loc='upper right')

# ax.set_ylabel(r'Energy use density ($W m^{-2}$)')
# ax.set_xlabel(r'Temperature (°C)')
# ax.xaxis.set_major_formatter(degtick)
# ax.set_xlim([0,40])
# ax.set_ylim([1.9,12])

# fig.savefig('%s/ch5_vplot2_we.png' %(plotpath), dpi=300,bbox_inches='tight',pad_inches=0.05)

# ###############################



# # #################################################################
# # #################### FIGURE: PEAK DEMAND ########################
# # #################################################################

# plt.close('all')

# alpha = 0.15

# sim1 = 'ACCESS1-0_cp33_if125'
# sim2 = 'ACCESS1-0_cp33_if1'

# Evars = ['Exx','E99']
# Enames = ['Peak','99%']

# for Evar,Ename in zip(Evars,Enames):
#     fig,ax = plt.subplots(figsize=(7,4))

#     ax.set_title('%s energy demand (%s): hot (>30°C) and cold (<15°C) days' %(Ename,Evar))

#     # ensemble members
#     for i in [1,2,3,4,5,6]:
#         (syear['%s_%s' %(model[i],ens_exp)]['%s_cld' %Evar]).plot(ax=ax, 
#                 label='%s ensemble member: cold day' %(Evar),  # model[i].split('-')[0]
#                 lw=0.75, alpha=alpha, color='blue', x_compat=True, legend=False)
#         syear['%s_%s' %(model[i],ens_exp)]['%s_hot' %Evar].plot(ax=ax, 
#                 label='%s ensemble member: hot day (%s%% AC)' %(Evar,ens_exp[2:]), 
#                 lw=0.75, alpha=alpha, color='red', x_compat=True, legend=False)

#     # ensemble mean
#     ens_mean['%s_cld' %Evar].plot(ax=ax, 
#                 label='%s ensemble mean: cold day'%(Evar), 
#                 lw=1, color='blue', ls='solid', x_compat=True, legend=True)
#     ens_mean['%s_hot' %Evar].plot(ax=ax, 
#                 label='%s ensemble mean: hot day (%s%% AC) '%(Evar,ens_exp[2:]), 
#                 lw=1, color='red', ls='solid', x_compat=True, legend=True)

#     oyear['%s_cld' %Evar].plot(ax=ax, 
#                 label='%s observations: cold day' %(Evar), 
#                 lw=0, marker='D',ms=2.5,mec='k',mew=0.5, color='blue', x_compat=True, legend=True)
#     oyear['%s_hot' %Evar].plot(ax=ax, 
#                 label='%s observations: hot day' %(Evar), 
#                 lw=0, marker='D',ms=2.5,mec='k',mew=0.5, color='red', x_compat=True, legend=True)

#     # alternative cooling proportion
#     syear['ACCESS1-0_%s' %exp[2]]['%s_hot' %Evar].plot(ax=ax, 
#                 label='%s member: hot day (%s%% AC)' %(Evar,exp[2][2:]), 
#                 lw=1.0, alpha=0.5, ls='dashed',color='orangered', x_compat=True, legend=False)
#     syear['ACCESS1-0_%s' %exp[3]]['%s_hot' %Evar].plot(ax=ax,
#                 label='%s member: hot day (%s%% AC)' %(Evar,exp[3][2:]),
#                 lw=1.0, alpha=0.5, ls='dotted',color='orangered',x_compat=True, legend=False)


#     syear[sim1].loc['2000':,'%s_cld' %Evar].plot(ax=ax,
#         label='%s cold %s'%(Evar,sim1), 
#         lw=1, color='black', ls='solid', alpha=0.5, x_compat=True, legend=True)
#     syear[sim1].loc['2000':,'%s_hot' %Evar].plot(ax=ax,
#         label='%s hot %s'%(Evar,sim1), 
#         lw=1, color='black', ls='solid', alpha=0.5, x_compat=True, legend=True)
#     syear[sim2].loc['2000':,'%s_cld' %Evar].plot(ax=ax,
#         label='%s cold %s'%(Evar,sim2), 
#         lw=1, color='black', ls='dashed', alpha=0.5, x_compat=True, legend=True)
#     syear[sim2].loc['2000':,'%s_hot' %Evar].plot(ax=ax,
#         label='%s hot %s'%(Evar,sim2), 
#         lw=1, color='black', ls='dashed', alpha=0.5, x_compat=True, legend=True)

#     ax.set_ylabel(r'Energy demand density ($\mathrm{W m^{-2}}$)', fontsize=9)

#     ax.set_xlabel('')
#     ax.xaxis.set_major_locator(mdates.YearLocator(base=10))
#     plt.xticks(ax.get_xticks(), fontsize=8, rotation=0, ha='center')
#     plt.yticks(range(0,21,2), fontsize=8)

#     ax.grid(lw=0.5,color='0.9')
#     ax.set_xlim([pd.Timestamp('1999'),pd.Timestamp('2101')])

#     handles, labels = ax.get_legend_handles_labels()
#     # legend_order=range(len(handles)) # natural order
#     # legend_order=[15,10,12,16,11,14,17,18,13]
#     # legend_order=[14,12,10,15,13,11,16,17,18]
#     legend_order = [14,10,12,15,11,13,16,17]
#     handles = [handles[i] for i in legend_order]
#     labels = [labels[i] for i in legend_order]
#     ax.legend(fontsize=6,loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.5,-0.27), ncol=3, frameon=True)
#     # fig.subplots_adjust(bottom = 0.2)

#     fig.savefig('%s/%sDemand_hotcold_2000-2100_cp35_70.png' %(plotpath,Evar), dpi=300,bbox_inches='tight',pad_inches=0.05)

# # ################################################################################################################
# # # ##### MEAN DEMAND #######
# # ################################################################################################################

# plt.close('all')

# fig,ax = plt.subplots(figsize=(7,4))
# ax2 = plt.twinx()

# alpha = 0.15
# degtick = mtick.StrMethodFormatter('{x:,.0f}°C')

# Evar = 'Eavg'
# Ename = 'Median'

# ax.set_title('%s energy demand and average temperature' %Ename)

# # ensemble members
# for i in [1,2,3,4,5,6]:
#     syear['%s_%s' %(model[i],ens_exp)][Evar].plot(ax=ax, 
#             label='E50 ensemble member (%s%% AC)' %(ens_exp[2:]),  # model[i].split('-')[0]
#             lw=0.75, alpha=alpha, color='red', x_compat=True, legend=False)
#     syear['%s_%s' %(model[i],ens_exp)]['Tavg'].plot(ax=ax2, 
#             label='Tavg ensemble member (%s%% AC)' %(ens_exp[2:]), 
#             lw=0.75, alpha=alpha, color='royalblue', x_compat=True, legend=False)

# # ensemble mean
# ens_mean[Evar].plot(ax=ax, 
#             label='E50 ensemble mean (%s%% AC)' %(ens_exp[2:]), 
#             lw=1, color='red', ls='solid', x_compat=True, legend=True)
# ens_mean['Tavg'].plot(ax=ax2, 
#             label='Tavg ensemble member (%s%% AC)' %(ens_exp[2:]), 
#             lw=1, color='royalblue', ls='solid', x_compat=True, legend=False)


# oyear[Evar].plot(ax=ax, 
#             label='E50 observations (Preston)', 
#             lw=0, marker='D',ms=2.5,mec='k',mew=0.5, color='red', x_compat=True, legend=True)
# oyear['Tavg'].plot(ax=ax2, 
#             label='Tavg observations (Bundoora)', 
#             lw=0, marker='s',ms=2.5,mec='k',mew=0.5,color='royalblue', x_compat=True, legend=False)

# syear[sim1].loc['2000':,Evar].plot(ax=ax,
#     label='%s %s'%(Evar,sim1), 
#     lw=1, color='black', ls='solid', alpha=0.5, x_compat=True, legend=True)
# syear[sim2].loc['2000':,Evar].plot(ax=ax,
#     label='%s %s'%(Evar,sim2), 
#     lw=1, color='black', ls='dashed', alpha=0.5, x_compat=True, legend=True)

# # met forcing data
# # (metdata.loc['2000':'2099']-273.16-0.25).plot(ax=ax2,color='gold',lw=0.5, x_compat=True)
# # CMIP5 output surface air temperature (tas)
# # (cmipACCESS-273.16).plot(ax=ax2,lw=1.0,color='black',label='cmip tas output (ACCESS)')

# syear['ACCESS1-0_%s' %exp[3]]['Tavg'].plot(ax=ax2,
#             label='ACCESS no QF',lw=1.0, alpha=0.5, ls='dotted',color='royalblue',x_compat=True, legend=False)
# # alternative cooling proportion
# # syear['ACCESS1-0_%s' %exp[2]]['Eavg'].plot(ax=ax, 
# #             label='Avg E (%s%% AC)' %(exp[2][2:]), 
# #             lw=1.0, alpha=1.0, ls='dashed',color='darkblue', x_compat=True, legend=False)
# # syear['ACCESS1-0_%s' %exp[2]]['Tavg'].plot(ax=ax2, 
# #             label='Avg E (%s%% AC)' %(exp[2][2:]), 
# #             lw=1.0, alpha=1.0, ls='dashed',color='darkorange', x_compat=True, legend=False)
# # syear['ACCESS1-0_%s' %exp[3]]['E50'].plot(ax=ax,
# #             label='Avg T (%s%% AC)' %(exp[3][2:]),
# #             lw=1.0, alpha=1.0, ls='dashed',color='orange',x_compat=True, legend=False)
# # syear['ACCESS1-0_%s' %exp[3]]['Tavg'].plot(ax=ax2,
# #             label='Avg T (%s%% AC)' %(exp[3][2:]),
# #             lw=1.0, alpha=1.0, ls='dashed',color='orange',x_compat=True, legend=False)

# ax.set_ylabel(r'Energy demand density ($\mathrm{W m^{-2}}$)', fontsize=9)
# ax.set_xlabel('')
# ax.xaxis.set_major_locator(mdates.YearLocator(base=10))
# # ax.set_ylim([0,5.0])
# ax2.set_ylim([14,22])
# # ax.set_ylim(bottom=0,top=5.1)
# # ax2.set_ylim(bottom=0,top=21)
# plt.sca(ax)
# plt.xticks(ax.get_xticks(), fontsize=8, rotation=0, ha='center')
# plt.yticks(ax.get_yticks(), fontsize=8)
# plt.sca(ax2)
# plt.xticks(ax2.get_xticks(), fontsize=8, rotation=0, ha='center')
# plt.yticks(ax2.get_yticks(), fontsize=8)
# ax2.yaxis.set_major_formatter(degtick)
# ax.grid(lw=0.5,color='0.9')
# ax.set_xlim([pd.Timestamp('1999'),pd.Timestamp('2101')])


# handles, labels = ax.get_legend_handles_labels()
# handles2, labels2 = ax2.get_legend_handles_labels()
# # legend_order=range(len(handles)) # natural order
# # legend_order=[7,5,6,15,13,14]
# # handles += handles2
# # labels += labels2
# # handles = [handles[i] for i in legend_order]
# # labels = [labels[i] for i in legend_order]
# ax.legend(fontsize=6,loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.5,-0.27), ncol=2, frameon=True)
# # fig.subplots_adjust(bottom = 0.2)

# fig.savefig('%s/AvgDemand_2000-2100_cp35_70.png' %(plotpath), dpi=300,bbox_inches='tight',pad_inches=0.05)

