import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pandas as pd
import sys
import os

##### globals #####

pd.set_option('display.width', 150)
scriptname = "uclem_ep.py"
oshome=os.getenv('HOME')

exp    = 'ch4_EPvsUCLEM'
epname = 'EP_unit_ig4_infc1'
ucname = 'UCLEM_'
dt     = 60
nl     = 4
fullname = '%s' %(exp)

projpath = '..'
datapath = '%s/figurescripts/output' %projpath
plotpath = '%s/figurescripts/figures' %projpath

bldheight = 6
bldwidth = 10
bldlength = 1000

area = bldwidth*bldlength
bldhw = bldheight/bldwidth

colours = ['brown','red','orange','green','purple','black','blue']

def read_epdata():
    ''' read energyplus data into facet dataframes'''

    # 1 minute timesteps
    skiprows = 1

    colnames = ['skintemp_int','cvflux_int','lwrad_int','cond_int','cond_ext','storage'] # ,'storage'
    # read selected csv data
    walle = pd.read_csv('%s/%sout.csv' %(datapath,epname), header=None, names=colnames, skiprows=skiprows, usecols=[11,12,13,14,15,16])
    wallw = pd.read_csv('%s/%sout.csv' %(datapath,epname), header=None, names=colnames, skiprows=skiprows, usecols=[23,24,25,26,27,28])
    slab  = pd.read_csv('%s/%sout.csv' %(datapath,epname), header=None, names=colnames, skiprows=skiprows, usecols=[29,30,31,32,33,34])
    roof  = pd.read_csv('%s/%sout.csv' %(datapath,epname), header=None, names=colnames, skiprows=skiprows, usecols=[35,36,37,38,39,40])

    temps = pd.read_csv('%s/%sout.csv' %(datapath,epname), header=None, 
        names = ['airtemp_ext','skintemp_walle','skintemp_wallw','skintemp_slab','skintemp_roof','airtemp_int'], skiprows=skiprows, usecols=[1,11,23,29,35,41])

    intflux = pd.read_csv('%s/%sout.csv' %(datapath,epname), header=None, names=['intgains','infloss','infgain'],skiprows=skiprows, usecols=[2,42,43])/area
    acflux = pd.read_csv('%s/%sout.csv' %(datapath,epname), header=None, names=['acloss','acgain','elecdemand'],skiprows=skiprows, usecols=[47,48,49])/area

    # set index times
    walle.index   = pd.date_range(start='20000101-0000',periods=1441,freq='1Min')
    wallw.index   = pd.date_range(start='20000101-0000',periods=1441,freq='1Min')
    slab.index    = pd.date_range(start='20000101-0000',periods=1441,freq='1Min')
    roof.index    = pd.date_range(start='20000101-0000',periods=1441,freq='1Min')
    temps.index   = pd.date_range(start='20000101-0000',periods=1441,freq='1Min')
    intflux.index = pd.date_range(start='20000101-0000',periods=1441,freq='1Min')
    acflux.index  = pd.date_range(start='20000101-0000',periods=1441,freq='1Min')

    flux = pd.DataFrame()

    flux = flux.assign(infil   = (intflux['infgain'] - intflux['infloss'])/60,  # convert Joules to Watts
                       intgains= intflux['intgains'],
                       acflux  = acflux['acloss']-acflux['acgain'],
                       convect = -bldhw*(walle['cvflux_int']+wallw['cvflux_int']) - (slab['cvflux_int']+roof['cvflux_int']),
                       cond_ext = bldhw*(walle['cond_ext']+wallw['cond_ext']) + (slab['cond_ext']+roof['cond_ext']),
                       storage = bldhw*(walle['storage']+wallw['storage']) + (slab['storage']+roof['storage']),
                       elecdemand = acflux['elecdemand'] ) 

    return walle,wallw,slab,roof,temps,flux

def read_uclemdata(dt,its):
    ''' read uclem data into single dataframe '''
    uclem = pd.read_csv('%s/UCLEM_%ss_its%s.dat' %(datapath,dt,its), delim_whitespace=True,usecols=range(1,9))
    if dt<=180:
        uclem.index = pd.date_range(start='20000101-0000',periods=481,freq='3Min')
    elif dt==600:
        # uclem.index = pd.date_range(start='19991231-2350',periods=145,freq='10Min')
        uclem.index = pd.date_range(start='20000101-0000',periods=145,freq='10Min')
    else:
        # uclem.index = pd.date_range(start='19991231-2330',periods=49,freq='30Min')
        uclem.index = pd.date_range(start='20000101-0000',periods=49,freq='30Min')

    return uclem

####################################################################
### print column names
test = pd.read_csv('%s/%sout.csv' %(datapath,epname))
for i, colname in enumerate(test.columns):
    print(i,colname)


#####################################################################
# read EnergyPlus data
ep_walle,ep_wallw,ep_slab,ep_roof,ep_temps,ep_flux = read_epdata()

# read UCLEM data
uc_fluxes = {}
uc_fluxes[0] = read_uclemdata(60,6)
uc_fluxes[1] = read_uclemdata(600,6)
uc_fluxes[2] = read_uclemdata(1800,6)
uc_fluxes[3] = read_uclemdata(1800,18)

uc_fluxes[4] = read_uclemdata(60,18)
uc_fluxes[5] = read_uclemdata(600,18)

#####################################################################
# plot flux data
plt.close('all')

name = ['60s_its6','600s_its6','1800s_its6','1800s_its18']

fig,axi = plt.subplots(4,1,figsize=(6.8,10.5))

for i, ax in enumerate(axi.reshape(-1)):

    ax2 = ax.twinx()

    # fluxes
    ep_temps[['airtemp_ext','airtemp_int']].plot(ax=ax,color=colours[5:7],label=['airtemp_ext','airtemp_int'],ls='solid',lw=1.2,alpha=0.5)
    ep_flux[['convect','infil','intgains','acflux']].plot(ax=ax,color=colours,ls='solid',lw=1.2,alpha=0.5)
    ep_slab['lwrad_int'].plot(ax=ax,color=colours[4],ls='solid',lw=1.2,alpha=0.5)

    uc_fluxes[i][['ext_air_T','int_air_T']].plot(ax=ax,color=colours[5:7],label=['airtemp_ext','airtemp_int'],ls='dashed',lw=1.2,alpha=0.6,
        marker='|',markersize=0,mew=0.5)
    uc_fluxes[i][['convect','infil','intgains','acflux']].plot(ax=ax,color=colours,ls='dashed',lw=1.2,alpha=0.6,
        marker='|',markersize=0,mew=0.5)
    (-1*uc_fluxes[i]['nodetemp']).plot(ax=ax,color=colours[4],ls='dashed',lw=1.2,alpha=0.6,
        marker='|',markersize=0,mew=0.5)

    # annotations
    if i == 0:
        xlabel = pd.Timestamp('2000-01-01 06:30:00')
        xdata0  = pd.Timestamp('2000-01-01 07:15:00')
        xdata1  = pd.Timestamp('2000-01-01 08:00:00')
        xdata2  = pd.Timestamp('2000-01-01 09:00:00')
        xdata3  = pd.Timestamp('2000-01-01 10:00:00')
        xdata4  = pd.Timestamp('2000-01-01 16:30:00')
        xdata5  = pd.Timestamp('2000-01-01 18:30:00')

        alw = 0.25  # arrow lineweight
        asa = 0     # arrow shrink a
        asb = 0.75  # arrow shrink b
        afs = 6     # arrow fontsize

        ax.annotate('Internal air temperature',xy=(xdata2,ep_temps['airtemp_int'][xdata2]), 
                    xycoords='data', xytext=(xlabel, 44), fontsize=afs,color=colours[6],ha='right',
                    arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=110,rad=0',
                    color=colours[6],linewidth=alw,shrinkA=asa,shrinkB=asb))
        ax.annotate('External temperature (forcing)',xy=(xdata1,ep_temps['airtemp_ext'][xdata1]), 
                    xycoords='data', xytext=(xlabel, 38), fontsize=afs,color=colours[5],ha='right',
                    arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=110,rad=0',
                    color=colours[5],linewidth=alw,shrinkA=asa,shrinkB=asb))

        ax.annotate('Infiltration flux',xy=(xdata0,ep_flux['infil'][xdata0]), 
                    xycoords='data', xytext=(xlabel, -30), fontsize=afs,color=colours[1],ha='right',
                    arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=70,rad=0',
                    color=colours[1],linewidth=alw,shrinkA=asa,shrinkB=asb))
        ax.annotate('Longwave radiation flux (slab)',xy=(xdata2,ep_slab['lwrad_int'][xdata2]), 
                    xycoords='data', xytext=(xlabel, -36), fontsize=afs,color=colours[4],ha='right',
                    arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=70,rad=0',
                    color=colours[4],linewidth=alw,shrinkA=asa,shrinkB=asb))
        ax.annotate('Internal surface convection flux',xy=(xdata3,ep_flux['convect'][xdata3]), 
                    xycoords='data', xytext=(xlabel, -42), fontsize=afs,color=colours[0],ha='right',
                    arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=70,rad=0',
                    color=colours[0],linewidth=alw,shrinkA=asa,shrinkB=asb))
        ax.annotate('Air conditioning flux',xy=(xdata4,ep_flux['acflux'][xdata4]), 
                    xycoords='data', xytext=(xlabel, -48), fontsize=afs,color=colours[3],ha='right',
                    arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=60,rad=0',
                    color=colours[3],linewidth=alw,shrinkA=asa,shrinkB=asb))

        xlabel = pd.Timestamp('2000-01-01 19:30:00')

        ax.annotate('Internal gains flux',xy=(xdata5,ep_flux['intgains'][xdata5]), 
                    xycoords='data', xytext=(xlabel, -8), fontsize=afs,color=colours[2],ha='left',
                    arrowprops=dict( arrowstyle='->',connectionstyle='angle,angleA=0,angleB=110,rad=0',
                    color=colours[2],linewidth=alw,shrinkA=asa,shrinkB=asb))

    if name[i] == '60s_its6':
        ax.text(0.01,0.98,'a) 1 min',transform=ax.transAxes,fontsize=8,ha='left',va='top',fontweight='bold')
        ax.text(0.75,0.97,'EnergyPlus (solid) @ 1 min ',transform=ax.transAxes,fontsize=6,ha='left',va='top')
        ax.text(0.75,0.92,'UCLEM (dashed) @ 1 min',transform=ax.transAxes,fontsize=6,ha='left',va='top')
    if name[i] == '600s_its6':
        ax.text(0.01,0.98,'b) 10 min',transform=ax.transAxes,fontsize=8,ha='left',va='top',fontweight='bold')
        ax.text(0.75,0.97,'EnergyPlus (solid) @ 1 min',transform=ax.transAxes,fontsize=6,ha='left',va='top')
        ax.text(0.75,0.92,'UCLEM (dashed) @ 10 min',transform=ax.transAxes,fontsize=6,ha='left',va='top')
    if name[i] == '1800s_its6':
        ax.text(0.01,0.98,'c) 30 min',transform=ax.transAxes,fontsize=8,ha='left',va='top',fontweight='bold')
        ax.text(0.75,0.97,'EnergyPlus (solid) @ 1 min',transform=ax.transAxes,fontsize=6,ha='left',va='top')
        ax.text(0.75,0.92,'UCLEM (dashed) @ 30 min',transform=ax.transAxes,fontsize=6,ha='left',va='top')
    if name[i] == '1800s_its18':
        ax.text(0.01,0.98,'d) 30 min, 18 internal iterations',transform=ax.transAxes,fontsize=8,ha='left',va='top',fontweight='bold')
        ax.text(0.75,0.97,'EnergyPlus (solid) @ 1 min',transform=ax.transAxes,fontsize=6,ha='left',va='top')
        ax.text(0.75,0.92,'UCLEM (dashed) @ 30 min, \n   with 3x default iterations',transform=ax.transAxes,fontsize=6,ha='left',va='top')

    ax2.plot([15778080+1000,15778080+1065],[53,53],color='0.25',ls='solid',lw=1.2,alpha=0.5)
    ax2.plot([15778080+1000,15778080+1065],[47.5,47.5],color='0.25',ls='dashed',lw=1.2,alpha=0.5)

    ax.axes.axhline(0, color='k', lw=0.5)
    ax.grid(False)

    ax.legend_.remove()
    # fig.legend(ncol=6,loc='lower center')

    ax.set_xlim([ep_temps.index[0],ep_temps.index[-1]])
    ax.set_ylim([-54,58])
    ax2.set_ylim([-54,58])

    # labels
    ax.set_xlabel('Time')

    ax.set_ylabel('Flux ($W m^{-2}$)',labelpad=-1)
    ax.tick_params(axis='both',which='both', color='0.25',right=True,labelright=True,labelsize=6)

    ax2.tick_params(axis='both',which='both', color='0.25',right=False,labelright=False,labelsize=6)
    ax2.set_ylabel('Temperature (°C)',labelpad=22)
    
    # remove date information from labels
    labels = [item.get_text() for item in ax.get_xticklabels()]
    labels[0] = '00:00'
    labels[-1]= '00:00'
    ax.set_xticklabels(labels,fontsize=6)

    # ticks and spines
    for spine in ax.spines.values():
        spine.set_edgecolor('0.75')
    ax.set_facecolor('0.98')
    
axi[0].set_title('EnergyPlus vs UCLEM at different timesteps')

fig.subplots_adjust(hspace = 0.04)
fig.savefig('%s/ch4_EPcombined.pdf' %(plotpath), dpi=300,bbox_inches='tight',pad_inches=0.05)

# #####################################################################
# plot COP  data

# fig,ax = plt.subplots(1,figsize=(7,3))

# ax.set_title('COP comparison')

# COP = pd.DataFrame()

# COP['UCLEM'] = uc_flux['acflux']/uc_flux['elecdemand']
# COP['EnergyPlus'] = abs(ep_flux['acflux'])/ep_flux['elecdemand']

# COP.plot(ax=ax,marker='o',markersize=2,ls='None')
# ax.set_ylim(bottom=0)

# fig.savefig('%s/COP_%s.png' %(plotpath,fullname), dpi=300,bbox_inches='tight',pad_inches=0.05)
