#!/Users/MatLipson/miniconda2/envs/py3x/bin/python

__title__ = "Wrangle netCDF for UCLEM_SCM"
__author__ = "Mathew Lipson"
__version__ = "180618"
__email__ = "m.lipson@unsw.edu.au"

''' This script plots distribution and QQ plots of meterological variables and energy use
for the period 2000-2009 for SCM coupled, offline and observations for the evaluation in
Chapter 5 of Model Development for Urban Climates'''

import xarray as xr
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import matplotlib.patheffects as path_effects
import numpy as np
import pandas as pd
import os
from scipy import stats

def read_BOMobs(stationid):

    obsrnd = pd.read_csv('%s/BOMdata/IDCJAC0009_%s_1800_Data.csv' %(datapath,stationid), parse_dates=[[2,3,4]],index_col=0)
    obsmax = pd.read_csv('%s/BOMdata/IDCJAC0010_%s_1800_Data.csv' %(datapath,stationid), parse_dates=[[2,3,4]],index_col=0)
    obsmin = pd.read_csv('%s/BOMdata/IDCJAC0011_%s_1800_Data.csv' %(datapath,stationid), parse_dates=[[2,3,4]],index_col=0)

    obs = pd.DataFrame()
    obs['t2max'] = obsmax.loc['2000':,'Maximum temperature (Degree C)']
    obs['t2min'] = obsmin.loc['2000':,'Minimum temperature (Degree C)']
    obs['rain']  = obsrnd.loc['2000':,'Rainfall amount (millimetres)']

    return obs

def import_siteobs():
    ''' use pandas to read csv and return dataframe '''
    obs=pd.read_csv('%s/observation_preston.csv' %(obspath), 
        na_values=['nan'],engine='c')
    alpha04=pd.read_csv('%s/alpha04_preston.dat' %(obspath), 
        na_values=['nan'], delim_whitespace=True, skiprows=[0,1,2,3,4,6], engine='c')
    return obs, alpha04

def organise_siteobs(obs,alpha04):
    ''' Use PILPS-urban ('''

    # use alpha04 to remove gapfilled data
    obs.loc[alpha04.QSWdown!=1,['NetRad','SWup','LWup','QHup','QEup']] = np.nan
    obs.loc[alpha04.QLWdown!=1,['NetRad','SWup','LWup','QHup','QEup']] = np.nan
    obs.loc[alpha04.QWind_N!=1,['NetRad','SWup','LWup','QHup','QEup']] = np.nan
    obs.loc[alpha04.QWind_E!=1,['NetRad','SWup','LWup','QHup','QEup']] = np.nan

    #re-index P and O to pandas time from 12 August 2003 (day 224)
    alpha04.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')
    obs.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')
    # convert column time to decimal format
    obs['Time']=obs.index.hour + obs.index.minute/60
    #remove observed SWup at night (spurious) where SWdown forcing = 0
    obs.loc[obs.SWdown == 0, 'SWup'] = np.nan
    # create a list of unique index values where NaN appears in any flux except SWup and replace row with NaN
    # dropindx = np.unique(np.where(pd.isnull(obs[['NetRad','LWup','QHup','QEup']]))[0])
    # obs.loc[obs.index[dropindx],['NetRad','SWup','LWup','QHup','QEup']] = np.nan

    # select one year
    obs = obs['2003-11-27':'2004-11-26']

    return obs,alpha04

def import_offline(obs,expname):
    ''' import offline sim results, including energy output '''
    
    # read experiment flux data
    sim=pd.read_csv('%s/../offline/%s.dat' %(datapath,expname), na_values=['nan'], delim_whitespace=True, comment='#',engine='c')
    simQF=pd.read_csv('%s/../offline/%s.nrg' %(datapath,expname), delim_whitespace=True, skiprows=[0], header=None, 
        usecols=[8,9,10,11,12,13], names=['Heating','Cooling','IntGains','Traffic','IntAirTemp','ScrnTemp'],engine='c')
    sim.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')
    sim.Time=sim.index.hour + sim.index.minute/60
    
    assert(sim.columns[0] == 'Year')
    # place all NaN in obs within corresponding simulated data
    sim = sim[~obs.isnull()] 
    simQF.index = sim.index

    simQF['ElecGas']=simQF['Heating']+simQF['Cooling']+simQF['IntGains']
    simQF['NetQF']=simQF['ElecGas']+simQF['Traffic']

    sim['NetQF'] = simQF['NetQF']
    sim['ElecGas'] = simQF['ElecGas']
    sim['dQS']   = sim['NetRad']+sim['NetQF']-sim['QHup']-sim['QEup']

    return sim, simQF

def perkins_skill(a,b,bins=100):

        amax,amin = a.max(),a.min()
        bmax,bmin = b.max(),b.min()

        # histogram with min/max extends from both data
        acount, aedge = np.histogram(a,bins=bins,range=(min(amin,bmin),max(amax,bmax)))
        aprob = acount/acount.sum()

        bcount, bedge = np.histogram(b,bins=bins,range=(min(amin,bmin),max(amax,bmax)))
        bprob = bcount/bcount.sum()

        skill = np.minimum(aprob,bprob).sum()

        print(skill.round(4))
        return skill

# ########################################################## 

def plot_kde(data,evalpoints,ax,label,colour='red'):
    ''' plots kernal distribution estimate'''
    data.plot(kind='kde',ind=evalpoints,bw_method=0.15,ax=ax,color=colour,label=label,legend=True)
    return ax

def plot_hist(data,datarange,bins,ax,colour='red'):
    ''' plots histogram'''
    data.plot(kind='hist',range=datarange,density=True,bins=bins,ax=ax,color=colour,alpha=0.3,legend=False)#range=datarange,
    return ax

def plot_hist_kde(data,evalpoints,datarange,bins,ax,label,colour='red'):
    ''' combined histogram and kde plot'''
    plot_kde(data,evalpoints,ax,label,colour)
    plot_hist(data,datarange,bins,ax,colour)
    return ax

def plot_qq(obs,data,ax,colour='red',ha='right',va='bottom'):
    x = np.arange(0.00,1.01,0.01).round(2)
    obs_q  = obs.quantile(x)
    data_q = data.quantile(x)

    # qq simulation
    ax.scatter(obs_q,data_q, c=colour, s=4, linewidths=0.001, zorder=10)
    # qq simulation key points
    ax.scatter([obs_q[0.0],obs_q[0.1],obs_q[0.5],obs_q[0.9],obs_q[1.0]],
               [data_q[0.0],data_q[0.1],data_q[0.5],data_q[0.9],data_q[1.0]], 
               color=colour, s=10, marker='x', facecolor=None, linewidths=0.001, zorder=10, alpha=0.5)

    # qq observation
    ax.scatter(obs_q,obs_q, c='black', s=4, linewidths=0.001, zorder=1)
    # qq observation key points
    ax.scatter([obs_q[0.0],obs_q[0.1],obs_q[0.5],obs_q[0.9],obs_q[1.0]],
               [obs_q[0.0],obs_q[0.1],obs_q[0.5],obs_q[0.9],obs_q[1.0]], 
               color='black', s=10, marker='x', facecolor=None, linewidths=0.001, zorder=1, alpha=0.35)
    
    # annotation (sims only)
    tnn = ax.text(obs_q[0.0], data_q[0.0], ' min ',fontsize=5, ha=ha,va=va,color=colour)
    t10 = ax.text(obs_q[0.1], data_q[0.1], ' 10% ',fontsize=5, ha=ha,va=va,color=colour)
    t50 = ax.text(obs_q[0.5], data_q[0.5], ' 50% ',fontsize=5, ha=ha,va=va,color=colour)
    t90 = ax.text(obs_q[0.9], data_q[0.9], ' 90% ',fontsize=5, ha=ha,va=va,color=colour)
    txx = ax.text(obs_q[1.0], data_q[1.0], ' max ',fontsize=5, ha=ha,va='center',color=colour)

    tnn.set_path_effects([path_effects.Stroke( linewidth=0.5, foreground='white'),path_effects.Normal()])
    t10.set_path_effects([path_effects.Stroke(linewidth=0.5, foreground='white'),path_effects.Normal()])
    t50.set_path_effects([path_effects.Stroke(linewidth=0.5, foreground='white'),path_effects.Normal()]) 
    t90.set_path_effects([path_effects.Stroke(linewidth=0.5, foreground='white'),path_effects.Normal()]) 
    txx.set_path_effects([path_effects.Stroke(linewidth=0.5, foreground='white'),path_effects.Normal()]) 

    # identity line
    imin = min(obs_q.min(),data_q.min())
    imax = max(obs_q.max(),data_q.max())
    # extend range by 1%
    imin = imin-0.01*(abs(imax)-abs(imin))
    imax = imax+0.01*(abs(imax)-abs(imin))

    ax.plot([imin,imax],[imin,imax], color='0.75', lw=0.5, zorder=-101)
    # ax.plot([obs_q.min(),obs_q.max()],[obs_q.min(),obs_q.max()], color='k', lw=1.0, zorder=5)

    # ax.set_xlim([imin,imax])
    # ax.set_ylim([imin,imax])

    ax.grid(lw=0.3,color='0.8',zorder=-100)

    return ax,imin,imax

if __name__ == "__main__":

    plt.rcParams['legend.fontsize'] = 'x-small'
    plt.rcParams['legend.loc'] = 'upper right'

    pd.set_option('display.width', 150)
    scriptname = "A5_SCMresults.py"
    oshome=os.getenv('HOME')

    # User inputs
    projpath = '.'
    datapath = '%s/output/SCM' %projpath
    plotpath = '%s/figures/ch5' %projpath
    obspath  = '%s/ownCloud/phd/03-Code/phd_python/anthro/data' %oshome

    exp = 'control'
    decade = '2000_2099'
    # model = 'MPI-ESM-LR'
    # model = 'GFDL-CM3'
    model = 'ACCESS1-0'
    # decade = '1990_1999'
    file = '%s_rcp85_time_scm_CCAM.%s_%s.nc' %(model,decade,exp)
    ffile ='%s_rcp85_time_scm_CCAM.2000_2009_forcing.nc' %(model)
    bins = 50
    QQ = True

    start, end = '2003-11-27','2004-11-26'

    # add degree symbol to ticks
    degtick = mtick.StrMethodFormatter('{x:,.0f}°C')

    # load metforcing data
    forcing_ds = xr.open_dataset('%s/%s' %(datapath,ffile))

    print('File: %s' %file)

    ds = xr.open_dataset('%s/%s' %(datapath,file))
    # construct net radiation
    ds['rnet'] = ds.qdw + ds.ldw - ds.qup - ds.lup
    # remove shortwave at night
    ds.qdw[ds.qdw==0] = np.nan
    ds.qup[ds.qdw.isnull()] = np.nan

    # compare CONTROL with OBSERVED at PRESTON
    # fluxlist = ['NetRad','qdw','ldw','qup','lup','shf','lhf','g','qf','qf_bem','qf_heat','qf_cool','t40m','q40m','rh40m','u40m','v40m','rain','psurf']
    fluxlist = ['rnet','qdw','ldw','qup','lup','shf','lhf','g','qf_bem','qf_heat','qf_cool','t2m','t40m','q40m','u40m','v40m','rain','psurf']

    coupled = ds[fluxlist].to_dataframe()
    forcing = forcing_ds[['rnet','qdw','ldw','shf','lhf']].sel(lat=-37.5,lon=145).to_dataframe()

    # # # remove 2 spurious latent heat flux timesteps from simulation
    # coupled.loc[coupled.lhf>750,'lhf'] = np.nan
    # coupled.loc[coupled.g<-500,'g'] = np.nan

    # coupled.g[coupled.index == coupled.lhf.idxmax()] == np.nan
    # coupled.lhf[coupled.index == coupled.lhf.idxmax()] == np.nan

    # correct off-hour values and localize to Melbourne from utc
    coupled.index = coupled.index.round('30Min').tz_localize(None) + pd.offsets.Hour(10)
    forcing.index = forcing.index.round('30Min').tz_localize(None) + pd.offsets.Hour(10)

    # limit coupled values to 2000-2009
    coupled = coupled['2000':'2009']
    forcing = forcing['2000':'2009']
    coupled = coupled.assign(t40m = coupled.t40m - 273.15,
                             t2m = coupled.t2m - 273.15,
                             psurf = coupled.psurf/100)

    # calculate daily Tmin and Tmax in Celcius
    # coupledday = pd.DataFrame()
    # coupledday['t2max'] = coupled['t2m'].resample('1D').max() - 273.15
    # coupledday['t2min'] = coupled['t2m'].resample('1D').min() - 273.15
    # coupledday['t40max'] = coupled['t40m'].resample('1D').max() - 273.15
    # coupledday['t40min'] = coupled['t40m'].resample('1D').min() - 273.15


    # ##########################################################

    # BOM observations

    # create dictionary of Tmax/Tmin from BOM records
    stations = ['Melb','Viewbank','Bundoora']
    sids     = ['086071','086068','086351']

    BOMobs = {}
    for station,sid in zip(stations,sids):
        BOMobs[station] = read_BOMobs(sid)
        BOMobs[station].index.tz_localize('Australia/Melbourne')
        # print('%s \n%s' %(station,BOMobs[station].mean()))

    # ##########################################################
    # preston observations

    # prestonQF = pd.read_csv('%s/output/obsQF.csv' %projpath, index_col=[0],parse_dates=['DateTime'])
    prestonQF = pd.read_csv('%s/inputs/Preston_obsQF.csv' %projpath,index_col=[0])
    prestonQF.index = pd.date_range(start='20000101-0030',periods=308255,freq='30Min')
    prestonQF = prestonQF.loc['2000':'2009']

    preston, alpha04 = import_siteobs()
    preston, alpha04 = organise_siteobs(preston,alpha04)
    preston['NetQF'] = prestonQF['NetQF']
    preston['ElecGas'] = prestonQF['ElecGas']
    #Calculate residual heat storage from NetRad + QF = QH + QE + dQS
    preston['dQS'] = preston['NetRad']+preston['NetQF']-preston['QHup']-preston['QEup']
    # remove shortwave at night
    preston.loc[preston.SWdown==0,'SWdown'] = np.nan
    preston.loc[preston.SWdown.isna(),'SWup'] = np.nan

    # ##########################################################

    # # chose year of obs data
    preston = preston[start:end]
    # drop leap day
    preston.drop(preston['2004-02-29'].index,inplace=True)

    # set decimal time of year
    coupled['Dectime'] = coupled['Dectime']=coupled.index.dayofyear + coupled.index.hour/24 + coupled.index.minute/(60*24)
    preston['Dectime'] = preston['Dectime']=preston.index.dayofyear + preston.index.hour/24 + preston.index.minute/(60*24)

    ncfluxes = ['rnet','qdw','ldw','qup','lup','shf','lhf','g','qf','qf_bem']
    obsfluxes= ['NetRad','SWdown','LWdown','SWup','LWup','QHup','QEup','dQS','NetQF','ElecGas']
    fluxnames = ['net radiation','shortwave down','longwave down','shortwave up','longwave up','sensible heat',
                    'latent heat','storage','anthropogenic','electricity and gas']

    # dectime with na values in obs
    for ncflux,fluxname,obsflux in zip(ncfluxes,fluxnames,obsfluxes):
        decna = preston.where(preston[obsflux].isna())['Dectime'].dropna().values
        coupled.loc[coupled.Dectime.isin(decna),ncflux] = np.nan

    # ##########################################################

    # collect tower observations

    towerobs = alpha04.loc['2003-11-27':'2004-11-26',['Wind_N', 'Wind_E', 'Psurf', 'Tair', 'Qair', 'Rainf']]
    # convert tower obs from mm/s to mm/day
    towerobs = towerobs.assign(Rainf = towerobs[['Rainf']]*60*60*24, 
                               Tair = towerobs[['Tair']]-273.15,
                               Psurf = towerobs[['Psurf']]/100)

    towerobs['rain_day'] = towerobs.Rainf.resample('1D').mean()
    coupled['rain_day'] = coupled['rain'].resample('1D').mean()

    # coupledday['rain']=coupled['rain'].resample('1D').mean()

    obsday = pd.DataFrame()
    obsday['rain'] = towerobs['Rainf'].resample('1D').mean()
    obsday['t40min'] = towerobs['Tair'].resample('1D').min() - 273.15
    obsday['t40max'] = towerobs['Tair'].resample('1D').max() - 273.15

    coupled['wind40']= np.sqrt(coupled['u40m']**2 + coupled['v40m']**2)

    coupled['qf_base'] = coupled['qf_bem'] - coupled['qf_heat'] - coupled['qf_cool']

    coupled['qf_elec'] = 0.76*coupled['qf_base'] + 0.14*coupled['qf_heat'] + 1.00*coupled['qf_cool']
    coupled['qf_gas']  = 0.24*coupled['qf_base'] + 0.86*coupled['qf_heat'] + 0.00*coupled['qf_cool']

    # check conservation of building energy partitioning
    assert (abs(coupled['qf_elec'] + coupled['qf_gas'] - coupled['qf_bem']).max() < 1E-3),'elec/gas partitioning not conserved!'

    towerobs['wind40']= np.sqrt(towerobs['Wind_N']**2 + towerobs['Wind_E']**2)

    offline,offlineQF = import_offline(preston,'ch5_control_offline')
    offline = offline[start:end]

    ##############################################################
    # hot and cold periods

    hot = 25
    cld = 15

    coupled['hot'] = coupled.loc[coupled.t40m > hot].qf_bem
    coupled['cld'] = coupled.loc[coupled.t40m < cld].qf_bem

    offline['hot'] = offline.loc[towerobs.Tair > hot].ElecGas
    offline['cld'] = offline.loc[towerobs.Tair < cld].ElecGas

    preston['hot'] = preston.loc[towerobs.Tair > hot].ElecGas
    preston['cld'] = preston.loc[towerobs.Tair < cld].ElecGas

    # ##################################################################################
    # #                              plots                                             #
    # ##################################################################################

    plt.close('all')

    ##################################################################################
    ### COMPARE TOWER OBS AND CONTROL SIMULATION DIST AND QQ
    ##################################################################################


    ##################################################################################
    # Energy Balance

    ncfluxes = ['qdw', 'ldw', 'qup', 'lup', 'rnet', 'shf', 'lhf', 'g']
    obsfluxes= ['SWdown','LWdown','SWup','LWup','NetRad','QHup','QEup','dQS']
    fluxnames = ['shortwave down',
                 'longwave down',
                 'shortwave up',
                 'longwave up',
                 'net radiation',
                 'sensible heat',
                 'latent heat',
                 'storage heat']
    # forcing fluxes
    # ncfluxes = ['rnet','qdw', 'ldw', 'shf', 'lhf']
    # obsfluxes= ['NetRad','SWdown','LWdown','QHup','QEup']
    # fluxnames = ['net radiation',
    #              'shortwave down',
    #              'longwave down',
    #              'sensible heat',
    #              'latent heat']
    figids = ['a)','b)','c)','d)','a)','b)','c)','d)']

    for ncflux,fluxname,obsflux,figid in zip(ncfluxes,fluxnames,obsfluxes,figids):

        print('plotting %s (%s)' %(ncflux,fluxname))

        fig, (ax1,ax2) = plt.subplots(nrows=1, ncols=2, figsize=(8,2.8), gridspec_kw = {'width_ratios':[1.65, 1]})

        # account for latent heat outliers making distribution difficult to read
        if obsflux=='QEup':
            gmin = -100
            gmax = 300
        else:
            gmin = min(coupled[ncflux].min(),offline[obsflux].min(),preston[obsflux].min())
            gmax = max(coupled[ncflux].max(),offline[obsflux].max(),preston[obsflux].max())

        # gmin = -200
        # gmax = 800
        evalpoints=np.linspace(gmin,gmax,bins*2)
        datarange = (gmin,gmax)

        ax1.set_title('%s distribution' %(fluxname.capitalize()), fontsize=9)
        ax1.text(-0.045,1.035,figid,transform=ax1.transAxes,fontsize=9,ha='left',color='black')

        # coupled
        plot_hist_kde(data=coupled[ncflux].dropna(), label='Coupled (n=%s)' %(len(coupled[ncflux].dropna())), 
            colour='royalblue', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)
        ax1.text(0.02,0.95,'Coupled skill: %.2f' %perkins_skill(coupled[ncflux].dropna(),preston[obsflux].dropna()),
             transform=ax1.transAxes,fontsize=7,ha='left',color='royalblue')
        # # forcing
        # plot_hist_kde(data=forcing[ncflux].dropna(), label='forcing (n=%s)' %(len(forcing[ncflux].dropna())), 
        #     colour='gold', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)
        # offline
        if obsflux not in ['SWdown','LWdown']:
            plot_hist_kde(data=offline[obsflux].dropna(), label='Offline (n=%s)' %(len(preston[obsflux].dropna())), 
                colour='red', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)
        ax1.text(0.02,0.90,'Offline skill: %.2f' %perkins_skill(offline[obsflux].dropna(),preston[obsflux].dropna()),
             transform=ax1.transAxes,fontsize=7,ha='left',color='red')
        # observation
        plot_hist_kde(data=preston[obsflux].dropna(), label='Observed (n=%s)' %(len(preston[obsflux].dropna())),
            colour='black', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)

        ax1.axes.get_yaxis().set_ticks([])
        ax1.margins(y=0.15)

        ########### QQ plots #################

        ax2.set_title('%s QQ (percentiles)' %(fluxname.capitalize()), fontsize=9)
        
        ax2,imin,imax = plot_qq(preston[obsflux],coupled[ncflux],ax2,'royalblue')
        ## forcing flux ['rnet','qdw', 'ldw', 'shf', 'lhf']
        # ax2,imin,imax = plot_qq(preston[obsflux],forcing[ncflux],ax2,'gold')
        if obsflux not in ['SWdown','LWdown']:
            ax2,_,_ = plot_qq(preston[obsflux],offline[obsflux],ax2,'red','left','top')

        ax1.set_ylabel('Frequency',fontsize=9)
        ax2.set_ylabel('Simulation $\mathrm{[W/m^2]}$', fontsize=9)

        if obsflux in ['LWup','dQS']:    
            ax1.set_xlabel('Heat flux $\mathrm{[W/m^2]}$', fontsize=9)
            ax2.set_xlabel('Observation $\mathrm{[W/m^2]}$', fontsize=9)

        ax2.yaxis.tick_right()

        handles, labels = ax2.get_legend_handles_labels()
        # remove additional observed handle
        handles.pop(1)
        labels.pop(1)

        labels = ['Coupled','Offline','Observed']
        ax2.legend(labels=labels,handles=handles,loc='upper left',handletextpad=-0.2)

        fig.subplots_adjust(wspace=0.09)

        # set QQ limits and number of ticks to be equal
        ax2.set_xlim([imin,imax])
        ax2.set_ylim([imin,imax])
        # ax2.set_xlim([gmin,gmax])
        # ax2.set_ylim([gmin,gmax])
        plt.locator_params(nbins=6)

        fig.savefig('%s/ch5_%s_%s.pdf' %(plotpath,ncflux,exp), dpi=300,bbox_inches='tight',pad_inches=0.05)

    ##################################################################################
    ### temperature, rain, wind, pressure

    # ncfluxes = ['wind40', 't40m','t40min','t40max','rain','psurf']
    ncfluxes = ['t40m','q40m','wind40', 'psurf','rain_day']
    obsfluxes = ['Tair','Qair','wind40', 'Psurf','rain_day']
    fluxnames = ['air temperature at 40m','specific humidity at 40m','wind speed at 40m','air pressure at 40m','precipitation']
    xlabels = ['Temperature [°C]','Specific humidity [kg/kg]','Wind speed [m/s]','Pressure [hPa]','Rainfall rate [mm/day]']
    figids = ['a)','b)','c)','d)','x)']

    for ncflux,obsflux,fluxname,xlabel,figid in zip(ncfluxes,obsfluxes,fluxnames,xlabels,figids):

        print('plotting %s (%s)' %(ncflux,fluxname))

        fig, (ax1,ax2) = plt.subplots(nrows=1, ncols=2, figsize=(8,2.8), gridspec_kw = {'width_ratios':[1.65, 1]})

        ## account for latent heat outliers making distribution difficult to read
        gmin = min(coupled[ncflux].min(),towerobs[obsflux].min())
        gmax = max(coupled[ncflux].max(),towerobs[obsflux].max())

        evalpoints=np.linspace(gmin,gmax,bins*2)
        datarange = (gmin,gmax)

        ax1.set_title('%s distribution' %(fluxname.capitalize()), fontsize=9)
        ax1.text(-0.045,1.035,figid,transform=ax1.transAxes,fontsize=9,ha='left',color='black')

        # coupled
        plot_hist_kde(data=coupled[ncflux], label='Coupled (n=%s)' %(len(coupled[ncflux].dropna())), 
            colour='royalblue', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)
        ax1.text(0.02,0.95,'Coupled skill: %.2f' %perkins_skill(coupled[ncflux].dropna(),towerobs[obsflux].dropna()),
             transform=ax1.transAxes,fontsize=7,ha='left',color='royalblue')
        # observation
        plot_hist_kde(data=towerobs[obsflux], label='Observed (n=%s)' %(len(towerobs[obsflux].dropna())),
            colour='black', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)

        ax1.axes.get_yaxis().set_ticks([])
        ax1.margins(y=0.15)

        ########### QQ plots #################

        ax2.set_title('%s QQ (percentiles)' %(fluxname.capitalize()), fontsize=9)
        
        ax2,imin,imax = plot_qq(towerobs[obsflux],coupled[ncflux],ax2,'royalblue')

        ax1.set_ylabel('Frequency',fontsize=9)
        ax2.set_ylabel('Simulation %s' %xlabel.split()[-1], fontsize=9)

        ax1.set_xlabel(xlabel, fontsize=9)
        ax2.set_xlabel('Observation %s' %xlabel.split()[-1], fontsize=9)

        ax2.yaxis.tick_right()

        handles, labels = ax2.get_legend_handles_labels()

        labels = ['Coupled','Observed']
        ax2.legend(labels=labels,handles=handles,loc='upper left',handletextpad=-0.2)

        fig.subplots_adjust(wspace=0.09)

        if ncflux == 'rain_day':
            ax1.text(0.02,0.90,'Coupled rainfall yearly total: %.0f mm' %(coupled['rain_day'].sum()/10), 
                transform=ax1.transAxes,fontsize=7,ha='left',color='royalblue')
            ax1.text(0.02,0.85,'Observed rainfall yearly total: %.0f mm' %(towerobs['rain_day'].sum()),
                transform=ax1.transAxes,fontsize=7,ha='left',color='black')

        # set QQ limits and number of ticks to be equal
        ax2.set_xlim([imin,imax])
        ax2.set_ylim([imin,imax])
        plt.locator_params(nbins=6)

        fig.savefig('%s/ch5_%s_%s.pdf' %(plotpath,ncflux,exp), dpi=300,bbox_inches='tight',pad_inches=0.05)
        
    ##################################################################################
    # Building energy use

    ncfluxes = ['qf_bem','hot','cld']
    obsfluxes= ['ElecGas','hot','cld']
    fluxnames = ['Building energy use (2003-2004)','hot periods (2003-2004)','cold periods (2003-2004)']
    figids = ['a)','c)','d)']

    for ncflux,fluxname,obsflux,figid in zip(ncfluxes,fluxnames,obsfluxes,figids):

        print('plotting %s (%s)' %(ncflux,fluxname))

        fig, (ax1,ax2) = plt.subplots(nrows=1, ncols=2, figsize=(8,2.8), gridspec_kw = {'width_ratios':[1.65, 1]})

        gmin = min(coupled[ncflux].min(),offline[obsflux].min(),preston[obsflux].min())
        gmax = max(coupled[ncflux].max(),offline[obsflux].max(),preston[obsflux].max())

        evalpoints=np.linspace(gmin,gmax,bins*2)
        datarange = (gmin,gmax)

        ax1.set_title('%s distribution' %(fluxname.capitalize()), fontsize=9)
        ax1.text(-0.045,1.035,figid,transform=ax1.transAxes,fontsize=9,ha='left',color='black')

        # coupled
        plot_hist_kde(data=coupled[ncflux], label='Coupled (n=%s)' %(len(coupled[ncflux].dropna())), 
            colour='royalblue', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)
        ax1.text(0.02,0.95,'Coupled skill: %.2f' %perkins_skill(coupled[ncflux].dropna(),preston[obsflux].dropna()),
             transform=ax1.transAxes,fontsize=7,ha='left',color='royalblue')
        # offline
        if obsflux not in ['SWdown','LWdown']:
            plot_hist_kde(data=offline[obsflux], label='Offline (n=%s)' %(len(preston[obsflux].dropna())), 
                colour='red', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)
        ax1.text(0.02,0.90,'Offline skill: %.2f' %perkins_skill(offline[obsflux].dropna(),preston[obsflux].dropna()),
             transform=ax1.transAxes,fontsize=7,ha='left',color='red')
        # observation
        plot_hist_kde(data=preston[obsflux], label='Observed (n=%s)' %(len(preston[obsflux].dropna())),
            colour='black', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)

        ax1.axes.get_yaxis().set_ticks([])
        ax1.margins(y=0.15)

        ########### QQ plots #################

        ax2.set_title('%s QQ (percentiles)' %(fluxname.capitalize()), fontsize=9)
        
        ax2,imin,imax = plot_qq(preston[obsflux],coupled[ncflux],ax2,'royalblue')
        if obsflux not in ['SWdown','LWdown']:
            ax2,_,_ = plot_qq(preston[obsflux],offline[obsflux],ax2,'red','left','top')

        ax1.set_ylabel('Frequency',fontsize=9)
        ax2.set_ylabel('Simulation $\mathrm{[W/m^2]}$', fontsize=9)

        if obsflux in ['cld']:
            ax1.set_xlabel('Heat flux $\mathrm{[W/m^2]}$', fontsize=9)
            ax2.set_xlabel('Observation $\mathrm{[W/m^2]}$', fontsize=9)

        ax2.yaxis.tick_right()

        handles, labels = ax2.get_legend_handles_labels()
        # remove additional observed handle
        handles.pop(1)
        labels.pop(1)

        labels = ['Coupled','Offline','Observed']
        ax2.legend(labels=labels,handles=handles,loc='upper left',handletextpad=-0.2)

        fig.subplots_adjust(wspace=0.09)

        # set QQ limits and number of ticks to be equal
        ax2.set_xlim([imin,imax])
        ax2.set_ylim([imin,imax])
        plt.locator_params(nbins=6)

        fig.savefig('%s/ch5_%s_%s.pdf' %(plotpath,ncflux,exp), dpi=300,bbox_inches='tight',pad_inches=0.05)

    ##################################################################################
    # Building energy use for 2000-2010

    ncfluxes = ['qf_bem']
    obsfluxes= ['ElecGas']
    fluxnames = ['Building energy use (2000-2009)']
    figids = ['b)']

    for ncflux,fluxname,obsflux,figid in zip(ncfluxes,fluxnames,obsfluxes,figids):

        print('plotting %s (%s)' %(ncflux,fluxname))

        fig, (ax1,ax2) = plt.subplots(nrows=1, ncols=2, figsize=(8,2.8), gridspec_kw = {'width_ratios':[1.65, 1]})

        gmin = min(coupled[ncflux].min(),offline[obsflux].min(),prestonQF[obsflux].min())
        gmax = max(coupled[ncflux].max(),offline[obsflux].max(),prestonQF[obsflux].max())

        evalpoints=np.linspace(gmin,gmax,bins*2)
        datarange = (gmin,gmax)

        ax1.set_title('%s distribution' %(fluxname.capitalize()), fontsize=9)
        ax1.text(-0.045,1.035,figid,transform=ax1.transAxes,fontsize=9,ha='left',color='black')

        # coupled
        plot_hist_kde(data=coupled[ncflux], label='Coupled (n=%s)' %(len(coupled[ncflux].dropna())), 
            colour='royalblue', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)
        ax1.text(0.02,0.95,'Coupled skill: %.2f' %perkins_skill(coupled[ncflux].dropna(),prestonQF[obsflux].dropna()),
             transform=ax1.transAxes,fontsize=7,ha='left',color='royalblue')
        # offline
        if obsflux not in ['SWdown','LWdown']:
            plot_hist_kde(data=offline[obsflux], label='Offline (n=%s)' %(len(prestonQF[obsflux].dropna())), 
                colour='red', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)
        ax1.text(0.02,0.90,'Offline skill: %.2f' %perkins_skill(offline[obsflux].dropna(),prestonQF[obsflux].dropna()),
             transform=ax1.transAxes,fontsize=7,ha='left',color='red')
        # observation
        plot_hist_kde(data=prestonQF[obsflux], label='Observed (n=%s)' %(len(prestonQF[obsflux].dropna())),
            colour='black', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)

        ax1.axes.get_yaxis().set_ticks([])
        ax1.margins(y=0.15)

        ########### QQ plots #################

        ax2.set_title('%s QQ' %(fluxname.capitalize()), fontsize=9)
        
        ax2,imin,imax = plot_qq(prestonQF[obsflux],coupled[ncflux],ax2,'royalblue')
        if obsflux not in ['SWdown','LWdown']:
            ax2,_,_ = plot_qq(prestonQF[obsflux],offline[obsflux],ax2,'red','left','top')


        ax1.set_ylabel('Frequency',fontsize=9)
        ax2.set_ylabel('Simulation $\mathrm{[W/m^2]}$', fontsize=9)

        # ax1.set_xlabel('Heat flux $\mathrm{[W/m^2]}$', fontsize=9)
        # ax2.set_xlabel('Observation $\mathrm{[W/m^2]}$', fontsize=9)

        ax2.yaxis.tick_right()

        handles, labels = ax2.get_legend_handles_labels()

        labels = ['Coupled','Offline','Observed']
        ax2.legend(labels=labels,handles=handles,loc='upper left',handletextpad=-0.2)

        fig.subplots_adjust(wspace=0.09)

        # set QQ limits and number of ticks to be equal
        ax2.set_xlim([imin,imax])
        ax2.set_ylim([imin,imax])
        plt.locator_params(nbins=6)

        fig.savefig('%s/ch5_%s_decade_%s.pdf' %(plotpath,ncflux,exp), dpi=300,bbox_inches='tight',pad_inches=0.05)

    ##################################################################################
    # Building energy use for 2000-2010

    ncfluxes = ['qf_gas','qf_elec']
    obsfluxes= ['Gas','Electricity']
    fluxnames = ['gas use (decade)','electricity use (2000-2009)']
    figids = ['b)','a)']

    for ncflux,fluxname,obsflux,figid in zip(ncfluxes,fluxnames,obsfluxes,figids):

        print('plotting %s (%s)' %(ncflux,fluxname))

        fig, (ax1,ax2) = plt.subplots(nrows=1, ncols=2, figsize=(8,2.8), gridspec_kw = {'width_ratios':[1.65, 1]})

        gmin = min(coupled[ncflux].min(),prestonQF[obsflux].min())
        gmax = max(coupled[ncflux].max(),prestonQF[obsflux].max())

        evalpoints=np.linspace(gmin,gmax,bins*2)
        datarange = (gmin,gmax)

        ax1.set_title('%s distribution' %(fluxname.capitalize()), fontsize=9)
        ax1.text(-0.045,1.035,figid,transform=ax1.transAxes,fontsize=9,ha='left',color='black')

        # coupled
        plot_hist_kde(data=coupled[ncflux], label='Coupled (n=%s)' %(len(coupled[ncflux].dropna())), 
            colour='royalblue', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)
        ax1.text(0.02,0.95,'Coupled skill: %.2f' %perkins_skill(coupled[ncflux].dropna(),prestonQF[obsflux].dropna()),
             transform=ax1.transAxes,fontsize=7,ha='left',color='royalblue')

        # observation
        plot_hist_kde(data=prestonQF[obsflux], label='Observed (n=%s)' %(len(prestonQF[obsflux].dropna())),
            colour='black', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)

        ax1.axes.get_yaxis().set_ticks([])
        ax1.margins(y=0.15)

        ########### QQ plots #################

        ax2.set_title('%s QQ' %(fluxname.capitalize()), fontsize=9)
        
        ax2,imin,imax = plot_qq(prestonQF[obsflux],coupled[ncflux],ax2,'royalblue')


        ax1.set_ylabel('Frequency',fontsize=9)
        ax2.set_ylabel('Simulation $\mathrm{[W/m^2]}$', fontsize=9)

        # ax1.set_xlabel('Heat flux $\mathrm{[W/m^2]}$', fontsize=9)
        # ax2.set_xlabel('Observation $\mathrm{[W/m^2]}$', fontsize=9)

        ax2.yaxis.tick_right()

        handles, labels = ax2.get_legend_handles_labels()

        labels = ['Coupled','Observed']
        ax2.legend(labels=labels,handles=handles,loc='upper left',handletextpad=-0.2)

        if obsflux in ['Gas']:
            ax1.set_xlabel('Heat flux $\mathrm{[W/m^2]}$', fontsize=9)
            ax2.set_xlabel('Observation $\mathrm{[W/m^2]}$', fontsize=9)

        fig.subplots_adjust(wspace=0.09)

        # set QQ limits and number of ticks to be equal
        ax2.set_xlim([imin,imax])
        ax2.set_ylim([imin,imax])
        plt.locator_params(nbins=6)

        fig.savefig('%s/ch5_%s_decade_%s.pdf' %(plotpath,ncflux,exp), dpi=300,bbox_inches='tight',pad_inches=0.05)

    ####################################################################################################

    ### Scatter of electricity and gas

    ### NOT USED BECAUSE T2M of BOM (city or rural) and SIM (preston) WILL NOT BE REPRESENTING THE SAME THING

    def plot_ols(x, y,color,note,ax):
        '''function that plots scatter and OLS line from data x, y'''
        # ax.scatter(x,y,color=colour,alpha=0.5,label=label[0],marker='x')   # calculate and plot linear regression
        # calculate and plot linear regression
        slope, intercept, r, p, stderr = stats.linregress(x, y)
        ax.plot((x.min(),x.max()),(intercept+x.min()*slope,intercept+x.max()*slope),
            color=color, lw=2.0,
            label='%s trend: %.3f $\mathrm{[W/m^2/°C]}$' %(note,slope))
        return ax

    ############

    plt.close('all')

    obsmax = prestonQF[['Electricity']].resample('1D').max()
    obsmax['Gas'] = prestonQF[['Gas']].resample('1D').max()
    obsmax['t40max'] = towerobs['Tair'].resample('1D').max()
    obsmax = obsmax.dropna()
    obshot = obsmax[obsmax.t40max>hot]
    obscld = obsmax[obsmax.t40max<cld]

    simmax = coupled.loc[start:end,['qf_elec']].resample('1D').max()
    # simmax = coupled[['qf_elec']].resample('1D').max()
    simmax['qf_gas'] = coupled['qf_gas'].resample('1D').max()
    simmax['t40max'] = coupled[['t40m']].resample('1D').max()
    simmax = simmax.dropna()
    simhot = simmax[simmax.t40max>hot]
    simcld = simmax[simmax.t40max<cld]

    fig,(ax1,ax2) = plt.subplots(nrows=1,ncols=2,figsize=(8,3))

    # gas OLS
    ax1.scatter(x=obsmax['t40max'],y=obsmax['Gas'],color='black',alpha=0.5,label=None,marker='o',s=3)
    plot_ols(x=obscld['t40max'], y=obscld['Gas'],color='black',note='Observed gas',ax=ax1)

    ax1.scatter(x=simmax['t40max'],y=simmax['qf_gas'],color='SeaGreen',alpha=0.5,label=None,marker='o',s=3)
    plot_ols(x=simcld['t40max'], y=simcld['qf_gas'],color='SeaGreen',note='Simulated gas',ax=ax1)

    ax1.legend(loc='upper center')
    ax1.set_xticks([10,15,20,25,30,35,40])
    ax1.set_ylim(top=12)
    # ax1.axvline(x=cld,color='black',lw=0.5)
    # ax1.xaxis.set_major_formatter(degtick)
    ax1.set_title('Gas demand vs temperature',fontsize=9)
    ax1.set_xlabel('Max. daily 40m air temperature [°C]')
    ax1.set_ylabel('Max. daily\ngas demand $\mathrm{[W/m^2]}$')

    # elec OLS
    ax2.scatter(x=obsmax['t40max'],y=obsmax['Electricity'],color='black',alpha=0.5,label=None,marker='o',s=3)
    plot_ols(x=obshot['t40max'], y=obshot['Electricity'],color='black',note='Observed electricity',ax=ax2)

    ax2.scatter(x=simmax['t40max'],y=simmax['qf_elec'],color='DarkOrchid',alpha=0.5,label=None,marker='o',s=3)
    plot_ols(x=simhot['t40max'], y=simhot['qf_elec'],color='DarkOrchid',note='Simulated electricity',ax=ax2)

    ax2.legend(loc='upper center')
    ax2.yaxis.tick_right()
    ax2.set_xticks([10,15,20,25,30,35,40])
    ax2.set_yticks([3,4,5,6])
    ax2.set_ylim(top=6)
    # ax2.axvline(x=hot,color='black',lw=0.5)
    # ax2.xaxis.set_major_formatter(degtick)
    ax2.set_title('Electricity demand vs temperature',fontsize=9)
    ax2.set_xlabel('Max. daily 40m air temperature [°C]')
    ax2.set_ylabel('Max. daily\nelectricity demand $\mathrm{[W/m^2]}$')

    fig.savefig('%s/ch5_elecgas_scatter_%s.pdf' %(plotpath,exp), dpi=300,bbox_inches='tight',pad_inches=0.05)


# plt.close('all')

# obsmax = prestonQF[['Electricity']].resample('1D').max()
# obsmax['Gas'] = prestonQF[['Gas']].resample('1D').max()
# obsmax['t2max'] = BOMobs['Melb'].t2max
# obshot = obsmax[obsmax.t2max>31]
# obscld = obsmax[obsmax.t2max<cld]

# simmax = coupled[['qf_elec']].resample('1D').max()
# simmax['qf_gas'] = coupled['qf_gas'].resample('1D').max()
# simmax['t2max'] = coupled[['t2m']].resample('1D').max()
# simhot = simmax[simmax.t2max>31]
# simcld = simmax[simmax.t2max<cld]

# fig,ax = plt.subplots()
# ax.scatter(x=obsmax['t2max'],y=obsmax['Electricity'],color='black',alpha=0.5,label='obs1',marker='x',s=2)
# plot_ols(x=obshot['t2max'], y=obshot['Electricity'],color='black',note='obs2')

# ax.scatter(x=simmax['t2max'],y=simmax['qf_elec'],color='royalblue',alpha=0.5,label='sim1',marker='x',s=2)
# plot_ols(x=simhot['t2max'], y=simhot['qf_elec'],color='royalblue',note='sim2')

# ax.legend()
# plt.show()

# fig,ax = plt.subplots()

# ax.scatter(x=obsmax['t2max'],y=obsmax['Gas'],color='black',alpha=0.5,label='obs1',marker='x',s=2)
# plot_ols(x=obscld['t2max'], y=obscld['Gas'],color='black',note='obs2')

# ax.scatter(x=simmax['t2max'],y=simmax['qf_gas'],color='royalblue',alpha=0.5,label='sim1',marker='x',s=2)
# plot_ols(x=simcld['t2max'], y=simcld['qf_gas'],color='royalblue',note='sim2')

# ax.legend()
# plt.show()


# ##################################################################################
# ### temperature, rain, wind, pressure

# # ncfluxes = ['wind40', 't40m','t40min','t40max','rain','psurf']
# obsfluxes = ['t2max', 't2min','rain']
# fluxnames = ['daily max. air temp. at 2m','daily min. air temp. at 2m','daily precipitation']
# xlabels = ['Temperature [°C]','Temperature [°C]','Daily rainfall [mm/day]']

# plt.close('all')

# for obsflux,fluxname,xlabel in zip(obsfluxes,fluxnames,xlabels):

#     print('plotting %s (%s)' %(obsflux,fluxname))

#     fig, (ax1,ax2) = plt.subplots(nrows=1, ncols=2, figsize=(8,2.8), gridspec_kw = {'width_ratios':[1.65, 1]})

#     obs_decade = BOMobs['Melb'].loc['2000':'2009',obsflux]
#     obs_year   = BOMobs['Melb'].loc[start:end,obsflux]
#     # account for latent heat outliers making distribution difficult to read
#     gmin = min(obs_decade.min(),obs_year.min())
#     gmax = max(obs_decade.max(),obs_year.max())

#     evalpoints=np.linspace(gmin,gmax,bins*2)
#     datarange = (gmin,gmax)

#     ax1.set_title('%s distribution' %(fluxname.capitalize()), fontsize=9)

#     # coupled
#     plot_hist_kde(data=obs_decade, label='Obs. decade (n=%s)' %(len(obs_decade.dropna())), 
#         colour='royalblue', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)
#     ax1.text(0.02,0.95,'Obs. year skill: %.2f' %perkins_skill(obs_decade.dropna(),obs_year.dropna()),
#          transform=ax1.transAxes,fontsize=7,ha='left',color='royalblue')
#     # observation
#     plot_hist_kde(data=obs_year, label='Obs. year (n=%s)' %(len(obs_year.dropna())),
#         colour='black', evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax1)

#     ax1.axes.get_yaxis().set_ticks([])
#     ax1.margins(y=0.15)

#     ########### QQ plots #################

#     ax2.set_title('%s QQ (percentiles)' %(fluxname.capitalize()), fontsize=9)
    
#     ax2,imin,imax = plot_qq(obs_year,obs_decade,ax2,'royalblue')

#     ax1.set_ylabel('Frequency',fontsize=9)
#     ax2.set_ylabel('Observation (decade) %s' %xlabel.split()[-1], fontsize=9)

#     ax1.set_xlabel(xlabel, fontsize=9)
#     ax2.set_xlabel('Observation (year) %s' %xlabel.split()[-1], fontsize=9)

#     ax2.yaxis.tick_right()

#     handles, labels = ax2.get_legend_handles_labels()

#     labels = ['Coupled']
#     ax2.legend(labels=labels,handles=handles,loc='upper left',handletextpad=-0.2)

#     fig.subplots_adjust(wspace=0.09)

#     if obsflux == 'rain':
#         ax1.text(0.02,0.90,'Obs. decade yearly total: %.0f mm' %(obs_decade.sum()/10), 
#             transform=ax1.transAxes,fontsize=7,ha='left',color='royalblue')
#         ax1.text(0.02,0.85,'Obs. year total: %.0f mm' %(obs_year.sum()),
#             transform=ax1.transAxes,fontsize=7,ha='left',color='black')

#     # set QQ limits and number of ticks to be equal
#     ax2.set_xlim([imin,imax])
#     ax2.set_ylim([imin,imax])
#     plt.locator_params(nbins=4)

#     fig.savefig('%s/ch5_%s_%s.pdf' %(plotpath,obsflux,exp), dpi=300,bbox_inches='tight',pad_inches=0.05)

# print(BOMobs['Melb'].loc[start:end].describe())
# print(BOMobs['Melb'].loc['2000':'2009'].describe())

####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################

    # for ncflux,obsflux,fluxname in zip(ncfluxes,obsfluxes,fluxnames):

    #     ##################################################
    #     # print('plotting dist: %s (%s)' %(ncflux,fluxname))

    #     if ncflux=='rain':
    #         s = coupledday[ncflux].dropna()
    #         # o = obsday.loc[(obsday['rain']>0.01),'rain']
    #         o = BOMobs['Bundoora']['rain'].dropna()
    #     elif ncflux in ['t40max','t40min']:
    #         s = coupledday[ncflux].dropna()
    #         o = obsday[obsflux].dropna()
    #     else:
    #         s = coupled[ncflux].dropna()
    #         o = towerobs[obsflux].dropna()
        
    #     gmin = min(s.min(),o.min())
    #     gmax = max(s.max(),o.max())
    #     # gmin = o.min()
    #     # gmax = 10
    #     evalpoints=np.linspace(gmin,gmax,bins*2)
    #     datarange = (gmin,gmax)

    #     fig, ax = plt.subplots(figsize=(7,3))

    #     ax.set_title('%s distribution (%s)' %(fluxname.capitalize(),decade))
    #     ax.text(0.02,0.94,'%s' %model,transform=ax.transAxes,fontsize=8,ha='left')
    #     ax.text(0.02,0.88, 'Skill: %s' %perkins_skill(s,o,bins=50).round(2),transform=ax.transAxes,fontsize=8,ha='left')

    #     plot_hist_kde(data=s, label='%s: coupledulation RCP85 (n=%s)' %(ncflux,len(s.dropna())), colour='red', 
    #                     evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax)
    #     plot_hist_kde(data=o, label='%s: Observation at Preston (n=%s)' %(obsflux,len(o.dropna())), colour='black', 
    #                     evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax)


    #     fig.savefig('%s/dist_%s_%s_%s_%s.png' %(plotpath,ncflux,model,exp,decade), dpi=300,bbox_inches='tight',pad_inches=0.05)

    #     if QQ:
    #         ################################################
    #         print('plotting QQ: %s (%s)' %(ncflux,fluxname))

    #         fig, ax = plt.subplots(figsize=(5.5,5)) 
    #         ax.set_title('%s QQ (%s)' %(fluxname.capitalize(),decade))
    #         ax.text(0.02,0.94,'%s' %model,transform=ax.transAxes,fontsize=8,ha='left')

    #         plot_qq(o,s,ax)

    #         ax.set_xlabel('Observation (Preston) percentiles $\mathrm{[W/m^2]}$', fontsize=8)
    #         ax.set_ylabel('Simulation percentiles $\mathrm{[W/m^2]}$', fontsize=8)

    #         fig.savefig('%s/QQ_%s_%s_%s_%s.png' %(plotpath,ncflux,model,exp,decade), dpi=300,bbox_inches='tight',pad_inches=0.05)


# # ###################################

# flux = 'cc'
# s=coupled[flux]

# gmin = s.min()
# gmax = s.max()
# evalpoints=np.linspace(gmin,gmax,bins*2)
# datarange = (gmin,gmax)

# fig, ax = plt.subplots(figsize=(7,3)) 
# ax.set_title('Daily %s distribution (%s)' %(flux.capitalize(),decade))
# ax.text(0.02,0.94,'%s' %model,transform=ax.transAxes,fontsize=8,ha='left')

# plot_hist_kde(data=s, label='%s: Simulation RCP85 (n=%s)' %(flux,len(s.dropna())), colour='red', 
#                 evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax)

# fig.savefig('%s/dist_%s_%s_%s_%s.png' %(plotpath,flux,model,exp,decade), dpi=300,bbox_inches='tight',pad_inches=0.05)


#     # plot Tmin,Tmax

#     plt.close('all')

#     for flux,bomvar in zip(['t2min','t2max'],['t2min','t2max']):
#         # print('plotting dist: %s' %(flux))

#         gmin = min(coupledday[flux].min(),BOMobs['Melb'][bomvar].min(),BOMobs['Bundoora'][bomvar].min())
#         gmax = max(coupledday[flux].max(),BOMobs['Melb'][bomvar].max(),BOMobs['Bundoora'][bomvar].max())
#         evalpoints=np.linspace(gmin,gmax,bins*2)
#         datarange = (gmin,gmax)

#         fig, ax = plt.subplots(figsize=(7,3)) 
#         ax.set_title('Daily %s distribution (%s)' %(flux.capitalize(),decade))
#         ax.text(0.02,0.94,'%s' %model,transform=ax.transAxes,fontsize=8,ha='left')
#         ax.text(0.02,0.88, 'Skill: %s' %perkins_skill(coupledday[flux],BOMobs['Melb'][bomvar],bins=50).round(2),transform=ax.transAxes,fontsize=8,ha='left')

#         plot_hist_kde(data=coupledday[flux], label='%s: Simulation RCP85 (n=%s)' %(flux,len(coupledday[flux].dropna())), colour='red', 
#                         evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax)
#         # plot_hist_kde(data=BOMobs['Viewbank'][bomvar], label='%s: Observation at Viewbank (n=%s)' %(bomvar,len(BOMobs['Viewbank'][bomvar].dropna())), colour='blue', 
#         #                 evalpoints=evalpoints, datarange=datarange, ax=ax)
#         plot_hist_kde(data=BOMobs['Bundoora'][bomvar], label='%s: Observation at Bundoora (n=%s)' %(bomvar,len(BOMobs['Bundoora'][bomvar].dropna())), colour='blue', 
#                         evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax)
#         plot_hist_kde(data=BOMobs['Melb'][bomvar], label='%s: Observation at Melb Regional Office (n=%s)' %(bomvar,len(BOMobs['Melb'][bomvar].dropna())), colour='black', 
#                         evalpoints=evalpoints, datarange=datarange, bins=bins, ax=ax)

#         ax.xaxis.set_major_formatter(degtick)

#         fig.savefig('%s/dist_%s_%s_%s_%s.png' %(plotpath,flux,model,exp,decade), dpi=300,bbox_inches='tight',pad_inches=0.05)

#     ########### QQ plots #################
#         if QQ:

#         # for flux,bomvar in zip(['t2min','t2max'],['t2min','t2max']):
#             print('plotting QQ: %s' %(flux))

#             fig, ax = plt.subplots(figsize=(3.5,3)) 
#             ax.set_title('%s QQ (%s)' %(flux.capitalize(),decade))
#             ax.text(0.02,0.94,'%s' %model,transform=ax.transAxes,fontsize=8,ha='left')

#             plot_qq(BOMobs['Melb'][bomvar],coupledday[flux],ax)

#             ax.xaxis.set_major_formatter(degtick)
#             ax.yaxis.set_major_formatter(degtick)

#             ax.set_xlabel('Observation (Melb. Regional Office) percentiles', fontsize=8)
#             ax.set_ylabel('Simulation percentiles', fontsize=8)

#             fig.savefig('%s/QQ_%s_%s_%s_%s.png' %(plotpath,flux,model,exp,decade), dpi=300,bbox_inches='tight',pad_inches=0.05)


