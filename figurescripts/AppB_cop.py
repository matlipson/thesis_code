import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sys
import os

oshome=os.getenv('HOME')
pd.set_option('display.width', 150)
sys.path.append('%s/ownCloud/phd/03-Code/phd_python' %oshome)

atebpath = '%s/ownCloud/phd/10-Thesis/figurescripts/output/cop' %oshome

alpha04=pd.read_csv('%s/alpha04.dat' %(atebpath), 
    na_values=['nan'], delim_whitespace=True, skiprows=[0,1,2,3,4,6], engine='c')
alpha04.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')

x = np.arange(27.8,54.4,0.2)

uclem = pd.read_csv('%s/appB_it1_uclem.var' %(atebpath), delim_whitespace=True, skiprows=4, skipfooter=1, header=None, 
    names=['roomtemp','canyontemp','spec_humid','pressure','energyuse','ac_flux'],engine='python')
uclem.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')
uclem['cop'] = uclem['ac_flux']/uclem['energyuse']
uclem['Tair'] = alpha04.Tair - 273.15

# uclem2 = pd.read_csv('%s/appB_it1_uclem8.var' %(atebpath), delim_whitespace=True, skiprows=4, skipfooter=1, header=None, 
#     names=['roomtemp','canyontemp','spec_humid','pressure','energyuse','ac_flux'],engine='python')
# uclem2.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')
# uclem2['cop'] = uclem2['ac_flux']/uclem2['energyuse']
# uclem2['Tair'] = alpha04.Tair - 273.15

ep = pd.read_csv('%s/appB_it1_ep.var' %(atebpath), delim_whitespace=True, skiprows=4, skipfooter=1, header=None, 
    names=['roomtemp','canyontemp','spec_humid','pressure','energyuse','ac_flux'],engine='python')
ep.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')
ep['cop'] = ep['ac_flux']/ep['energyuse']
ep['Tair'] = alpha04.Tair - 273.15

modified = pd.read_csv('%s/appB_it1_modified.var' %(atebpath), delim_whitespace=True, skiprows=4, skipfooter=1, header=None, 
    names=['roomtemp','canyontemp','spec_humid','pressure','energyuse','ac_flux'],engine='python')
modified.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')
modified['cop'] = modified['ac_flux']/modified['energyuse']
modified['Tair'] = alpha04.Tair - 273.15

modified2 = pd.read_csv('%s/appB_it1_modified2.var' %(atebpath), delim_whitespace=True, skiprows=4, skipfooter=1, header=None, 
    names=['roomtemp','canyontemp','spec_humid','pressure','energyuse','ac_flux'],engine='python')
modified2.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')
modified2['cop'] = modified2['ac_flux']/modified2['energyuse']
modified2['Tair'] = alpha04.Tair - 273.15


plt.close('all')
fig, ax1 = plt.subplots(ncols=1,figsize=(6,3))

ep.plot.scatter(x='Tair',y='cop',ax=ax1,label='TEB-BEM (Bueno et al., 2012)',c='none',edgecolor='blue',s=8,marker='o',facecolor='None',linewidth=0.2,alpha=0.80)
uclem.plot.scatter(x='Tair',y='cop',ax=ax1,label=r'UCLEM original $\Gamma_{ac}=10$',c='green',s=8,marker='x',linewidth=0.4,alpha=0.80)
modified2.plot.scatter(x='Tair',y='cop',ax=ax1,label=r'UCLEM modified $\Gamma_{ac}=2$',c='orange',s=9,marker='+',linewidth=0.4,alpha=0.80)
modified.plot.scatter(x='Tair',y='cop',ax=ax1,label=r'UCLEM modified $\Gamma_{ac}=3$',c='red',s=9,marker='+',linewidth=0.4,alpha=0.80)
ax1.plot(x,(9.459-0.3323*x**(0.7654)),label="Payne & Domanski, 2002",color='k')
ax1.plot([27,35,43,48],[3.61,2.93,2.31,1.94],ms=3,marker='s',color='k',label='Zheng & Liang, 2010',linewidth=1.0)

ax1.set_title('Coefficient of performance vs temperature')
ax1.set_ylim(0,11)
ax1.set_ylabel('Coefficient of performance (COP)')
ax1.set_xlim(20,50)
ax1.set_xticks(range(20,51,2))
ax1.set_xlabel('External air temperature (°C)')



ax1.legend(fontsize=6,loc='upper right')

start, end = '2003-12-07','2003-12-21'

fig.savefig('figures/AppB_cop.pdf', dpi=200,bbox_inches='tight',pad_inches=0.05)

#### following used to test energyplus paramaterisation only ####

# from met_functions import calc_sat_vap_pressure, calc_rel_humidity, calc_wet_bulb_temp, \
#                           cap_curve_ep, eir_curve_ep, rtf_curve, uclem_cop

# data.loc[data.ac_flux<=0,'ac_flux'] = np.nan

# ac_coolcap = 100
# cop_nominal = 24.5
# cop_max = 10
# gamma_ac = 10

# # vectorise functions
# Tw_vect = np.vectorize(calc_wet_bulb_temp)
# eir_vect = np.vectorize(eir_curve_ep)
# cap_vect = np.vectorize(cap_curve_ep)
# rtf_vect = np.vectorize(rtf_curve)
# uclem_vect = np.vectorize(uclem_cop)


# data['Tw'] = Tw_vect(data['spec_humid'],data['pressure'],data['roomtemp'])

# cop = pd.DataFrame(index=data.index)

# # cop['uclem'] = data['ac_flux']/data['energyuse']
# # cop['energyplus'] = ( cop_nominal / ( 
# #                         cap_vect(data['Tw'],data['canyontemp'])*
# #                         eir_vect(data['Tw'],data['canyontemp'])*
# #                         plf_vect(data['ac_flux'],ac_coolcap) ))

# # cop['uclem_func'] = uclem_vect(gamma_ac,data['canyontemp'],data['roomtemp'],cop_max)
# cop['uclem_cop'] = data['ac_flux']/data['energyuse']
# cop['ep_power'] = (ac_coolcap 
# 					*cap_vect(data['Tw'],data['canyontemp'])
# 					*eir_vect(data['Tw'],data['canyontemp'])
# 					*rtf_vect(data['ac_flux'],ac_coolcap)
# 					/cop_nominal )
# cop['ep_cop'] = data['ac_flux']/cop['ep_power']
# cop['ep_func'] = ( cop_nominal / ( 
#                         cap_vect(data['Tw'],data['canyontemp'])*
#                         eir_vect(data['Tw'],data['canyontemp'])))


# raw_cop = pd.read_csv('%s/variables/test_epcop35.var' %(atebpath), delim_whitespace=True, skiprows=4, skipfooter=1, header=None, 
#     names=['cop','energyuse'],engine='python')
# raw_cop.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')

# martilli = pd.read_csv('%s/variables/test_martilli2.var' %(atebpath), delim_whitespace=True, skiprows=4, skipfooter=1, header=None, 
#     names=['cop','energyuse'],engine='python')
# martilli.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')

# alpha04=pd.read_csv('%s/alpha04.dat' %(atebpath), 
#     na_values=['nan'], delim_whitespace=True, skiprows=[0,1,2,3,4,6], engine='c')
# alpha04.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')
# cop['Tair'] = alpha04.Tair - 273.15
# cop['raw_cop'] = raw_cop['cop']
# cop['martilli'] = martilli['cop']

# cop['raw_cop'] = raw_cop[raw_cop['cop']>1]['cop']

# # cop[cop['Tair']>30][['uclem_cop','ep_cop']].plot()

# plt.close('all')
# fig, (ax1,ax2) = plt.subplots(ncols=2,figsize=(10,3))
# # cop[cop['Tair']>24].plot.scatter(x='Tair',y='uclem_cop',ax=ax,label='uclem',c='r',s=8,marker='x',linewidth=0.4)
# # cop[cop['Tair']>24].plot.scatter(x='Tair',y='raw_cop',ax=ax,label='uclem raw',c='b',s=8,marker='x',linewidth=0.4)
# # cop[cop['Tair']>24].plot.scatter(x='Tair',y='ep_cop',ax=ax,label='Energyplus',c='k',s=8,marker='x',linewidth=0.4)
# cop.plot.scatter(x='Tair',y='uclem_cop',ax=ax1,label='uclem',c='royalblue',s=8,marker='x',linewidth=0.4)
# cop.plot.scatter(x='Tair',y='raw_cop',ax=ax1,label='energyplus',c='orangered',s=8,marker='x',linewidth=0.4)
# cop.plot.scatter(x='Tair',y='martilli',ax=ax1,label='martilli',c='green',s=8,marker='x',linewidth=0.4)
# ax1.plot(x,(9.459-0.3323*x**(0.7654)),label="payne")
# # cop.plot.scatter(x='Tair',y='ep_cop',ax=ax1,label='Energyplus',c='orangered',s=8,marker='x',linewidth=0.4)
# ax1.set_title('COP vs temperature w/ capacity %s' %ac_coolcap)
# ax1.set_ylim(0,10)
# ax1.set_ylabel('Coefficient of performance (COP)')
# ax1.set_xlim(20,40)
# ax1.set_xticks(range(20,41,2))
# ax1.set_xlabel('40m external air temperature (°C)')

# start, end = '2003-12-07','2003-12-21'

# ax2.set_title(r'Energy use')

# data['energyuse'].plot(ax=ax2, color='royalblue',label='uclem')
# raw_cop['energyuse'].plot(ax=ax2, color='orangered',label='energyplus')
# martilli['energyuse'].plot(ax=ax2, color='green',label='martilli')
# ax2.legend()
# # cop['ep_power'].plot(ax=ax2, color='orangered')

# ax2.set_ylabel(r'Cooling & heating energy ($W/m^2$)')
# ax2.set_ylim([0,20])

# # ax2.set_xlabel('date')
# ax2.set_xlim([pd.Timestamp(start),pd.Timestamp(end)])

# fig.savefig('%s/analysis/cop_it1_ep.png' %(atebpath), dpi=200,bbox_inches='tight',pad_inches=0.05)

# # plt.show()

